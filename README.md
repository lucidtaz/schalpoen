# Schalpoen

A personal blog project!

## Development

### Installation

1. `docker-compose build`
2. `docker-compose up -d`
3. `./dev composer install`
5. `./dev php vendor/bin/phinx seed:run`
6. `docker-compose logs -f`

This starts the development environment with the project directory mounted
inside it. That's why the composer install step is needed. There will also be
seeded test data, which can be re-run in order to create more data.

### Workflow

These steps are only needed during development because the local project
directory is mounted into the container. Normally the build step takes care of
it and the files don't change after that.

To install/update new Composer libraries:
```
$ ./dev composer install
$ ./dev composer update
```

To regenerate SCSS:
```
$ ./dev sassc resources/scss/schalpoen.scss public/css/schalpoen.css
```

To access the database:
```
$ docker-compose exec db psql -U webuser schalpoen_blog
```

To run checks and tests:
```
$ ./ci.sh
```

## Production

### Deployment

The master branch will automatically deploy to production as part of the GitLab
CI/CD flow. If for any reason this fails, the manual steps below can be
executed.

### Data export/import

To export/backup data from the database:

```
docker exec -i $(docker-compose -f docker-compose.production.yml ps -q db) pg_dump -U webuser schalpoen_blog > data_yyyy-mm-dd.sql
```

To import data into the database (on first setup):
```
$ docker exec -i $(docker-compose -f docker-compose.production.yml ps -q db) psql -U webuser schalpoen_blog < datadump.sql
```

It needs the container id trick because of an issue with docker-compose and
piping, discussed here: https://github.com/docker/compose/issues/4290

### Upload files for static.schalpoen.nl

The stack contains an sftp container for uploading static files to be served
from `static.schalpoen.nl`.

### Run stack on DigitalOcean

Create machine, this creates the droplet and registers it locally as a machine:

```
$ docker-machine create \
    --driver digitalocean \
    --digitalocean-access-token=<DO API key> \
    --digitalocean-size 1gb \
    --digitalocean-ipv6 \
    --digitalocean-region ams3 \
    your-docker-machine-name
$ eval $(docker-machine env your-docker-machine-name)
```

Build and run stack:
```
$ (Create .production.*.env files)
$ docker-compose -f docker-compose.production.yml build
$ docker-compose up -f docker-compose.production.yml -d
$ curl http://$(docker-machine ip your-docker-machine-name)/
```

To destroy the droplet again:
```
$ docker-machine rm your-docker-machine-name
```

## Implementation details

### Architecture

This project is built on "Clean Architecture" by Robert C. Martin. It's built in
three layers: domain, application and infrastructure. Since the project is the
result of many years of refactoring from PHP4 style code, there are still some
parts that are not properly divided yet. The technical goal of the project is to
refactor rather than rewrite.

### Testing approach

There's three levels of testing:

1. Unit - everything runs in isolation so there's no dependencies and
   configuration to keep synchronized.
2. Integration - edge-to-edge tests which build an HTTP request object and pass
   it to the application, making assertions on the HTTP response object and
   possibly any side effects like database changes.
3. Smoke - end-to-end tests which call the HTTP webserver, making assertions
   only on the response.
