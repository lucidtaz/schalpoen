#!/bin/sh

set -eux

docker-compose build
docker-compose run --rm dev composer install

docker-compose up -d

docker-compose run --rm dev php vendor/bin/phpstan analyse --configuration phpstan.neon --level 5 src

docker/scripts/wait-for-container-healthy.sh "$(docker-compose ps -q app)"
docker-compose run --rm dev php vendor/bin/phpunit --testdox

docker-compose down
