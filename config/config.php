<?php

declare(strict_types=1);

return [
    'analytics' => [
        'enabled' => filter_var(getenv('ANALYTICS'), FILTER_VALIDATE_BOOLEAN),
        'id' => getenv('ANALYTICS_ID') ?: 'unset',
    ],
    'db' => [
        'host' => getenv('DB_HOST') ?: 'localhost',
        'user' => getenv('DB_USER') ?: 'schalpoen_blog',
        'pass' => getenv('DB_PASS') ?: 'unset',
        'name' => getenv('DB_NAME') ?: 'schalpoen_blog',
    ],
    'app' => [
        'basepath' => getenv('APP_BASEPATH') ?: 'http://localhost',
        'captcha' => [
            'enabled' => getenv('APP_CAPTCHA_ENABLED') !== 'false',
        ],
        'https' => [
            'strict' => getenv('APP_HTTPS_STRICT') !== 'false',
        ],
        'timezone' => [
            // Default timezone for display purposes
            'default' => getenv('APP_TIMEZONE_DEFAULT') ?: 'Europe/Amsterdam',
        ],
    ],
    'sentry' => [
        'enabled' => filter_var(getenv('SENTRY'), FILTER_VALIDATE_BOOLEAN),
        'dsn' => getenv('SENTRY_DSN') ?: null,
        'environment' => getenv('SENTRY_ENVIRONMENT') ?: null,
    ],
    'exceptions' => [
        'stacktrace' => filter_var(getenv('EXCEPTIONS_STACKTRACE'), FILTER_VALIDATE_BOOLEAN),
    ],
];
