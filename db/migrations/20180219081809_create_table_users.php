<?php

use Phinx\Migration\AbstractMigration;

class CreateTableUsers extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('users');
        $table->addColumn('displayName', 'text')
            ->addColumn('userName', 'text')
            ->addColumn('passwd', 'text')
            ->addColumn('isAuthor', 'integer', ['default' => 0, 'limit' => 65535]) // TODO: boolean?
            ->addColumn('isPublisher', 'integer', ['default' => 0, 'limit' => 65535]) // TODO: boolean?
            ->addColumn('email', 'text', ['null' => true])
            ->addColumn('emailHidden', 'boolean', ['default' => true])
            ->addColumn('notifyReplies', 'boolean', ['default' => true])
            ->addColumn('created', 'timestamp')
            ->addColumn('lastLogin', 'timestamp', ['null' => true])
            ->create();
    }
}
