<?php

use Phinx\Migration\AbstractMigration;

class CreateTableTokens extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('tokens', ['id' => false]);
        $table->addColumn('uId', 'integer')
            ->addColumn('token', 'text')
            ->addColumn('created', 'timestamp')
            ->addForeignKey('uId', 'users', 'id')
            ->create();
    }
}
