<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePosts extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('posts');
        $table->addColumn('author', 'integer')
            ->addColumn('title', 'text')
            ->addColumn('text', 'text')
            ->addColumn('created', 'timestamp')
            ->addColumn('edited', 'timestamp', ['null' => true])
            ->addColumn('published', 'timestamp', ['null' => true])
            ->addColumn('status', 'text')
            ->addColumn('preview', 'text')
            ->addForeignKey('author', 'users', 'id')
            ->addIndex('created')
            ->addIndex('status')
            ->create();
    }
}
