<?php

use Phinx\Migration\AbstractMigration;

class CreateTableTags extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('tags');
        $table->addColumn('title', 'text')
            ->create();
    }
}
