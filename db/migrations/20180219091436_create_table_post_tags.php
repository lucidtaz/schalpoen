<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePostTags extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('postTags', ['id' => false]);
        $table->addColumn('post', 'integer')
            ->addColumn('tag', 'integer')
            ->addForeignKey('post', 'posts', 'id')
            ->addForeignKey('tag', 'tags', 'id')
            ->create();
    }
}
