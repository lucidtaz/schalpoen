<?php

use Phinx\Migration\AbstractMigration;

class CreateTableSmileys extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('smileys');
        $table->addColumn('filePath', 'text')
            ->addColumn('link', 'text')
            ->addColumn('width', 'integer')
            ->addColumn('height', 'integer')
            ->create();
    }
}
