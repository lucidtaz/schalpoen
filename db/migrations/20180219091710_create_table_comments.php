<?php

use Phinx\Migration\AbstractMigration;

class CreateTableComments extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('comments');
        $table->addColumn('post', 'integer')
            ->addColumn('author', 'integer')
            ->addColumn('reply', 'integer', ['null' => true])
            ->addColumn('text', 'text')
            ->addColumn('created', 'timestamp')
            ->addForeignKey('post', 'posts', 'id')
            ->addForeignKey('author', 'users', 'id')
            ->create();
    }
}
