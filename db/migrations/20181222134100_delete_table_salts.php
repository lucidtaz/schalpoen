<?php

use Phinx\Migration\AbstractMigration;

class DeleteTableSalts extends AbstractMigration
{
    public function change(): void
    {
        if ($this->hasTable('salts')) {
            $table = $this->table('salts', ['id' => false]);
            $table->drop()->save();
        }
    }
}
