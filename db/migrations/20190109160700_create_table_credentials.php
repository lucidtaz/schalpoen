<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCredentials extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('credentials');
        $table->addColumn('userId', 'integer')
            ->addColumn('username', 'text')
            ->addColumn('passwordHash', 'text')
            ->addColumn('created', 'timestamp')
            ->addColumn('updated', 'timestamp')
            ->addForeignKey('userId', 'users', 'id')
            ->create();

        // TODO: Index on username?
    }
}
