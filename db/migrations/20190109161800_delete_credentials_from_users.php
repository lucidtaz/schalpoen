<?php

use Phinx\Migration\AbstractMigration;

class DeleteCredentialsFromUsers extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('users');
        $table->removeColumn('userName')
            ->removeColumn('passwd')
            ->update();
    }
}
