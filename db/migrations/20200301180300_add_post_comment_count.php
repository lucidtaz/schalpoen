<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

class AddPostCommentCount extends AbstractMigration
{
    public function change(): void
    {
        $table = $this->table('posts');
        $table->addColumn('commentCount', 'integer', ['default' => 0])
            ->update();

        // Correcting the data should be done one time manually on production.
    }
}
