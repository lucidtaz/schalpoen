<?php

declare(strict_types=1);

use schalpoen\infrastructure\components\PasswordStrategy;
use schalpoen\domain\models\Post;
use Phinx\Seed\AbstractSeed;

class FixedData extends AbstractSeed
{
    public function run(): void
    {
        $exists = $this->fetchRow('SELECT 1 FROM users WHERE "displayName" = \'Adminius\';');
        if (!empty($exists)) {
            $this->getOutput()->writeln('Skipping FixedData since it is already applied.');
            return;
        }

        $passwordStrategy = new PasswordStrategy();

        $users = $this->table('users');
        $users->insert([
            'displayName' => 'Adminius',
            'isAuthor' => 1,
            'isPublisher' => 1,
            'created' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
        ]);
        $users->save();

        // We cannot use application code to fetch the inserted user, because
        // this seeder runs in a separate connection from the rest of the
        // application and I believe the transactions are isolated between
        // connections
        $insertedUser = $this->fetchRow('SELECT * FROM users ORDER BY id DESC LIMIT 1;');

        $credentials = $this->table('credentials');
        $credentials->insert([
            'userId' => $insertedUser['id'],
            'username' => 'admin',
            'passwordHash' => $passwordStrategy->hashPassword('admin'),
            'created' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
            'updated' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
        ]);
        $credentials->save();

        $posts = $this->table('posts');
        $posts->insert([
            'author' => $insertedUser['id'],
            'title' => 'Welcome to the sample data',
            'text' => 'Welcome to the sample data. An author+publisher user has been seeded with the credentials admin/admin.',
            'created' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
            'published' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
            'status' => Post::STATUS_PUBLISHED,
            'preview' => 'schalpoen.png',
        ]);
        $posts->save();
    }
}
