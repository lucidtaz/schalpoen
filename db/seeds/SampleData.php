<?php

declare(strict_types=1);

use Faker\Factory;
use schalpoen\infrastructure\components\PasswordStrategy;
use schalpoen\domain\models\Post;
use Phinx\Seed\AbstractSeed;

class SampleData extends AbstractSeed
{
    public function run(): void
    {
        $faker = Factory::create();

        $passwordStrategy = new PasswordStrategy();

        $users = $this->table('users');
        $users->insert([
            'displayName' => $faker->name,
            'isAuthor' => 1,
            'email' => $faker->email,
            'created' => $faker->dateTime->format('Y-m-d H:i:s'),
        ]);
        $users->save();

        // We cannot use application code to fetch the inserted user, because
        // this seeder runs in a separate connection from the rest of the
        // application and I believe the transactions are isolated between
        // connections
        $insertedUser = $this->fetchRow('SELECT * FROM users ORDER BY id DESC LIMIT 1;');

        $credentials = $this->table('credentials');
        $credentials->insert([
            'userId' => $insertedUser['id'],
            'username' => $faker->userName,
            'passwordHash' => $passwordStrategy->hashPassword($faker->password),
            'created' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
            'updated' => (new DateTimeImmutable())->format('Y-m-d H:i:s'),
        ]);
        $credentials->save();

        $posts = $this->table('posts');
        $posts->insert([
            'author' => $insertedUser['id'],
            'title' => $faker->sentence,
            'text' => $faker->paragraphs(5, true),
            'created' => $faker->dateTime->format('Y-m-d H:i:s'),
            'published' => $faker->dateTime->format('Y-m-d H:i:s'),
            'status' => Post::STATUS_PUBLISHED,
            'preview' => 'schalpoen.png',
        ]);
        $posts->save();
    }
}
