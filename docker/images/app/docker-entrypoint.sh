#!/usr/bin/env bash

set -e

docker/scripts/wait-for-database.sh "$DB_HOST" "$DB_USER" "$DB_PASS" "$DB_NAME"

vendor/bin/phinx migrate

exec "$@"
