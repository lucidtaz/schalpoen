#!/bin/sh

# Ensure that the entire volume is owned by the uploader user, so it can
# add files directly to the root of the volume.
chown -R uploader /home/uploader/schalpoen-upload
