<?php

$config = require(__DIR__ . '/config/config.php');

return [
    'paths' => [
        'migrations' => __DIR__ . '/db/migrations',
        'seeds' => __DIR__ . '/db/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'dockerized',
        'dockerized' => [
            'adapter' => 'pgsql',
            'host' => $config['db']['host'],
            'name' => $config['db']['name'],
            'user' => $config['db']['user'],
            'pass' => $config['db']['pass'],
            'port' => '5432',
            'charset' => 'utf8',
        ],
    ],
    'version_order' => 'creation',
];
