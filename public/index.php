<?php

declare(strict_types=1);

use GuzzleHttp\Psr7\ServerRequest;
use schalpoen\infrastructure\components\Application;
use schalpoen\infrastructure\components\ResponseEmitter;
use schalpoen\infrastructure\components\Toolbox;

$executionStarted = microtime(true);
define('SCHALPOEN_EXECUTION_STARTED', $executionStarted);

require_once __DIR__ . '/../vendor/autoload.php';

$config = Toolbox::getConfig();

if ($config['sentry']['enabled']) {
    Sentry\init([
        'dsn' => $config['sentry']['dsn'],
        'environment' => $config['sentry']['environment'],
    ]);
}

$application = new Application($config);
$responseEmitter = new ResponseEmitter();

$request = ServerRequest::fromGlobals();
$response = $application->getHttpKernel()->handle($request);
$responseEmitter->send($response);
