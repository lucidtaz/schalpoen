<?php

declare(strict_types=1);

namespace schalpoen\application\email;

class Email
{
    public string $fromEmail;

    public ?string $fromName;

    public array $to = [];


    public string $subject;

    public string $body;

    public function setFrom(string $email, ?string $name = null): void
    {
        $this->fromEmail = $email;
        $this->fromName = $name;
    }

    public function addTo(string $email, ?string $name = null): void
    {
        $this->to[$email] = $name;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function setBody(string $body): void
    {
        $this->body = $body;
    }
}
