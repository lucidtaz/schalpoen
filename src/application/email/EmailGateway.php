<?php

declare(strict_types=1);

namespace schalpoen\application\email;

interface EmailGateway
{
    public function send(Email $email): void;
}
