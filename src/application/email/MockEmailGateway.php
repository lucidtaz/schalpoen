<?php

declare(strict_types=1);

namespace schalpoen\application\email;

class MockEmailGateway implements EmailGateway
{
    /**
     * @var Email[]
     */
    public array $sentEmails = [];

    public function send(Email $email): void {
        $this->sentEmails[] = $email;
    }
}
