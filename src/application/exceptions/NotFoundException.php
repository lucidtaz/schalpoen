<?php

declare(strict_types=1);

namespace schalpoen\application\exceptions;

use \Exception;

class NotFoundException extends Exception
{

}
