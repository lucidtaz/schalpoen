<?php

declare(strict_types=1);

namespace schalpoen\application\exceptions;

use RuntimeException;
use Throwable;

class ValidationException extends RuntimeException
{
    /**
     * @var string[]
     */
    private array $errors;

    /**
     * @param string[] $errors
     */
    public function __construct(array $errors, Throwable $previous = null)
    {
        parent::__construct('Validation error', 0, $previous);
        $this->errors = $errors;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
