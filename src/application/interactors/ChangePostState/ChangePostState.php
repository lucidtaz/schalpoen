<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ChangePostState;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\exceptions\ValidationException;
use schalpoen\domain\exceptions\AuthorizationException;
use schalpoen\domain\models\Post;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;

class ChangePostState
{
    private PostRepository $postRepository;
    private UserRepository $userRepository;

    public function __construct(PostRepository $postRepository, UserRepository $userRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws ForbiddenException
     * @throws NotFoundException
     */
    public function run(ChangePostStateInputModel $inputModel): Post
    {
        $user = $this->userRepository->retrieve($inputModel->userId);
        $post = $this->postRepository->retrieve($inputModel->postId);

        try {
            $post->authorize($user);
        } catch (AuthorizationException $e) {
            throw new ForbiddenException('Not authorized.', 0, $e);
        }

        if ($inputModel->targetState === Post::STATUS_PUBLISHED && !$user->isPublisher()) {
            throw new ForbiddenException('You are no publisher.');
        }

        if (!$this->isValidTransition($post->status, $inputModel->targetState)) {
            throw new ValidationException(['Not a valid state transition.']);
        }

        $post->setState($inputModel->targetState);

        $this->postRepository->update($post);

        return $post;
    }

    private function isValidTransition(string $fromState, string $toState): bool
    {
        $transitions = [
            Post::STATUS_DRAFT => [Post::STATUS_FINISHED, Post::STATUS_DELETED],
            Post::STATUS_FINISHED => [Post::STATUS_DRAFT, Post::STATUS_REJECTED, Post::STATUS_PUBLISHED, Post::STATUS_DELETED],
            Post::STATUS_REJECTED => [Post::STATUS_FINISHED, Post::STATUS_DELETED],
            Post::STATUS_PUBLISHED => [],
        ];
        $validTargetStates = $transitions[$fromState];
        return in_array($toState, $validTargetStates, true);
    }
}
