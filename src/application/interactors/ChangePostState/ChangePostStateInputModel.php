<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ChangePostState;

class ChangePostStateInputModel
{
    public int $userId;

    public int $postId;

    public string $targetState;

    public function __construct(int $userId, int $postId, string $targetState)
    {
        $this->userId = $userId;
        $this->postId = $postId;
        $this->targetState = $targetState;
    }
}
