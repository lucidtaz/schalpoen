<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ChooseTags;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\exceptions\AuthorizationException;
use schalpoen\domain\models\Tag;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\TagRepository;
use schalpoen\application\persistence\UserRepository;

/**
 * Filter which tags should be shown based on current post tags
 */
class ChooseTags
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;
    private TagRepository $tagRepository;

    public function __construct(
        UserRepository $userRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository
    ) {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @throws NotFoundException
     * @throws ForbiddenException
     */
    public function run(ChooseTagsInputModel $inputModel): ChooseTagsResponseModel
    {
        $user = $this->userRepository->retrieve($inputModel->actingUserId);
        $post = $this->postRepository->retrieve($inputModel->postId);
        $postTags = $this->tagRepository->retrieveByPost($post);
        $allTags = $this->tagRepository->retrieveAll();

        try {
            $post->authorize($user);
        } catch (AuthorizationException $e) {
            throw new ForbiddenException('Only publishers can tag posts.', 0, $e);
        }

        $availableTags = array_udiff($allTags, $postTags, fn(Tag $a, Tag $b) => $a->getId() <=> $b->getId());

        return new ChooseTagsResponseModel($post, $availableTags);
    }
}
