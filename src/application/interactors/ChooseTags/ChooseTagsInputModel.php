<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ChooseTags;

class ChooseTagsInputModel
{
    public int $actingUserId;

    public int $postId;

    public function __construct(int $actingUserId, int $postId)
    {
        $this->actingUserId = $actingUserId;
        $this->postId = $postId;
    }
}
