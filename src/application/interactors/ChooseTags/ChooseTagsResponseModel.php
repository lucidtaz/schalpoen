<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ChooseTags;

use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;

class ChooseTagsResponseModel
{
    public Post $post;

    /**
     * @var Tag[]
     */
    public array $availableTags;

    /**
     * @param Tag[] $availableTags
     */
    public function __construct(Post $post, array $availableTags)
    {
        $this->post = $post;
        $this->availableTags = $availableTags;
    }
}
