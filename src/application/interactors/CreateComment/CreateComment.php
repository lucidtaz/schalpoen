<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\CreateComment;

use LogicException;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\parser\Parser;
use schalpoen\application\email\Email;
use schalpoen\application\email\EmailGateway;
use schalpoen\domain\exceptions\AuthorizationException;
use schalpoen\domain\models\Comment;
use schalpoen\application\persistence\CommentRepository;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;

class CreateComment
{
    private CommentRepository $commentRepository;
    private PostRepository $postRepository;
    private UserRepository $userRepository;

    private EmailGateway $emailGateway;
    private Parser $parser;

    public function __construct(
        CommentRepository $commentRepository,
        PostRepository $postRepository,
        UserRepository $userRepository,
        EmailGateway $emailGateway,
        Parser $parser
    ) {
        $this->commentRepository = $commentRepository;
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
        $this->emailGateway = $emailGateway;
        $this->parser = $parser;
    }

    /**
     * @throws ForbiddenException
     * @throws NotFoundException
     */
    public function run(CreateCommentInputModel $inputModel): Comment
    {
        $post = $this->postRepository->retrieve($inputModel->postId);
        $author = $this->userRepository->retrieve($inputModel->authorId);

        try {
            $post->authorize($author); // Forbid manipulating URL to an unpublished post
        } catch (AuthorizationException $e) {
            throw new ForbiddenException('Not authorized.', 0, $e);
        }

        $comment = new Comment($post, $author, $inputModel->text);

        if (isset($inputModel->parentId)) {
            $parent = $this->commentRepository->retrieve($inputModel->parentId);
            $comment->setParent($parent);
        }

        $this->commentRepository->insert($comment);
        $this->mailNotifications($comment);

        $post->commentCount++;
        $this->postRepository->update($post);

        return $comment;
    }

    private function mailNotifications(Comment $comment): void
    {
        // Notify the person whom you are replying to
        $mailSentToParent = false; // Will be used to determine if we should send a second email to the author
        if (!$comment->parent !== null && $comment->parentWantsNotification() && $comment->parent->author->getEmail() !== null) {
            $parentEmail = new Email();
            $parentEmail->setFrom('noreply@schalpoen.nl');
            $parentEmail->addTo($comment->parent->author->getEmail(), $comment->parent->author->getName());
            $parentEmail->setSubject('Antwoord op je reactie in artikel: ' . $comment->post->getTitle());
            $parentEmail->setBody(
                $comment->author->getName() . ' heeft geantwoord op je reactie in artikel: ' . $comment->post->href(true) . ".<br />\n" .
                "<br />\nJij schreef:<br />\n" . $this->parser->blockParse($comment->parent->text, false) . "<br />\n" .
                "<br />\n"
                . $comment->author->getName() . " schreef:<br />\n" .
                $this->parser->blockParse($comment->text, false) . "<br />\n" .
                "<br />\n" .
                "Je ontvangt deze mail omdat je in je account op <a href='http://www.schalpoen.nl'>schalpoen.nl</a> hebt aangegeven via de e-mail op de hoogte gehouden te worden van antwoorden. Ga naar je <a href='http://www.schalpoen.nl/profile'>profielpagina</a> om je instellingen te wijzigen. Lukt het niet? Stuur dan een mailtje naar thijs@schalpoen.nl."
            );
            $this->emailGateway->send($parentEmail);
            $mailSentToParent = true;
        }

        $postAuthor = $comment->post->getAuthor();

        // We don't want to receive two mails:
        if (!($mailSentToParent && $comment->parent !== null && $comment->parent->authorAlsoWrotePost())) {
            // We don't want to receive a mail on our own comments:
            if ($comment->postAuthorWantsNotification()) {
                $postAuthorEmailAddress = $postAuthor->getEmail();
                if ($postAuthorEmailAddress === null) {
                    throw new LogicException('Post author wants email but email is not set');
                }

                $postAuthorEmail = new Email();
                $postAuthorEmail->setFrom('noreply@schalpoen.nl');
                $postAuthorEmail->addTo($postAuthorEmailAddress, $postAuthor->getName());
                $postAuthorEmail->setSubject('Reactie op ' . $comment->post->getTitle());
                $postAuthorEmail->setBody($comment->author->getName() . ' heeft gereageerd op je artikel: ' . $comment->post->href(true) . ".<br />\n<br />\n" . $comment->author->getName() . " schreef:<br />\n" . $this->parser->blockParse($comment->text, false));
                $this->emailGateway->send($postAuthorEmail);
            }
        }
    }
}
