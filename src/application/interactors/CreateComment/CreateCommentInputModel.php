<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\CreateComment;

class CreateCommentInputModel
{
    public int $authorId;

    public int $postId;

    public string $text;

    public ?int $parentId;

    public function __construct(int $authorId, int $postId, string $text, ?int $parentId)
    {
        $this->authorId = $authorId;
        $this->postId = $postId;
        $this->text = $text;
        $this->parentId = $parentId;
    }
}
