<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\CreateDraft;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Post;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;

class CreateDraft
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;

    public function __construct(UserRepository $userRepository, PostRepository $postRepository)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @throws ForbiddenException
     * @throws NotFoundException
     */
    public function run(CreateDraftInputModel $inputModel): Post
    {
        $author = $this->userRepository->retrieve($inputModel->authorId);
        if (!$author->isAuthor()) {
            throw new ForbiddenException('Only authors can create drafts.');
        }

        $post = new Post(
            $author,
            $inputModel->title,
            $inputModel->text
        );
        $post->setPreview($inputModel->preview);
        $this->postRepository->insert($post);

        return $post;
    }
}
