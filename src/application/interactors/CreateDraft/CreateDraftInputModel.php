<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\CreateDraft;

class CreateDraftInputModel
{
    public int $authorId;

    public string $title;

    public string $preview;

    public string $text;

    public function __construct(int $authorId, string $title, string $preview, string $text)
    {
        $this->authorId = $authorId;
        $this->title = $title;
        $this->preview = $preview;
        $this->text = $text;
    }
}
