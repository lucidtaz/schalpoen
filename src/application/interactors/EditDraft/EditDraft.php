<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\EditDraft;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\domain\models\Post;

class EditDraft
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;

    public function __construct(UserRepository $userRepository, PostRepository $postRepository)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @throws ForbiddenException If the authenticated user is not authorized
     * @throws LoginException If the authenticated user does not exist
     * @throws NotFoundException If the post does not exist
     */
    public function run(EditDraftInputModel $inputModel): Post
    {
        try {
            $author = $this->userRepository->retrieve($inputModel->authorId);
        } catch (NotFoundException $e) {
            throw new LoginException('You must be logged in.', 0, $e);
        }

        $post = $this->postRepository->retrieve($inputModel->postId);

        if (!$author->isAuthor()) {
            throw new ForbiddenException('Only authors can edit drafts.');
        }

        $post->authorize($author);

        return $post;
    }
}
