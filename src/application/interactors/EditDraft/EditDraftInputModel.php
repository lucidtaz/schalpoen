<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\EditDraft;

class EditDraftInputModel
{
    public int $authorId;

    public int $postId;

    public function __construct(int $authorId, int $postId)
    {
        $this->authorId = $authorId;
        $this->postId = $postId;
    }
}
