<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\GetPostArchive;

use schalpoen\domain\models\Post;
use schalpoen\application\persistence\PostRepository;

class GetPostArchive
{
    private PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function run(): GetPostArchiveResponseModel
    {
        $posts = $this->postRepository->retrieveAll();
        $publishedPosts = array_filter($posts, fn(Post $post) => $post->isPublished());

        $postsByYearAndMonth = [];
        foreach ($publishedPosts as $post) {
            $month = $post->getMonth();
            $year = $post->getYear();

            if (!array_key_exists($year, $postsByYearAndMonth)) {
                $postsByYearAndMonth[$year] = [];
            }
            if (!array_key_exists($month, $postsByYearAndMonth[$year])) {
                $postsByYearAndMonth[$year][$month] = [];
            }
            $postsByYearAndMonth[$year][$month][] = $post;
        }

        foreach ($postsByYearAndMonth as &$postsByMonth) {
            foreach ($postsByMonth as &$posts) {
                usort($posts, $this->getDescendingPublishedPostComparator());
            }
            unset($posts);
            krsort($postsByMonth, SORT_NUMERIC);
        }
        unset($postsByMonth);
        krsort($postsByYearAndMonth, SORT_NUMERIC);

        return new GetPostArchiveResponseModel($postsByYearAndMonth);
    }

    private function getDescendingPublishedPostComparator(): callable
    {
        return fn(Post $a, Post $b) => $b->getPublishedAt() <=> $a->getPublishedAt();
    }
}
