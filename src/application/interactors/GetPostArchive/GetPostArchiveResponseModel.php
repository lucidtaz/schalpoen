<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\GetPostArchive;

class GetPostArchiveResponseModel
{
    /**
     * Multi-dimensional array of Post objects. They are indexed by year (int)
     * and then by month (int).
     */
    public array $postsByYearAndMonth;

    public function __construct(array $postsByYearAndMonth)
    {
        $this->postsByYearAndMonth = $postsByYearAndMonth;
    }
}
