<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\RegisterUser;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\exceptions\ValidationException;
use schalpoen\domain\models\User;
use schalpoen\application\persistence\CaptchaRepository;
use schalpoen\application\persistence\UserRepository;

class RegisterUser
{
    private UserRepository $userRepository;
    private CaptchaRepository $captchaRepository;

    public function __construct(UserRepository $userRepository, CaptchaRepository $captchaRepository)
    {
        $this->userRepository = $userRepository;
        $this->captchaRepository = $captchaRepository;
    }

    /**
     * @throws NotFoundException If the captcha does not exist
     * @throws ValidationException If the user's input is problematic
     */
    public function run(RegisterUserInputModel $inputModel): User
    {
        $errors = [];
        if (!$this->userRepository->isDisplaynameFree($inputModel->displayname)) {
            $errors[] = 'De gebruikersnaam is bezet.';
        }

        $captcha = $this->captchaRepository->retrieve($inputModel->captchaId);
        if (!$captcha->answerMatches($inputModel->captchaAnswer)) {
            $errors[] = 'Vul de verificatievraag correct in s.v.p.';
        }

        if (!empty($errors)) {
            throw new ValidationException($errors);
        }

        $user = new User($inputModel->displayname);
        $this->userRepository->insert($user);

        return $user;
    }
}
