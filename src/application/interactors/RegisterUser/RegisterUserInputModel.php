<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\RegisterUser;

class RegisterUserInputModel
{
    public string $displayname;

    public int $captchaId;

    public string $captchaAnswer;

    public function __construct(string $displayname, int $captchaId, string $captchaAnswer)
    {
        $this->displayname = $displayname;
        $this->captchaId = $captchaId;
        $this->captchaAnswer = $captchaAnswer;
    }
}
