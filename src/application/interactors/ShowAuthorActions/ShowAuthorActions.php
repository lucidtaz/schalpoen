<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ShowAuthorActions;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;

/**
 * Show a list of Posts that an author can interact with
 */
class ShowAuthorActions
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;

    public function __construct(UserRepository $userRepository, PostRepository $postRepository)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @throws ForbiddenException
     * @throws LoginException
     */
    public function run(ShowAuthorActionsInputModel $input): ShowAuthorActionsResponseModel
    {
        try {
            $author = $this->userRepository->retrieve($input->authorId);
        } catch (NotFoundException $e) {
            throw new LoginException('Please login.', 0, $e);
        }

        if (!$author->isAuthor()) {
            throw new ForbiddenException('User is not an author.');
        }

        $drafts = $this->postRepository->retrieveEditableBy($author);
        $finalizedPosts = $this->postRepository->retrieveFinalizedBy($author);

        return new ShowAuthorActionsResponseModel($drafts, $finalizedPosts);
    }
}
