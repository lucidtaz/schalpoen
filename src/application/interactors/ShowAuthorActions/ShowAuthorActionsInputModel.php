<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ShowAuthorActions;

class ShowAuthorActionsInputModel
{
    public int $authorId;

    public function __construct(int $authorId)
    {
        $this->authorId = $authorId;
    }
}
