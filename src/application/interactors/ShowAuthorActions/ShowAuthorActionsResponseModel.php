<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ShowAuthorActions;

use schalpoen\domain\models\Post;

class ShowAuthorActionsResponseModel
{
    /**
     * @var Post[]
     */
    public array $drafts;

    /**
     * @var Post[]
     */
    public array $finalizedPosts;

    /**
     * @param Post[] $drafts
     * @param Post[] $finalizedPosts
     */
    public function __construct(array $drafts, array $finalizedPosts)
    {
        $this->drafts = $drafts;
        $this->finalizedPosts = $finalizedPosts;
    }
}
