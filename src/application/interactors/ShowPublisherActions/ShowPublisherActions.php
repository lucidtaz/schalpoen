<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ShowPublisherActions;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\domain\models\Post;

/**
 * Show a list of Posts that a publisher can interact with
 */
class ShowPublisherActions
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;

    public function __construct(UserRepository $userRepository, PostRepository $postRepository)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @throws ForbiddenException
     * @throws LoginException
     */
    public function run(ShowPublisherActionsInputModel $input): ShowPublisherActionsResponseModel
    {
        try {
            $author = $this->userRepository->retrieve($input->publisherId);
        } catch (NotFoundException $e) {
            throw new LoginException('Please login.', 0, $e);
        }

        if (!$author->isPublisher()) {
            throw new ForbiddenException('User is not a publisher.');
        }

        $posts = $this->postRepository->retrieveAll();
        $publishablePosts = array_filter($posts, fn(Post $post) => $post->isPublishable());

        return new ShowPublisherActionsResponseModel($publishablePosts);
    }
}
