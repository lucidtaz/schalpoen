<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ShowPublisherActions;

class ShowPublisherActionsInputModel
{
    public int $publisherId;

    public function __construct(int $publisherId)
    {
        $this->publisherId = $publisherId;
    }
}
