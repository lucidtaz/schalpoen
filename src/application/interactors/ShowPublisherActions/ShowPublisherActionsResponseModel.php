<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ShowPublisherActions;

use schalpoen\domain\models\Post;

class ShowPublisherActionsResponseModel
{
    /**
     * @var Post[]
     */
    public array $publishablePosts;

    /**
     * @param Post[] $publishablePosts
     */
    public function __construct(array $publishablePosts)
    {
        $this->publishablePosts = $publishablePosts;
    }
}
