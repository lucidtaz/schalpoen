<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\TagPost;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\PostTagRepository;
use schalpoen\application\persistence\TagRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\domain\exceptions\AuthorizationException;
use schalpoen\domain\models\Tag;

class TagPost
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;
    private TagRepository $tagRepository;
    private PostTagRepository $postTagRepository;

    /**
     * @todo Rework relation between posts and tags to aggregate root approach with one repository
     */
    public function __construct(
        UserRepository $userRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository,
        PostTagRepository $postTagRepository
    ) {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
        $this->postTagRepository = $postTagRepository;
    }

    /**
     * @throws NotFoundException If any of the inputs do not exist.
     * @throws ForbiddenException
     */
    public function run(TagPostInputModel $inputModel): void
    {
        $post = $this->postRepository->retrieve($inputModel->postId);
        $user = $this->userRepository->retrieve($inputModel->publisherId);

        try {
            $post->authorize($user);
        } catch (AuthorizationException $e) {
            throw new ForbiddenException('Only publishers can tag posts', 0, $e);
        }

        if ($inputModel->tagId === null) {
            $tag = new Tag($inputModel->newTagTitle);
            $this->tagRepository->insert($tag);
        } else {
            $tag = $this->tagRepository->retrieve($inputModel->tagId);
        }
        $this->postTagRepository->link($post, $tag);
    }
}
