<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\TagPost;

use InvalidArgumentException;

class TagPostInputModel
{
    public int $publisherId;

    public int $postId;

    public ?int $tagId;

    public ?string $newTagTitle;

    public function __construct(int $publisherId, int $postId, ?int $tagId, ?string $newTagTitle)
    {
        if ($tagId === null && $newTagTitle === null) {
            throw new InvalidArgumentException('Either $tagId or $newTagTitle must be provided.');
        }

        $this->publisherId = $publisherId;
        $this->postId = $postId;
        $this->tagId = $tagId;
        $this->newTagTitle = $newTagTitle;
    }
}
