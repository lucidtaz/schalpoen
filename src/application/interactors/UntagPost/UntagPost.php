<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\UntagPost;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\PostTagRepository;
use schalpoen\application\persistence\TagRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\domain\exceptions\AuthorizationException;

class UntagPost
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;
    private TagRepository $tagRepository;
    private PostTagRepository $postTagRepository;

    /**
     * @todo Rework relation between posts and tags to aggregate root approach with one repository
     */
    public function __construct(
        UserRepository $userRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository,
        PostTagRepository $postTagRepository
    ) {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
        $this->postTagRepository = $postTagRepository;
    }

    /**
     * @throws NotFoundException If any of the inputs do not exist.
     * @throws ForbiddenException
     */
    public function run(UntagPostInputModel $inputModel): void
    {
        $user = $this->userRepository->retrieve($inputModel->publisherId);
        $post = $this->postRepository->retrieve($inputModel->postId);
        $tag = $this->tagRepository->retrieve($inputModel->tagId);

        try {
            $post->authorize($user);
        } catch (AuthorizationException $e) {
            throw new ForbiddenException('Only publishers can untag posts', 0, $e);
        }

        $this->postTagRepository->unlink($post, $tag);
    }
}
