<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\UntagPost;

class UntagPostInputModel
{
    public int $publisherId;

    public int $postId;

    public int $tagId;

    public function __construct(int $publisherId, int $postId, int $tagId)
    {
        $this->publisherId = $publisherId;
        $this->postId = $postId;
        $this->tagId = $tagId;
    }
}
