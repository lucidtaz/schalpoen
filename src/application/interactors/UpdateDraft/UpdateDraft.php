<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\UpdateDraft;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Post;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;

class UpdateDraft
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;

    public function __construct(UserRepository $userRepository, PostRepository $postRepository)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @throws ForbiddenException
     * @throws NotFoundException
     */
    public function run(UpdateDraftInputModel $inputModel): Post
    {
        $author = $this->userRepository->retrieve($inputModel->authorId);
        $post = $this->postRepository->retrieve($inputModel->postId);
        if ($post->getAuthor()->id !== $author->id) {
            throw new ForbiddenException('Only draft authors can update their drafts.');
        }

        $post->title = $inputModel->title;
        $post->preview = $inputModel->preview;
        $post->text = $inputModel->text;
        $post->editedUnixTime = time();

        $this->postRepository->update($post);

        return $post;
    }
}
