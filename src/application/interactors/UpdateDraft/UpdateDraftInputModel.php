<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\UpdateDraft;

class UpdateDraftInputModel
{
    public int $authorId;

    public int $postId;

    public string $title;

    public string $preview;

    public string $text;

    public function __construct(int $authorId, int $postId, string $title, string $preview, string $text)
    {
        $this->authorId = $authorId;
        $this->postId = $postId;
        $this->title = $title;
        $this->preview = $preview;
        $this->text = $text;
    }
}
