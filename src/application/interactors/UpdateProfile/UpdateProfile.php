<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\UpdateProfile;

use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\User;
use schalpoen\application\persistence\UserRepository;

class UpdateProfile
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @throws NotFoundException
     * @throws LoginException
     */
    public function run(UpdateProfileInputModel $inputModel): User
    {
        $user = $this->userRepository->retrieve($inputModel->userId);

        if (!$inputModel->reliablyAuthenticated && !empty($inputModel->email) && $inputModel->email !== $user->getEmail()) {
            // User tries to update email when not reliably authenticated
            // The input form disabled the field so this only happens when working around that
            throw new LoginException('Please login again before updating your email address');
        }

        $user->displayName = trim(substr($inputModel->displayName, 0, 30));
        if ($inputModel->reliablyAuthenticated) {
            // When updating without safeLogin, the email field is disabled,
            // leading to an empty value coming into this function. Make sure
            // not to use it!
            $user->email = $inputModel->email;
        }
        $user->emailHidden = $inputModel->emailHidden;
        $user->notifyReplies = $inputModel->notifyReplies;

        $this->userRepository->update($user);

        return $user;
    }
}
