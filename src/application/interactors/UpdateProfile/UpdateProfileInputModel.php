<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\UpdateProfile;

class UpdateProfileInputModel
{
    public int $userId;

    /**
     * Whether the authentication was done in a reliable manner (e.g. username +
     * password) or not (e.g. cookie token)
     */
    public bool $reliablyAuthenticated;

    public string $displayName;

    public string $email;

    public bool $emailHidden;

    public bool $notifyReplies;

    public function __construct(
        int $userId,
        bool $reliablyAuthenticated,
        string $displayName,
        string $email,
        bool $emailHidden,
        bool $notifyReplies
    ) {
        $this->userId = $userId;
        $this->reliablyAuthenticated = $reliablyAuthenticated;
        $this->displayName = $displayName;
        $this->email = $email;
        $this->emailHidden = $emailHidden;
        $this->notifyReplies = $notifyReplies;
    }
}
