<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ViewPost;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\exceptions\AuthorizationException;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;

class ViewPost
{
    private PostRepository $postRepository;
    private UserRepository $userRepository;

    public function __construct(PostRepository $postRepository, UserRepository $userRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws ForbiddenException
     * @throws NotFoundException
     */
    public function run(ViewPostInputModel $inputModel): ViewPostResponseModel
    {
        $post = $this->postRepository->retrieve($inputModel->postId);

        if ($inputModel->userId !== null) {
            $actingUser = $this->userRepository->retrieve($inputModel->userId);

            try {
                $post->authorize($actingUser);
            } catch (AuthorizationException $e) {
                throw new ForbiddenException('Not authorized.', 0, $e);
            }
        } else {
            // Guest user, the post must be published in order to be viewable
            $actingUser = null;
            if (!$post->isPublished()) {
                throw new ForbiddenException('This post is not published yet.');
            }
        }

        $userCanComment = $actingUser !== null && $post->isPublished();
        $userCanEdit = $actingUser !== null && !$post->isPublished() && $actingUser->getId() === $post->getAuthor()->getId();
        $userCanPublish = $actingUser !== null && $actingUser->isPublisher() && $post->isPublishable();

        return new ViewPostResponseModel($post, $userCanComment, $userCanEdit, $userCanPublish);
    }
}
