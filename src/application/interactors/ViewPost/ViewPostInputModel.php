<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ViewPost;

class ViewPostInputModel
{
    public int $postId;

    public ?int $userId;

    public function __construct(int $postId, ?int $userId)
    {
        $this->postId = $postId;
        $this->userId = $userId;
    }
}
