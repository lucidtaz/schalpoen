<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ViewPost;

use schalpoen\domain\models\Post;

class ViewPostResponseModel
{
    public Post $post;

    /**
     * Whether the authenticated user can comment on the post.
     */
    public bool $userCanComment;

    /**
     * Whether the authenticated user can edit the post in its current state.
     */
    public bool $userCanEdit;

    /**
     * Whether the authenticated user can publish the post in its current state.
     */
    public bool $userCanPublish;

    public function __construct(Post $post, bool $userCanComment, bool $userCanEdit, bool $userCanPublish)
    {
        $this->post = $post;
        $this->userCanComment = $userCanComment;
        $this->userCanEdit = $userCanEdit;
        $this->userCanPublish = $userCanPublish;
    }
}
