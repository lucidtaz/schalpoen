<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ViewTag;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\TagRepository;
use schalpoen\domain\models\Post;

class ViewTag
{
    private TagRepository $tagRepository;
    private PostRepository $postRepository;

    public function __construct(TagRepository $tagRepository, PostRepository $postRepository)
    {
        $this->tagRepository = $tagRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * @throws NotFoundException
     */
    public function run(ViewTagInputModel $inputModel): ViewTagResponseModel
    {
        $tag = $this->tagRepository->retrieve($inputModel->id);
        $posts = $this->postRepository->retrieveByTag($tag);

        $publishedPosts = array_filter($posts, fn(Post $post) => $post->isPublished());

        return new ViewTagResponseModel(
            $tag,
            $publishedPosts
        );
    }
}
