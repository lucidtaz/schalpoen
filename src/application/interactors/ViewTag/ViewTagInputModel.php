<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ViewTag;

class ViewTagInputModel
{
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
