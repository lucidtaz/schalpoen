<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\ViewTag;

use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;

class ViewTagResponseModel
{
    public Tag $tag;

    /**
     * @var Post[]
     */
    public array $posts;

    /**
     * @param Post[] $posts
     */
    public function __construct(Tag $tag, array $posts)
    {
        $this->tag = $tag;
        $this->posts = $posts;
    }
}
