<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\WriteDraft;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\UserRepository;

class WriteDraft
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @throws ForbiddenException
     * @throws LoginException
     */
    public function run(WriteDraftInputModel $inputModel): void
    {
        try {
            $author = $this->userRepository->retrieve($inputModel->authorId);
        } catch (NotFoundException $e) {
            throw new LoginException('You must be logged in.', 0, $e);
        }

        if (!$author->isAuthor()) {
            throw new ForbiddenException('Only authors can write drafts.');
        }
    }
}
