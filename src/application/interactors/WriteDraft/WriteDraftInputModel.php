<?php

declare(strict_types=1);

namespace schalpoen\application\interactors\WriteDraft;

class WriteDraftInputModel
{
    public int $authorId;

    public function __construct(int $authorId)
    {
        $this->authorId = $authorId;
    }
}
