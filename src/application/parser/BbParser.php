<?php

namespace schalpoen\application\parser;

use schalpoen\infrastructure\components\Toolbox;
use schalpoen\application\persistence\SmileyRepository;

/**
 * @todo Separate concerns to (1) parsing and (2) formatting. Parsing belongs in
 * the domain or application layer while formatting belongs in infrastructure.
 */
class BbParser implements Parser
{
    private SmileyRepository $smileyRepository;
    
    public function __construct(SmileyRepository $smileyRepository)
    {
        $this->smileyRepository = $smileyRepository;
    }

    public function inlineParse(string $input, bool $trusted = true): string
    {
        return $this->smileyParse(
            $this->inlineOnlyParse(
                $this->bothParse(
                    htmlspecialchars($input),
                    $trusted,
                    false
                )
            )
        );
    }

    public function blockParse(string $input, bool $trusted = true): string
    {
        return $this->smileyParse(
            $this->blockOnlyParse(
                $this->bothParse(
                    htmlspecialchars($input),
                    $trusted,
                    true
                )
            )
        );
    }

    private function inlineOnlyParse(string $input): string
    {
        $search = array(
        );

        $replace = array(
        );

        //Spoiler:
        $output = preg_replace_callback("/\[spoiler](.+?)\[\/spoiler]/im", array(self::class, 'spoilerInline'), $input);

        $output = preg_replace($search, $replace, $output ?? '');

        return $output ?? '';
    }

    private function blockOnlyParse(string $input): string
    {
        $search = array(
            "/\[size=([1-9]|[0-1][0-9]|20)\](.+?)\[\/size\]/ism",
            "/\[code\](.+?)\[\/code\]/ism",
            "/\[h\](.+?)\[\/h\]/ism",
        );

        $replace = array(
            "<span style='font-size: $1pt'>$2</span>",
            "<div style='padding: 0.5em; white-space: pre; font-family: \"Courier New\", Courier, monospace; border: 1px solid black; overflow: auto;'>$1</div>",
            "<h5 style='margin-top: 1em; margin-bottom: 0.5em;'>$1</h5>",
        );

        //Spoiler:
        $output = preg_replace_callback("/\[spoiler](.+?)\[\/spoiler]/ism", array(self::class, 'spoilerBlock'), $input);
        //Youtube Video:
        $output = preg_replace_callback("/\[vid](.+?)\[\/vid]/i", array(self::class, 'video'), $output ?? '');
        //Images:
        $output = preg_replace_callback("/\[img(=(.+?))?](http)?(.+?)\[\/img]/im", array(self::class, 'image'), $output ?? '');

        $output = preg_replace($search, $replace, $output ?? '');

        $output = nl2br($output ?? '');
        $output = str_replace(array("\r", "\n"), '', $output);
        return $output;
    }

    private function bothParse(string $input, bool $trusted, bool $multiLine): string
    {
        $search = array(
            "/\[b\](.+?)\[\/b\]/im",
            "/\[i\](.+?)\[\/i\]/im",
            "/\[u\](.+?)\[\/u\]/im",
            "/\[s\](.+?)\[\/s\]/im",
            "/\[li\](.+?)\[\/li\]/im",
            "/\[color=(red|orange|yellow|green|blue|indigo|violet|pink|white)\](.+?)\[\/color\]/im",
            "/\[color=#([0-9A-F]{6}|[0-9A-F]{3})\](.+?)\[\/color\]/im",
            "/\[bg=#([0-9A-F]{6}|[0-9A-F]{3})\](.+?)\[\/bg\]/im",
            "/\[inlinecode\](.+?)\[\/inlinecode\]/im",
        );

        $replace = array(
            "<span style='font-weight:bold;'>$1</span>",
            "<span style='font-style:italic;'>$1</span>",
            "<span style='text-decoration:underline;'>$1</span>",
            "<span style='text-decoration:line-through;'>$1</span>",
            '&#149; $1',
            "<span style='color: $1'>$2</span>",
            "<span style='color: #$1'>$2</span>",
            "<span style='background-color: #$1'>$2</span>",
            "<span style='white-space: pre-wrap; font-family: \"Courier New\", Courier, monospace; border: 1px solid black; padding-left: 0.25em; padding-right: 0.25em;'>$1</span>",
        );

        if ($multiLine) {
            // Add multiline modifiers
            $search = array_map(fn (string $needle) => "{$needle}s", $search);
        }

        $output = preg_replace($search, $replace, $input);

        // URLs
        if ($trusted) {
            $output = preg_replace_callback("/\[url(=(.+?))?](.+?)\[\/url]/im", array(self::class, 'BBUrlTrusted'), $output ?? '');
        } else {
            $output = preg_replace_callback("/\[url(=(.+?))?](.+?)\[\/url]/im", array(self::class, 'BBUrlUntrusted'), $output ?? '');
        }

        return $output ?? '';
    }

    private function smileyParse(string $input): string
    {
        $smileyTranslations = (new SmileyManager($this->smileyRepository))->getTranslationArray();

        $output = $input;

        foreach ($smileyTranslations as $link => $image) {
            $nFound = 0; //str_replace will assign this variable a value.
            $output = str_replace($link, $image, $output, $nFound);
        }
        return $output;
    }

    private function BBUrlTrusted(array $matches): string
    {
        return $this->BBUrl($matches, true);
    }

    private function BBUrlUntrusted(array $matches): string
    {
        return $this->BBUrl($matches, false);
    }

    private function BBUrl(array $matches, bool $trusted): string
    {
        if (empty($matches[1])) {
            $target = $matches[3];
            $desc = $matches[3];
        } else {
            $target = $matches[2];
            $desc = $matches[3];
        }
        if (strpos($target, 'http://') === false && strpos($target, 'https://') === false && strpos($target, 'ftp://') === false && strpos($target, 'mailto:') === false) {
            $target = 'https://' . $target;
        }
        $external = '';
        $baseUrl = Toolbox::getConfig()['app']['basepath'];
        if (strpos($target, $baseUrl) !== 0) {
            $external = " target='_blank'";
        } else {
            $trusted = true; // override omdat we toch naar het Schalpoen linken.
        }

        if (!$trusted) {
            $trustedPart = " rel='nofollow noopener'";
        } else {
            $trustedPart = '';
        }

        return "<a href='" . addslashes($target) . "'" . $external . $trustedPart . '>' . $desc . '</a>';
    }

    private function video(array $matches): string
    {
        // Youtube implementatie
        $vidId = stristr($matches[1], 'watch?v=');
        if ($vidId === false) {
            return '(Video embed error)';
        }
        $vidId = substr($vidId, 8, \strlen($vidId));
        // TODO: Responsive iframe embed: http://materializecss.com/media-css.html
        return '<iframe class="embeddedVideo"' .
              ' src="https://www.youtube.com/embed/' . ($vidId ?: '') . '?fs=1"></iframe>';
    }

    private function image(array $matches): string
    {
        if (\strlen($matches[1]) > 1) {
            return "<img class='bordered scaleWidth' src='http" . $matches[4] . "' alt='Geplaatste afbeelding' />" . "\n" .
                "<span style='font-size: small;'>" . $matches[2] . '</span>';
        }

        return "<img class='bordered scaleWidth' src='http" . $matches[4] . "' alt='Geplaatste afbeelding' />";
    }

    //Twee tussenmethods omdat we geen params aan spoiler(...) kunnen geven wanneer we hem als callback gebruiken.
    private function spoilerInline(array $matches): string
    {
        return $this->spoiler($matches, false);
    }

    private function spoilerBlock(array $matches): string
    {
        return $this->spoiler($matches, true);
    }

    private function spoiler(array $matches, bool $block): string
    {
        $id = Toolbox::getUniqueNumber();

        if ($block) {
            $expandedDisplayStyle = 'block';
        } else {
            $expandedDisplayStyle = 'inline';
        }

        return
            "<div style='display:" . $expandedDisplayStyle . ";'>" .
            '<a id="spoiler_placeholder_' . $id . "\" href='#' onclick='" .
            'document.getElementById("spoiler_' . $id . '").style.display = "' . $expandedDisplayStyle . '";' .
            'document.getElementById("spoiler_placeholder_' . $id . '").style.display = "none";' .
            'return false;' .
                "'>Spoiler!</a>" .
            '<span id="spoiler_' . $id . "\" style='display:none;'>" .
                    "<a href='#' onclick='" .
            'document.getElementById("spoiler_' . $id . '").style.display = "none";' .
            'document.getElementById("spoiler_placeholder_' . $id . '").style.display = "' . $expandedDisplayStyle . '";' .
            'return false;' .
                    "'>Spoiler:</a> " . $matches[1] .
            '</span>' .
            '</div>';
    }
}
