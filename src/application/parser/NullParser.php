<?php

declare(strict_types=1);

namespace schalpoen\application\parser;

class NullParser implements Parser
{
    public function inlineParse(string $input, bool $trusted = true): string
    {
        return $input;
    }

    public function blockParse(string $input, bool $trusted = true): string
    {
        return $input;
    }
}
