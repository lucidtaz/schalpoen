<?php

declare(strict_types=1);

namespace schalpoen\application\parser;

interface Parser
{
    public function inlineParse(string $input, bool $trusted = true): string;
    public function blockParse(string $input, bool $trusted = true): string;
}
