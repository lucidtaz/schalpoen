<?php

declare(strict_types=1);

namespace schalpoen\application\parser;

use schalpoen\domain\models\Smiley;
use schalpoen\application\persistence\SmileyRepository;

class SmileyManager
{
    private SmileyRepository $smileyRepository;

    private bool $initialized = false;

    /**
     * @var Smiley[]
     */
    private array $smileys = [];

    public function __construct(SmileyRepository $smileyRepository)
    {
        $this->smileyRepository = $smileyRepository;
    }

    private function init(): void
    {
        if (!$this->initialized) {
            $this->smileys = $this->smileyRepository->retrieveAll();
            $this->initialized = true;
        }
    }

    /**
     * Returns the translation array that maps smiley codes to smiley
     * URLs. The result of this function can directly be used in a
     * str_replace.
     */
    public function getTranslationArray(): array
    {
        $this->init();

        $result = [];

        foreach ($this->smileys as $smiley) {
            foreach ($smiley->getLinkOptions() as $linkOption) {
                $result[$linkOption] = $smiley->getHtml();
            }
        }

        // Order the array in decreasing link-length, such that :)) will be parsed before :)
        uksort($result, fn(string $a, string $b) => \strlen($b) - \strlen($a));

        return $result;
    }
}
