<?php

declare(strict_types=1);

namespace schalpoen\application\parser;

/**
 * Strips any HTML from parser result
 */
class StripParserDecorator implements Parser
{
    private Parser $inner;

    public function __construct(Parser $inner)
    {
        $this->inner = $inner;
    }

    public function inlineParse(string $input, bool $trusted = true): string
    {
        $text = $this->inner->inlineParse($input, $trusted);

        // TODO: Change this part once we update the parser architecture (The
        // goal here is to get the readable text without markup, which would be
        // a lot easier with an updated (proper) parser) Stripping tags is a
        // hack.

        $text = strip_tags($text);
        $text = htmlspecialchars_decode($text);
        return $text;
    }

    public function blockParse(string $input, bool $trusted = true): string
    {
        $text = $this->inner->blockParse($input, $trusted);
        $text = strip_tags($text);
        $text = htmlspecialchars_decode($text);
        return $text;
    }
}
