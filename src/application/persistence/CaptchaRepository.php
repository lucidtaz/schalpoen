<?php

declare(strict_types=1);

namespace schalpoen\application\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Captcha;

interface CaptchaRepository
{
    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): Captcha;

    /**
     * @throws NotFoundException If there are no Captcha's at all
     */
    public function retrieveRandom(): Captcha;
}
