<?php

declare(strict_types=1);

namespace schalpoen\application\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Comment;
use schalpoen\domain\models\Post;

interface CommentRepository
{
    public function insert(Comment $comment): void;

    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): Comment;

    /**
     * @return Comment[]
     */
    public function retrieveRootsForPost(Post $post): array;

    public function countByPost(Post $post): int;
}
