<?php

declare(strict_types=1);

namespace schalpoen\application\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\domain\models\User;

interface PostRepository
{
    public function insert(Post $post): void;

    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): Post;

    /**
     * @return Post[]
     */
    public function retrieveAll(): array;

    /**
     * @return Post[]
     */
    public function retrieveLastPublishedPosts(int $limit = 0): array;

    public function countPublishable(): int;

    /**
     * @return Post[]
     */
    public function retrieveEditableBy(User $author): array;

    public function countEditable(User $author): int;

    /**
     * @return Post[]
     */
    public function retrieveFinalizedBy(User $author): array;

    /**
     * @return Post[]
     */
    public function retrieveByTag(Tag $tag): array;

    public function update(Post $post): void;
}
