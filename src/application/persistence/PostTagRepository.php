<?php

declare(strict_types=1);

namespace schalpoen\application\persistence;

use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;

// TODO: PostTag is not a domain model object, the interface should not exist but be part of posts as the owning side of the relationship
interface PostTagRepository
{
    public function link(Post $post, Tag $tag): void;

    public function unlink(Post $post, Tag $tag): void;
}
