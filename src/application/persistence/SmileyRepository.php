<?php

declare(strict_types=1);

namespace schalpoen\application\persistence;

use schalpoen\domain\models\Smiley;

interface SmileyRepository
{
    /**
     * @return Smiley[]
     */
    public function retrieveAll(): array;
}
