<?php

declare(strict_types=1);

namespace schalpoen\application\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;

interface TagRepository
{
    public function insert(Tag $tag): void;

    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): Tag;

    /**
     * @return Tag[]
     */
    public function retrieveByPost(Post $post): array;

    /**
     * @return Tag[]
     */
    public function retrieveAll(): array;

    /**
     * Retrieve the n Tags with the most published posts
     *
     * If there is a tie, results are ordered by tag title alphabetically.
     *
     * @return Tag[]
     */
    public function retrievePopular(int $n): array;
}
