<?php

declare(strict_types=1);

namespace schalpoen\application\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\User;

interface UserRepository
{
    public function insert(User $user): void;

    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): User;

    public function isDisplaynameFree(string $displayName): bool;

    public function update(User $user): void;
}
