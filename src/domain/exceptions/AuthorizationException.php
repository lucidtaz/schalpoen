<?php

declare(strict_types=1);

namespace schalpoen\domain\exceptions;

use RuntimeException;

class AuthorizationException extends RuntimeException
{

}
