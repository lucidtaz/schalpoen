<?php

declare(strict_types=1);

namespace schalpoen\domain\models;

class Captcha
{
    public int $id;

    public string $question;

    /**
     * @var string[]
     */
    public array $answers;

    /**
     * @param string[] $answers
     */
    public function __construct(int $id, string $question, array $answers)
    {
        $this->id = $id;
        $this->question = $question;
        $this->answers = $answers;
    }

    public function answerMatches(string $answer): bool
    {
        $_answer = strtolower(trim(str_replace('.', '', $answer)));
        $_answers = array_map('strtolower', $this->answers);
        return \in_array($_answer, $_answers, true);
    }
}
