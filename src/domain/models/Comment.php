<?php

declare(strict_types=1);

namespace schalpoen\domain\models;

use DateTimeImmutable;

class Comment
{
    public ?int $id = null;

    public Post $post;

    public User $author;

    /**
     * Comment object to which this comment is a reply
     */
    public ?Comment $parent = null;

    public string $text;

    public int $unixTime;

    /**
     * @var Comment[]
     */
    public array $children = [];

    public function __construct(Post $post, User $author, string $text)
    {
        $this->post = $post;
        $this->author = $author;
        $this->text = $text;
        $this->unixTime = (int) (new DateTimeImmutable('now'))->format('U');
    }

    public function setParent(Comment $parent): void
    {
        $this->parent = $parent;
        $parent->addChild($this);
    }

    public function isRoot(): bool
    {
        return $this->parent === null;
    }

    private function addChild(Comment $child): void
    {
        $this->children[] = $child;
    }

    public function parentWantsNotification(): bool
    {
        return $this->parent !== null && $this->parent->author->wantsReplyNotify() && $this->parent->author->getEmail() !== null;
    }

    public function postAuthorWantsNotification(): bool
    {
        return !$this->authorAlsoWrotePost() // We don't want to receive a mail on our own comments:
            && $this->post->getAuthor()->getEmail() !== null;
    }

    public function authorAlsoWrotePost(): bool
    {
        return $this->author->getId() === $this->post->getAuthor()->getId();
    }

    public function getPostedAt(): DateTimeImmutable
    {
        return (new DateTimeImmutable())->setTimestamp($this->unixTime);
    }
}
