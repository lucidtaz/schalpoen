<?php

declare(strict_types=1);

namespace schalpoen\domain\models;

use DateTimeImmutable;
use schalpoen\domain\exceptions\AuthorizationException;
use schalpoen\infrastructure\components\Toolbox;
use schalpoen\infrastructure\persistence\DbCommentRepository;
use schalpoen\infrastructure\persistence\DbPostRepository;
use schalpoen\infrastructure\persistence\DbTagRepository;
use schalpoen\infrastructure\persistence\DbUserRepository;

class Post
{
    public int $id;

    /**
     * Author of the post
     */
    public User $user;

    public string $title = '';

    /**
     * Original post text, unparsed
     */
    public string $text = '';

    /**
     * @todo Change to ?DateTimeImmutable
     */
    public ?int $publishedUnixTime = null;

    /**
     * @todo Change to ?DateTimeImmutable
     */
    public ?int $editedUnixTime = null;

    public ?string $preview = null;

    public string $status = self::STATUS_DRAFT;

    /**
     * @var int[]
     */
    public array $tagIds = [];

    public int $commentCount = 0;

    public const STATUS_DRAFT = 'draft';
    public const STATUS_FINISHED = 'finished';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_PUBLISHED = 'published';
    public const STATUS_DELETED = 'deleted';

    public function __construct(User $author, string $title, string $text)
    {
        $this->user = $author;
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * @throws AuthorizationException
     */
    public function authorize(User $user): void
    {
        if ($this->isPublished()) {
            // Anyone may see
            return;
        }
        if ($this->isEditable()) {
            if ($this->user->getId() !== $user->getId()) {
                throw new AuthorizationException('You are not the author of this post.');
            }
        }
        if ($this->isPublishable()) {
            if (!$user->isPublisher() && $this->user->getId() !== $user->getId()) {
                throw new AuthorizationException('You are no publisher, nor the author of this post.');
            }
        }
    }

    public function setPreview(string $preview): void
    {
        $this->preview = $preview;
    }

    public function setState(string $state): void
    {
        $this->status = $state;
        if ($state === self::STATUS_PUBLISHED) {
            $this->publishedUnixTime = time();
        }
    }

    /**
     * @deprecated Retrieve tags yourself
     * @return Tag[]
     */
    public function getTags(): array
    {
        // TODO: Properly hydrate
        return (new DbTagRepository())->retrieveByPost($this);
    }

    /**
     * @return int[]
     */
    public function getTagIds(): array
    {
        return $this->tagIds;
    }

    public function url(bool $absolute = false): string
    {
        //PAS OP: Als je hier enge wijzigingen in gaat doen, onthoud dat er een 301 moved permanently status zit in /post.php
        if ($absolute) {
            $domain = Toolbox::getConfig()['app']['basepath'];
        } else {
            $domain = '';
        }
        $_title = str_replace(' ', '-', $this->title);
        $_title = preg_replace('/[^a-zA-Z0-9\-\s]/', '', $_title);
        $_title = strtolower($_title ?? '');
        return $domain . '/posts/' . $this->id . '/' . htmlspecialchars($_title);
    }

    /**
     * @deprecated Output the href in the template instead
     */
    public function href(bool $absolute = false): string
    {
        return '<a href="' . $this->url($absolute) . '">' . $this->title . '</a>';
    }

    public function getIntroduction(): string
    {
        $firstParagraph = explode("\n", $this->text, 2)[0];
        $firstParagraph = trim($firstParagraph);
        return $firstParagraph;
    }

    /**
     * @return Comment[]
     */
    public function getRootComments(): array
    {
        // TODO: Eliminate this method, make Comments part of Post aggregate?
        return (new DbCommentRepository(new DbPostRepository(new DbUserRepository()), new DbUserRepository()))->retrieveRootsForPost($this);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getAuthor(): User
    {
        return $this->user;
    }

    public function getPreview(): string
    {
        return $this->preview ?? 'schalpoen.png'; // TODO: Set in a ViewModel instead?
    }

    public function isPublished(): bool
    {
        return $this->status === self::STATUS_PUBLISHED;
    }

    public function isPublishable(): bool
    {
        return $this->isFinalized();
    }

    public function isFinalized(): bool
    {
        return $this->status === self::STATUS_FINISHED;
    }

    public function isEditable(): bool
    {
        return \in_array($this->status, [self::STATUS_DRAFT, self::STATUS_REJECTED], true);
    }

    public function getPreviewUrl(): string
    {
        if ($this->getPreview() === 'schalpoen.png') {
            // The default, serve from the project URL
            // TODO: Handle the default preview as null and set schalpoen.png in the ViewModel via the presenter?
            return '/style/previews/schalpoen.png';
        }
        // Custom, uploaded preview, serve from the upload location
        return '/uploads/content/previews/' . $this->preview;
    }

    public function getAbsolutePreviewUrl(): string
    {
        return Toolbox::getConfig()['app']['basepath'] . $this->getPreviewUrl();
    }

    public function getPublishedAt(): ?DateTimeImmutable
    {
        if ($this->publishedUnixTime === null) {
            return null;
        }
        return (new DateTimeImmutable())->setTimestamp($this->publishedUnixTime);
    }

    public function getEditedAt(): ?DateTimeImmutable
    {
        if ($this->editedUnixTime === null) {
            return null;
        }
        return (new DateTimeImmutable())->setTimestamp($this->editedUnixTime);
    }

    private function getDate(): DateTimeImmutable
    {
        // Show the current moment for any posts that are not published yet.
        return $this->getPublishedAt() ?? new DateTimeImmutable();
    }

    public function getShortDate(): string
    {
        //Example: 01/12 voor 1 december
        return $this->getDate()->format('d/m');
    }

    public function getYear(): int
    {
        return (int) $this->getDate()->format('Y');
    }

    public function getMonth(): int
    {
        return (int) $this->getDate()->format('m');
    }
}
