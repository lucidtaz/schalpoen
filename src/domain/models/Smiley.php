<?php

declare(strict_types=1);

namespace schalpoen\domain\models;

use schalpoen\infrastructure\components\Toolbox;

class Smiley
{
    public int $id;

    public string $path;

    public string $link;

    public int $width;

    public int $height;

    public function __construct(int $id, string $path, string $link, int $width, int $height)
    {
        $this->id = $id;
        $this->path = $path;
        $this->link = $link;
        $this->width = $width;
        $this->height = $height;
    }

    public function getAbsoluteUrl(): string
    {
        $basepath = Toolbox::getConfig()['app']['basepath'];
        return $basepath . '/uploads/smileys/' . $this->path;
    }

    public function getHtml(): string
    {
        return '<img src="' . htmlspecialchars($this->getAbsoluteUrl()) . '" style="border: none;" alt="' . htmlspecialchars($this->getFirstLinkOption()) . '" />';
    }

    private function getFirstLinkOption(): string
    {
        return explode(',', $this->link, 2)[0];
    }

    /**
     * @return string[]
     */
    public function getLinkOptions(): array
    {
        return explode(',', $this->link);
    }
}
