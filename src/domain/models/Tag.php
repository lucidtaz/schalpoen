<?php

declare(strict_types=1);

namespace schalpoen\domain\models;

class Tag
{
    public ?int $id = null;

    public string $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function href(): string
    {
        $_title = str_replace(' ', '-', $this->title);
        $_title = preg_replace('/[^a-zA-Z0-9\-\s]/', '', $_title);
        return '<a href="/tags/' . $this->id . '/' . htmlspecialchars($_title ?? '') . '">' . htmlspecialchars($this->title) . '</a>';
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
