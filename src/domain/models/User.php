<?php

declare(strict_types=1);

namespace schalpoen\domain\models;

class User
{
    public ?int $id = null;

    public string $displayName;

    public ?string $email = null;

    public bool $emailHidden = true;

    public bool $notifyReplies = false;

    public bool $isAuthor = false;

    public bool $isPublisher = false;

    /**
     * @todo: ?DateTimeImmutable
     */
    public ?string $lastLogin = null;

    public function __construct(string $displayName)
    {
        $this->displayName = $displayName;
    }

    public function updateLoginTime(): void
    {
        $this->lastLogin = date('Y-m-d H:i:s');
    }

    public function niceName(): string
    {
        return $this->href();
    }

    public function href(): string
    {
        $_name = str_replace(' ', '-', $this->displayName);
        return '<a href="/users/' . $this->id . '/' . htmlspecialchars($_name) . '">' . htmlspecialchars($this->displayName) . '</a>';
    }

    public function emailHidden(): bool
    {
        return $this->emailHidden;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->displayName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function wantsReplyNotify(): bool
    {
        return $this->notifyReplies;
    }

    public function isAuthor(): bool
    {
        return $this->isAuthor;
    }

    public function isPublisher(): bool
    {
        return $this->isPublisher;
    }
}
