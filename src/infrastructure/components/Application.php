<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use schalpoen\infrastructure\components\authentication\Authenticator;
use schalpoen\infrastructure\components\http\Kernel;
use schalpoen\infrastructure\components\http\middleware\AppendExecutionTime;
use schalpoen\infrastructure\components\http\middleware\CookieLogin;
use schalpoen\infrastructure\components\http\middleware\GarbageCollectLoginTokens;
use schalpoen\infrastructure\components\http\middleware\OpenDatabaseConnection;
use schalpoen\infrastructure\components\http\middleware\PackSessionData;
use schalpoen\infrastructure\components\http\middleware\RedirectToDefaultSubdomain;
use schalpoen\infrastructure\components\http\middleware\SetContentType;
use schalpoen\infrastructure\components\http\middleware\StartSession;
use schalpoen\infrastructure\components\http\Router;
use schalpoen\infrastructure\components\http\Routes;
use Throwable;

class Application
{
    /**
     * Application configuration array
     */
    private array $config;

    /**
     * Filled DI container
     */
    private Services $services;

    private Kernel $httpKernel;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->services = new Services($config);

        /** @var Router $router */
        $router = new Router($this->services);
        (new Routes())->register($router);

        $this->httpKernel = new Kernel($router);
        $this->registerExceptionHandler();
        $this->registerMiddleware();
    }

    private function registerExceptionHandler(): void
    {
        $handlerFunction = function (Throwable $e, ServerRequestInterface $request): ResponseInterface {
            /** @var ExceptionHandler $handler */
            $handler = $this->services->get(ExceptionHandler::class);
            $handler->setShowStacktrace($this->config['exceptions']['stacktrace']);
            return $handler->handle($e, $request);
        };
        $this->httpKernel->setExceptionHandler($handlerFunction);
    }

    private function registerMiddleware(): void
    {
        if (defined('SCHALPOEN_EXECUTION_STARTED')) {
            $this->httpKernel->addMiddleware(new AppendExecutionTime(SCHALPOEN_EXECUTION_STARTED));
        } else {
            $this->httpKernel->addMiddleware(new AppendExecutionTime(null));
        }

        $this->httpKernel->addMiddleware(new OpenDatabaseConnection());
        $this->httpKernel->addMiddleware(new GarbageCollectLoginTokens());
        $this->httpKernel->addMiddleware(new RedirectToDefaultSubdomain($this->config['app']['basepath']));
        $this->httpKernel->addMiddleware(new StartSession([
            'secure' => $this->config['app']['https']['strict'],
            'httponly' => true,
        ]));
        $this->httpKernel->addMiddleware(new PackSessionData());
        $this->httpKernel->addMiddleware(new SetContentType('text/html; charset=utf-8'));
        $this->httpKernel->addMiddleware(new CookieLogin($this->services->get(Authenticator::class)));
    }

    public function getHttpKernel(): RequestHandlerInterface
    {
        return $this->httpKernel;
    }
}
