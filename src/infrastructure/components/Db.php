<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components;

class Db
{
    public static function connect(): void
    {
        $config = Toolbox::getConfig();
        $dbhost = $config['db']['host'];
        $dbuser = $config['db']['user'];
        $dbpasw = $config['db']['pass'];
        $dbname = $config['db']['name'];

        pg_connect("host=$dbhost dbname=$dbname user=$dbuser password=$dbpasw");
    }
}
