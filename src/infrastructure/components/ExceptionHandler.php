<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerAwareInterface;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\infrastructure\components\http\factories\MakesCannedResponses;
use schalpoen\infrastructure\components\http\NoRoutesMatchedException;
use schalpoen\infrastructure\components\logging\LogClientTrait;
use schalpoen\infrastructure\components\view\LayoutFactory;
use Throwable;

class ExceptionHandler implements LoggerAwareInterface
{
    use LogClientTrait;
    use MakesCannedResponses;

    private LayoutFactory $layoutFactory;

    private bool $showStackTrace = false;

    public function __construct(LayoutFactory $layoutFactory)
    {
        /** @noinspection UnusedConstructorDependenciesInspection */
        $this->layoutFactory = $layoutFactory; // Used by the MakesCannedResponses trait
    }

    public function setShowStacktrace(bool $enabled): void
    {
        $this->showStackTrace = $enabled;
    }

    public function handle(Throwable $exception, ServerRequestInterface $request): ResponseInterface
    {
        $this->getLogger()->warning('Handling exception...', [
            'class' => get_class($exception),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ]);

        if ($exception instanceof NoRoutesMatchedException || $exception instanceof NotFoundException) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }

        if ($exception instanceof LoginException) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        if ($exception instanceof ForbiddenException) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        }

        if ($this->showStackTrace) {
            $message = 'Uncaught exception: ' . get_class($exception) . ' - ' . $exception->getMessage() . "\n"
                . '<pre>' . $exception->getTraceAsString() . '</pre>';
        } else {
            $message = 'Uncaught exception: ' . get_class($exception) . ' - ' . $exception->getMessage();
        }

        $this->getLogger()->error('Unhandled exception', [
            'class' => get_class($exception),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ]);
        return $this->createInternalServerErrorResponse($request, $message);
    }
}
