<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components;

use RuntimeException;

class PasswordStrategy
{
    public function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    public function hashPassword(string $password): string
    {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        if ($hash === false) {
            throw new RuntimeException('Could not hash password.');
        }
        return $hash;
    }

    public function needsRehash(string $hash): bool
    {
        return password_needs_rehash($hash, PASSWORD_DEFAULT);
    }
}
