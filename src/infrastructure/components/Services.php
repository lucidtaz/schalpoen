<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components;

use League\Container\Container;
use League\Container\ReflectionContainer;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use schalpoen\application\email\EmailGateway;
use schalpoen\application\parser\BbParser;
use schalpoen\application\parser\Parser;
use schalpoen\application\persistence\CaptchaRepository;
use schalpoen\application\persistence\CommentRepository;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\PostTagRepository;
use schalpoen\application\persistence\SmileyRepository;
use schalpoen\application\persistence\TagRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\infrastructure\email\SimpleEmailGateway;
use schalpoen\infrastructure\persistence\DbCommentRepository;
use schalpoen\infrastructure\persistence\DbPostRepository;
use schalpoen\infrastructure\persistence\DbPostTagRepository;
use schalpoen\infrastructure\persistence\DbSmileyRepository;
use schalpoen\infrastructure\persistence\DbTagRepository;
use schalpoen\infrastructure\persistence\DbUserRepository;
use schalpoen\infrastructure\persistence\HardcodedCaptchaRepository;

/**
 * DI container with all application services bound
 */
class Services implements ContainerInterface
{
    private Container $container;

    /**
     * @param array $config The application's config array to correctly
     * initialize services
     */
    public function __construct(array $config)
    {
        $this->container = new Container();

        // Enable autowiring:
        $this->container->delegate(
            (new ReflectionContainer())->cacheResolutions()
        );

        // Add a Logger to all classes which want a Logger
        $this->container->inflector(LoggerAwareInterface::class, function (LoggerAwareInterface $loggerAware) {
            $loggerAware->setLogger($this->container->get(LoggerInterface::class));
        });

        $this->bindServices($config);
        $this->bindApplicationComponents($config);
        $this->bindRepositories($config);
    }

    public function get($id)
    {
        return $this->container->get($id);
    }

    public function has($id): bool
    {
        return $this->container->has($id);
    }

    private function bindServices(array $config): void
    {
        $this->container->share(LoggerInterface::class, function () {
            $logger = new Logger('schalpoen');
            $logger->pushHandler(new StreamHandler('php://stderr', Logger::WARNING));
            return $logger;
        });
        $this->container->share(EmailGateway::class, SimpleEmailGateway::class);
    }

    private function bindApplicationComponents(array $config): void
    {
        $this->container->share(Parser::class, function () {
            // Manual wiring of subdependency
            // TODO: Find out why recursive autowiring does not work in this case
            // (/publisher/posts will explode due to failed autowiring without this workaround)
            return new BbParser($this->get(SmileyRepository::class));
        });
    }

    private function bindRepositories(array $config): void
    {
        $this->container->share(CaptchaRepository::class, HardcodedCaptchaRepository::class);
        $this->container->share(CommentRepository::class, function () {
            return new DbCommentRepository($this->get(PostRepository::class), $this->get(UserRepository::class));
        });
        $this->container->share(PostRepository::class, function () {
            return new DbPostRepository($this->get(UserRepository::class));
        });
        $this->container->share(PostTagRepository::class, DbPostTagRepository::class);
        $this->container->share(SmileyRepository::class, DbSmileyRepository::class);
        $this->container->share(TagRepository::class, DbTagRepository::class);
        $this->container->share(UserRepository::class, DbUserRepository::class);
    }
}
