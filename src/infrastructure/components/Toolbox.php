<?php

namespace schalpoen\infrastructure\components;

use DateTimeImmutable;
use DateTimeZone;

class Toolbox
{
    private static int $nextUniqueNumber = 1336;

    public static function getUniqueNumber(): int
    {
        return ++self::$nextUniqueNumber;
    }

    public static function getNiceTime(DateTimeImmutable $time): string
    {
        // TODO: Take from browser if possible? Or from user profile if authenticated?
        $displayTimezone = new DateTimeZone(self::getConfig()['app']['timezone']['default']);

        $now = new DateTimeImmutable('now', $displayTimezone);
        $givenTime = $time->setTimezone($displayTimezone);
        $daysAgo = $givenTime->diff($now, true)->days;

        if ($daysAgo === 0) {
            return 'Vandaag, ' . $givenTime->format('H:i');
        }

        if ($daysAgo === 1) {
            return 'Gisteren, ' . $givenTime->format('H:i');
        }

        if ($daysAgo === 2) {
            return 'Eergisteren, ' . $givenTime->format('H:i');
        }

        return $givenTime->format('d-m-Y H:i');
    }

    /**
     * @deprecated Layout presenter should get config and give it to the ViewModel
     */
    public static function showAnalytics(): bool
    {
        return self::getConfig()['analytics']['enabled'];
    }

    /**
     * @deprecated Layout presenter should get config and give it to the ViewModel
     */
    public static function getAnalyticsId(): string
    {
        return self::getConfig()['analytics']['id'];
    }

    public static function getConfig(): array
    {
        return require __DIR__ . '/../../../config/config.php';
    }
}
