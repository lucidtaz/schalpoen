<?php

namespace schalpoen\infrastructure\components;

use schalpoen\domain\models\Tag;

class UiElements
{
    public static function button(string $text, string $icon = '', string $href = '#'): string
    {
        $iconPart = !empty($icon) ? '<i class="material-icons right">' . htmlspecialchars($icon) . '</i>' : '';
        return '<a href="' . htmlspecialchars($href) . '" class="waves waves-light btn">' . htmlspecialchars($text) . $iconPart . '</a>';
    }

    public static function submitButton(string $value, string $name = '', string $icon = 'send', string $formAction = null): string
    {
        if ($formAction !== null) {
            $formActionPart = 'formaction="' . htmlspecialchars($formAction) . '"';
        } else {
            $formActionPart = '';
        }
        return '<button type="submit" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars($value) . '" ' . $formActionPart . 'class="waves waves-light btn">' . htmlspecialchars($value) . '<i class="material-icons right">' . htmlspecialchars($icon) . '</i></button>';
    }

    public static function tag(Tag $tag): string
    {
        return '<div class="chip">' . $tag->href() . '</div>';
    }
}
