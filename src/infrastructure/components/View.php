<?php

namespace schalpoen\infrastructure\components;

use schalpoen\infrastructure\components\view\Layout;

class View
{
    /**
     * Path to the view files for relative access. Ends with /
     */
    public string $viewPath;

    /**
     * Absolute layout file path and name
     * @deprecated Unused, migrate to Layout constructor injection
     */
    public string $deprecatedLayout;

    /**
     * Layout which will render everything around the content
     */
    public ?Layout $layout;

    public function __construct(string $viewPath, ?Layout $layout)
    {
        $this->viewPath = $viewPath;
        $this->layout = $layout;
    }

    /**
     * Render full page including layout
     * @param string $template Relative template file path and name
     * @param array $templateParams Parameters provided to the template
     * @return string
     */
    public function render(string $template, array $templateParams = []): string
    {
        $content = $this->renderPartial($template, $templateParams);
        if ($this->layout === null) {
            return $content;
        }
        return $this->layout->render($content);
    }

    /**
     * Render template only without layout
     * @param string $template Relative template file path and name
     * @param array $templateParams Parameters provided to the template
     * @return string
     */
    public function renderPartial(string $template, array $templateParams = []): string
    {
        extract($templateParams, EXTR_SKIP);

        ob_start();
        require $this->viewPath . $template;
        return ob_get_clean() ?: '';
    }
}
