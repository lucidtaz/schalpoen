<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\authentication;

use schalpoen\infrastructure\components\http\ResponseModification;

class AuthenticationResult
{
    /**
     * @var ResponseModification[]
     */
    public array $responseModifications;

    public AuthenticationState $authenticationState;

    /**
     * @param ResponseModification[] $responseModifications
     */
    public function __construct(array $responseModifications, AuthenticationState $authenticationState)
    {
        $this->responseModifications = $responseModifications;
        $this->authenticationState = $authenticationState;
    }
}
