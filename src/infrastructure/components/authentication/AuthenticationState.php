<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\authentication;

class AuthenticationState
{
    public bool $loggedIn;

    public ?int $userId;

    /**
     * Whether the authentication is above the safe threshold
     *
     * For example an authentication via password is safe while an
     * authentication via remember-me cookie is not safe.
     */
    public bool $safe;

    public function __construct(bool $loggedIn, ?int $userId, bool $safe)
    {
        $this->loggedIn = $loggedIn;
        $this->userId = $userId;
        $this->safe = $safe;
    }

    public static function unauthenticated(): self
    {
        return new self(false, null, false);
    }
}
