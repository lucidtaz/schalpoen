<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\authentication;

use DateInterval;
use DateTimeImmutable;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\exceptions\ValidationException;
use schalpoen\application\persistence\UserRepository;
use schalpoen\infrastructure\components\http\cookies\SetCookie;
use schalpoen\infrastructure\components\http\ResponseModification;
use schalpoen\infrastructure\components\http\SessionData;
use schalpoen\infrastructure\components\PasswordStrategy;
use schalpoen\infrastructure\components\Toolbox;
use schalpoen\infrastructure\models\Credentials;
use schalpoen\infrastructure\models\Token;
use schalpoen\infrastructure\persistence\CredentialsRepository;
use schalpoen\infrastructure\persistence\TokenRepository;

class Authenticator
{
    private UserRepository $userRepository;
    private CredentialsRepository $credentialsRepository;
    private TokenRepository $tokenRepository;

    private PasswordStrategy $passwordStrategy;

    public function __construct(
        UserRepository $userRepository,
        CredentialsRepository $credentialsRepository,
        TokenRepository $tokenRepository,
        PasswordStrategy $passwordStrategy
    ) {
        $this->userRepository = $userRepository;
        $this->credentialsRepository = $credentialsRepository;
        $this->tokenRepository = $tokenRepository;
        $this->passwordStrategy = $passwordStrategy;
    }

    public function handleCookieLogin(ServerRequestInterface $request): AuthenticationResult
    {
        // This function is called on every execution from the core
        // http://fishbowl.pastiche.org/2004/01/19/persistent_login_cookie_best_practice/

        /** @var SessionData $session */
        $session = $request->getAttribute('session');

        if ($session->has('uId')) {
            // This is only needed if we are not currently logged in... Because otherwise we invalidate our cookie&token on each page load while logged in.
            return new AuthenticationResult(
                [],
                new AuthenticationState(
                    true,
                    $session->get('uId'),
                    (bool) $session->get('safeLogin')
                )
            );
        }

        if (!isset($request->getCookieParams()['token'])) {
            return new AuthenticationResult([], AuthenticationState::unauthenticated());
        }

        // If successful, the user logs in using the oneshot token, and then a new oneshot token is created for the future
        // If unsuccessful, the token will be deleted and not regenerated at the end of the method
        [$userId, $token] = explode('|', $request->getCookieParams()['token'], 2);
        try {
            $user = $this->userRepository->retrieve((int) $userId); // Ensure the user exists, will throw if not
            if ($this->tokenIsValid($user->id, $token)) {
                $this->logUserIn($user->id, false, $session);
            } else {
                // Should make bruteforce protection here.
                // For the time being, let's just sleep
                sleep(1);
            }
        } catch (NotFoundException $e) {
            // User not found. Do nothing. Silently let the cookie die, don't bother the user
            trigger_error($e->getMessage(), E_USER_WARNING);
            $cookieDeletion = $this->invalidateTokenAndCookie($token, false);
            return new AuthenticationResult(
                [$cookieDeletion],
                AuthenticationState::unauthenticated()
            );
        }

        // Invalidate the used token, as it's a oneshot login
        $this->invalidateTokenAndCookie($token, true);

        // Generate new token for future logins
        $refreshedToken = $this->createAndPersistToken($user->id);
        $replaceCookie = $this->makeCookieForToken($refreshedToken);

        // Note: we don't return the cookie deleting modification, because the
        // cookie setting modification already deletes the existing one.
        return new AuthenticationResult(
            [$replaceCookie],
            new AuthenticationState(
                true,
                $user->id,
                false
            )
        );
    }

    private function makeCookieForToken(Token $token): ResponseModification
    {
        $now = new DateTimeImmutable('now');
        $lifetime = $token::getLifetime();
        $expire = $now->add($lifetime);

        return new SetCookie(
            'token',
            $token->userId . '|' . $token->value,
            $expire,
            '/',
            '',
            Toolbox::getConfig()['app']['https']['strict'],
            true
        );
    }

    public function handleLogin(string $userName, string $passwd, bool $persistent, ServerRequestInterface $request): AuthenticationResult
    {
        /** @var SessionData $session */
        $session = $request->getAttribute('session');

        $userName = trim($userName);

        try {
            $credentials = $this->credentialsRepository->retrieveByUsername($userName);
        } catch (NotFoundException $e) {
            return new AuthenticationResult([], AuthenticationState::unauthenticated());
        }

        if (!$this->passwordStrategy->verify($passwd, $credentials->passwordHash)) {
            return new AuthenticationResult([], AuthenticationState::unauthenticated());
        }

        if ($this->passwordStrategy->needsRehash($credentials->passwordHash)) {
            $credentials->passwordHash = $this->passwordStrategy->hashPassword($passwd);
            $this->credentialsRepository->update($credentials);
        }

        $this->logUserIn($credentials->userId, true, $session);

        $responseModifications = [];
        if ($persistent) {
            $createdToken = $this->createAndPersistToken($credentials->userId);
            $cookie = $this->makeCookieForToken($createdToken);
            $responseModifications = [$cookie];
        }

        return new AuthenticationResult(
            $responseModifications,
            new AuthenticationState(
                true,
                $credentials->userId,
                true
            )
        );
    }

    public function handleRegistration(
        int $userId,
        string $username,
        string $password,
        bool $safe,
        ServerRequestInterface $request
    ): AuthenticationResult {
        /** @var SessionData $session */
        $session = $request->getAttribute('session');

        $passwordHash = $this->passwordStrategy->hashPassword($password);
        $this->saveCredentials($userId, $username, $passwordHash);
        $this->logUserIn($userId, $safe, $session);
        return new AuthenticationResult([], new AuthenticationState(true, $userId, $safe));
    }

    /**
     * @throws ValidationException If the username is already taken
     */
    private function saveCredentials(int $userId, string $username, string $passwordHash): void
    {
        if (!$this->credentialsRepository->isUsernameFree($username)) {
            throw new ValidationException(['The username is already taken.']);
        }
        $credentials = Credentials::make($userId, $username, $passwordHash);
        $this->credentialsRepository->insert($credentials);
    }

    private function logUserIn(int $userId, bool $safe, SessionData $session): void
    {
        $user = $this->userRepository->retrieve($userId);

        $session->set('uId', $user->getId());
        $session->set('safeLogin', $safe);
        $user->updateLoginTime();
        $this->userRepository->update($user);
    }

    /**
     * @return ResponseModification[]
     */
    public function handleLogout(ServerRequestInterface $request): array
    {
        /** @var SessionData $session */
        $session = $request->getAttribute('session');

        $responseModifications = [];

        if (array_key_exists('token', $request->getCookieParams())) {
            [$userId, $token] = explode('|', $request->getCookieParams()['token'], 2);
            $cookieDeletion = $this->invalidateTokenAndCookie($token, true); // The current token, which is for a future session, not for this one, gets deleted
            $responseModifications[] = $cookieDeletion;
        }

        $session->remove('uId');

        return $responseModifications;
    }

    private function createAndPersistToken(int $userId): Token
    {
        $value = uniqid('', true);

        $token = new Token($userId, md5($value));
        $this->tokenRepository->insert($token);

        return $token;
    }

    private function invalidateTokenAndCookie(string $tokenValue, bool $userIsAuthenticated): ResponseModification
    {
        // If the user is logged in, delete the entry from the token table and delete the cookie
        // If the user did NOT successfully login, only delete the cookie, as the token is not valid (and could be spoofed to wipe the token table otherwise)

        if ($userIsAuthenticated) {
            // Delete table entry
            try {
                $token = $this->tokenRepository->retrieveByValue($tokenValue);
                $this->tokenRepository->delete($token);
            } catch (NotFoundException $e) {
                // Token not found, nothing to do
            }
        }

        $now = new DateTimeImmutable('now');
        $oneHour = new DateInterval('PT1H');
        $oneHourAgo = $now->sub($oneHour);

        return new SetCookie(
            'token',
            'deleted',
            $oneHourAgo,
            null, // Delete for any path
            null, // Delete for any domain
            false, // Delete for http and https
            false // Delete regardless of httpOnly state
        );
    }

    private function tokenIsValid(int $userId, string $tokenValue): bool
    {
        try {
            $token = $this->tokenRepository->retrieveByValue($tokenValue);
        } catch (NotFoundException $e) {
            return false;
        }
        return $token->userId === $userId && $token->isValid();
    }
}
