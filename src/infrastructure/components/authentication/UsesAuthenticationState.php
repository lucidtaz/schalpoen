<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\authentication;

use BadMethodCallException;
use Psr\Http\Message\ServerRequestInterface;

trait UsesAuthenticationState
{
    protected function setAuthenticationState(ServerRequestInterface $request, AuthenticationState $authenticationState): ServerRequestInterface
    {
        return $request->withAttribute('authentication-state', $authenticationState);
    }

    protected function getAuthenticationState(ServerRequestInterface $request): AuthenticationState
    {
        $authenticationState = $request->getAttribute('authentication-state');
        if (!($authenticationState instanceof AuthenticationState)) {
            throw new BadMethodCallException('No authentication state in request. Did you use the middleware?');
        }
        return $authenticationState;
    }
}
