<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\formatting;

use schalpoen\application\parser\Parser;
use schalpoen\domain\models\Post;

/**
 * Opengraph Post Formatter
 */
class OgPostFormatter implements PostFormatter
{
    private Parser $parser;

    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    public function getIntroduction(Post $post): string
    {
        $introduction = $post->getIntroduction();
        return $this->parser->blockParse($introduction);
    }

    public function getDate(Post $post): string
    {
        //ISO 8601, Example: 2017-02-24T09:41:26+00:00
        if ($post->isPublished() && $post->getPublishedAt() !== null) {
            return $post->getPublishedAt()->format('c');

        }
        return date('c');
    }
}
