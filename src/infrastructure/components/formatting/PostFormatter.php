<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\formatting;

use schalpoen\domain\models\Post;

interface PostFormatter
{
    public function getIntroduction(Post $post): string;
    public function getDate(Post $post): string;
}
