<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\formatting;

use LogicException;
use schalpoen\application\parser\Parser;
use schalpoen\domain\models\Post;

class RssPostFormatter implements PostFormatter
{
    private Parser $parser;

    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    public function getIntroduction(Post $post): string
    {
        $introduction = $post->getIntroduction();
        $formattedIntroduction = $this->parser->blockParse($introduction);

        if (\strlen($introduction) < \strlen($post->getText())) {
            $formattedIntroduction .= ' Lees het hele artikel op de website.';
        }

        return $formattedIntroduction;
    }

    public function getDate(Post $post): string
    {
        if ($post->getPublishedAt() === null) {
            throw new LogicException('RSS Formatter tried to format an unpublished post.');
        }
        //RFC 2822, Example: Thu, 21 Dec 2000 16:01:07 +0200
        return $post->getPublishedAt()->format('r');
    }
}
