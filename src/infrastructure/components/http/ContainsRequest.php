<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http;

use Psr\Http\Message\ServerRequestInterface;

interface ContainsRequest
{
    public function getRequest(): ServerRequestInterface;
}
