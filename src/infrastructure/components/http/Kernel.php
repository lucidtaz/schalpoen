<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http;

use Closure;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

class Kernel implements RequestHandlerInterface
{
    private Router $router;

    /**
     * @var MiddlewareInterface[]
     */
    private array $middlewares = [];

    private ?Closure $handleException;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @throws Throwable Any failure thrown in case there is no exception
     * handler or the exception handler itself throws an exception.
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $chain = $this->buildHandlerChain();

        try {
            return $chain->handle($request);
        } catch (Throwable $e) {
            if ($this->handleException === null) {
                throw $e;
            }

            $handler = $this->handleException;

            if ($e instanceof ContainsRequest) {
                // The exception might contain a modified response (i.e. with
                // added attribute like AuthenticationState), so use that one
                // instead of the original request when rendering the exception
                // page.
                return $handler($e, $e->getRequest());
            }

            return $handler($e, $request);
        }
    }

    private function buildHandlerChain(): RequestHandlerInterface
    {
        $chain = $this->router; // Router will be the final link of the chain

        foreach (array_reverse($this->middlewares) as $middleware) {
            $chain = new MiddlewareHandler($middleware, $chain);
        }

        return $chain;
    }

    public function addMiddleware(MiddlewareInterface $middleware): void
    {
        $this->middlewares[] = $middleware;
    }

    /**
     * @param callable $handler Receiving a Throwable and a
     * ServerRequestInterface, returning a ResponseInterface
     */
    public function setExceptionHandler(callable $handler): void
    {
        $this->handleException = $handler;
    }
}
