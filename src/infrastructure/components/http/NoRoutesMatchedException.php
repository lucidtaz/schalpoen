<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http;

use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Throwable;

class NoRoutesMatchedException extends RuntimeException implements ContainsRequest
{
    private ServerRequestInterface $request;

    public function __construct(ServerRequestInterface $request, Throwable $previous = null)
    {
        parent::__construct('No routes matched for ' . $request->getUri()->getPath(), 0, $previous);
        $this->request = $request;
    }

    public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }
}
