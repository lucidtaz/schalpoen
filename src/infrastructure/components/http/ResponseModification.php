<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http;

use Psr\Http\Message\ResponseInterface;

interface ResponseModification
{
    /**
     * @param ResponseInterface $response The Response to apply the modification on
     * @return ResponseInterface The modified Response
     */
    public function apply(ResponseInterface $response): ResponseInterface;
}
