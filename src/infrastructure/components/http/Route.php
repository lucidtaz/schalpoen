<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http;

use Closure;
use Psr\Http\Message\ServerRequestInterface;

class Route
{
    /**
     * HTTP Method to match against
     */
    private string $method;

    /**
     * Regular expression to match the request uri against
     */
    private string $pattern;

    private Closure $action;

    public function __construct(string $method, string $pattern, callable $action)
    {
        $this->method = $method;
        $this->pattern = $pattern;
        $this->action = $action;
    }

    public function matches(ServerRequestInterface $request): bool
    {
        return ($request->getMethod() === $this->method
            && preg_match("#^$this->pattern#", $request->getUri()->getPath()));
    }

    /**
     * Parse the request using the pattern and return the matched values
     */
    public function parseRequest(ServerRequestInterface $request): ?array
    {
        $matches = null;
        if (!preg_match("#^$this->pattern#", $request->getUri()->getPath(), $matches)) {
            return null;
        }
        return $matches;
    }

    /**
     * Get the route's bound action
     *
     * It takes a ServerRequestInterface, an associative array of route-matched
     * parameters and returns a ResponseInterface.
     */
    public function getAction(): callable
    {
        return $this->action;
    }
}
