<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Router implements RequestHandlerInterface
{
    private ContainerInterface $container;

    /**
     * @var Route[]
     */
    private array $routes = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function addRoute(string $method, string $pattern, string $controllerClass, string $actionName): void
    {
        $this->routes[] = $this->createRouteForControllerAction($method, $pattern, $controllerClass, $actionName);
    }

    private function createRouteForControllerAction(string $method, string $pattern, string $controllerClass, string $actionName): Route
    {
        $action = function (ServerRequestInterface $request, array $params) use ($controllerClass, $actionName): ResponseInterface {
            $controller = $this->container->get($controllerClass);
            return $controller->$actionName($request, $params);
        };
        return new Route($method, $pattern, $action);
    }

    /**
     * Send the request to the correct controller
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface The resulting response
     * @throws NoRoutesMatchedException
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $controllerClass = null;
        $action = null;
        $params = null;

        foreach ($this->routes as $route) {
            if ($route->matches($request)) {
                $params = $route->parseRequest($request);
                $action = $route->getAction();
                return $action($request, $params);
            }
        }

        throw new NoRoutesMatchedException($request);
    }
}
