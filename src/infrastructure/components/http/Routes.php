<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http;

use schalpoen\infrastructure\controllers\ArchiveController;
use schalpoen\infrastructure\controllers\AuthorController;
use schalpoen\infrastructure\controllers\BuildAThingController;
use schalpoen\infrastructure\controllers\CommentController;
use schalpoen\infrastructure\controllers\CrazyCoinMachineController;
use schalpoen\infrastructure\controllers\PostController;
use schalpoen\infrastructure\controllers\PublisherController;
use schalpoen\infrastructure\controllers\RssController;
use schalpoen\infrastructure\controllers\SiteController;
use schalpoen\infrastructure\controllers\TagController;
use schalpoen\infrastructure\controllers\UserController;

class Routes
{
    public function register(Router $router): void
    {
        $any = '(.+?)';
        $id = '(?<id>\d+)';

        $router->addRoute('GET', '/$', SiteController::class, 'actionIndex');
        $router->addRoute('GET', '/login', SiteController::class, 'actionLoginForm');
        $router->addRoute('POST', '/login', SiteController::class, 'actionLogin');
        $router->addRoute('GET', '/logout', SiteController::class, 'actionLogout');
        $router->addRoute('GET', '/register', SiteController::class, 'actionRegistrationForm');
        $router->addRoute('POST', '/register', SiteController::class, 'actionRegister');

        $router->addRoute('GET', '/profile', UserController::class, 'actionEditProfile');
        $router->addRoute('POST', '/profile', UserController::class, 'actionUpdateProfile');
        $router->addRoute('GET', "/user/$id/$any", UserController::class, 'actionView'); // Backwards compatibility of published links
        $router->addRoute('GET', "/users/$id/$any", UserController::class, 'actionView');

        $router->addRoute('GET', '/author/drafts', AuthorController::class, 'actionShowActions');
        $router->addRoute('GET', '/author/posts/new', AuthorController::class, 'actionWriteDraft');
        $router->addRoute('POST', '/author/posts/new/preview', AuthorController::class, 'actionWriteDraft');
        $router->addRoute('GET', "/author/posts/$id", AuthorController::class, 'actionEditDraft');
        $router->addRoute('POST', "/author/posts/$id/preview", AuthorController::class, 'actionEditDraft');
        $router->addRoute('POST', "/author/posts/$id/finalize", AuthorController::class, 'actionFinalize');
        $router->addRoute('POST', "/author/posts/$id/unfinalize", AuthorController::class, 'actionUnfinalize');
        $router->addRoute('POST', "/author/posts/$id/delete", AuthorController::class, 'actionDelete');
        $router->addRoute('POST', "/author/posts/$id", AuthorController::class, 'actionUpdateDraft');
        $router->addRoute('POST', '/author/posts', AuthorController::class, 'actionCreateDraft');

        $router->addRoute('GET', "/publisher/posts/$id/tags", PublisherController::class, 'actionChooseTag');
        $router->addRoute('POST', "/publisher/posts/$id/tags", PublisherController::class, 'actionTag');
        $router->addRoute('POST', "/publisher/posts/$id/untag", PublisherController::class, 'actionUntag');
        $router->addRoute('POST', "/publisher/posts/$id/reject", PublisherController::class, 'actionReject');
        $router->addRoute('POST', "/publisher/posts/$id/publish", PublisherController::class, 'actionPublish');
        $router->addRoute('GET', '/publisher/posts', PublisherController::class, 'actionShowActions');

        $router->addRoute('GET', "/post/$id/$any", PostController::class, 'actionView'); // Backwards compatibility of published links
        $router->addRoute('GET', "/posts/$id/$any", PostController::class, 'actionView');
        $router->addRoute('POST', "/posts/$id/comment", CommentController::class, 'actionCreate');

        $router->addRoute('GET', "/tags/$id/$any", TagController::class, 'actionView');

        $router->addRoute('GET', '/rss', RssController::class, 'actionIndex');

        $router->addRoute('GET', '/archive', ArchiveController::class, 'actionIndex');

        $router->addRoute('GET', "/build-a-thing/$any", BuildAThingController::class, 'actionPrivacy');
        $router->addRoute('GET', "/crazy-coin-machine/$any", CrazyCoinMachineController::class, 'actionPrivacy');
    }
}
