<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\cookies;

use DateTimeInterface;
use Psr\Http\Message\ResponseInterface;
use schalpoen\infrastructure\components\http\ResponseModification;

class SetCookie implements ResponseModification
{
    private string $name;

    private string $value;

    private ?DateTimeInterface $expiresAt;

    private ?string $path;

    private ?string $domain;

    private bool $secure;

    private bool $httpOnly;

    public function __construct(
        string $name,
        string $value,
        ?DateTimeInterface $expiresAt,
        ?string $path,
        ?string $domain,
        bool $secure,
        bool $httpOnly
    ) {
        $this->name = $name;
        $this->value = $value;
        $this->expiresAt = $expiresAt;
        $this->path = $path;
        $this->domain = $domain;
        $this->secure = $secure;
        $this->httpOnly = $httpOnly;
    }

    public function apply(ResponseInterface $response): ResponseInterface
    {
        return $response->withAddedHeader('Set-Cookie', $this->formatHeaderString());
    }

    private function formatHeaderString() : string
    {
        $headerParts = [];

        $headerParts[] = sprintf('%s=%s', $this->name, urlencode($this->value));

        if ($this->expiresAt !== null) {
            $headerParts[] = sprintf(
                'expires=%s',
                $this->expiresAt->format(DateTimeInterface::COOKIE)
            );
        }
        if ($this->path !== null) {
            $headerParts[] = sprintf('path=%s', $this->path);
        }
        if ($this->domain !== null) {
            $headerParts[] = sprintf('domain=%s', $this->domain);
        }
        if ($this->secure) {
            $headerParts[] = 'secure';
        }
        if ($this->httpOnly) {
            $headerParts[] = 'httponly';
        }
        return implode('; ', $headerParts);
    }
}
