<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\factories;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\viewmodels\site\ErrorViewModel;

/**
 * @property LayoutFactory $layoutFactory
 */
trait MakesCannedResponses
{
    protected function createNotAuthenticatedResponse(ServerRequestInterface $request, string $page): ResponseInterface
    {
        $view = new View(
            __DIR__ . '/../../../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        $model = new ErrorViewModel(
            'Signin needed',
            'You need to sign in before accessing ' . htmlspecialchars($page) . '.'
        );

        return new Response(401, [], $view->render('error.php', [
            'model' => $model,
        ]));
    }

    protected function createForbiddenResponse(ServerRequestInterface $request, string $page): ResponseInterface
    {
        $view = new View(
            __DIR__ . '/../../../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        $model = new ErrorViewModel(
            'Not authorized',
            'You don\'t have access to ' . htmlspecialchars($page) . '.'
        );

        return new Response(403, [], $view->render('error.php', [
            'model' => $model,
        ]));
    }

    protected function createNotFoundResponse(ServerRequestInterface $request, string $page): ResponseInterface
    {
        $view = new View(
            __DIR__ . '/../../../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        $model = new ErrorViewModel(
            'Not found',
            'The page ' . htmlspecialchars($page) . ' cannot be found.'
        );

        return new Response(404, [], $view->render('error.php', [
            'model' => $model,
        ]));
    }

    protected function createInternalServerErrorResponse(
        ServerRequestInterface $request,
        ?string $message
    ): ResponseInterface {
        if ($message === null) {
            $displayMessage = 'The server encountered a request while handling your request.';
        } else {
            $displayMessage = 'The server encountered a request while handling your request. The reported error is: ' . $message;
        }

        return new Response(500, [], $displayMessage);
    }
}
