<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AppendExecutionTime implements MiddlewareInterface
{
    private ?float $executionStarted;

    /**
     * @param ?float $executionStarted If accurately known, give the earliest
     * time (using microtime(true)) of this execution you can. This enables you
     * to also profile application bootstrapping.
     */
    public function __construct(?float $executionStarted)
    {
        $this->executionStarted = $executionStarted;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $timeStarted = microtime(true);
        $response = $handler->handle($request);
        $timeFinished = microtime(true);

        if ($this->executionStarted !== null) {
            $bootElapsed = $timeStarted - $this->executionStarted;
            $totalElapsed = $timeFinished - $this->executionStarted;
            $response = $response->withAddedHeader('X-Schalpoen-Application-Bootstrap-Duration', round($bootElapsed * 1000) . 'ms')
                ->withAddedHeader('X-Schalpoen-Application-Total-Duration', round($totalElapsed * 1000) . 'ms');
        }

        $elapsed = $timeFinished - $timeStarted;
        return $response->withAddedHeader('X-Schalpoen-Request-Handling-Duration', round($elapsed * 1000) . 'ms');
    }
}
