<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use schalpoen\infrastructure\components\authentication\Authenticator;
use schalpoen\infrastructure\components\authentication\UsesAuthenticationState;

/**
 * If the user has a remember-me cookie, try to login with it
 */
class CookieLogin implements MiddlewareInterface
{
    use UsesAuthenticationState;

    private Authenticator $authenticator;

    public function __construct(Authenticator $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $authenticationResult = $this->authenticator->handleCookieLogin($request);

        $response = $handler->handle(
            $this->setAuthenticationState($request, $authenticationResult->authenticationState)
        );

        foreach ($authenticationResult->responseModifications as $responseModification) {
            $response = $responseModification->apply($response);
        }

        return $response;
    }
}
