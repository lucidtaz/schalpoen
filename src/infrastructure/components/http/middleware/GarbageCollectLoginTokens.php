<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use DateTimeImmutable;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use schalpoen\infrastructure\models\Token;
use schalpoen\infrastructure\persistence\TokenRepository;

class GarbageCollectLoginTokens implements MiddlewareInterface
{
    private TokenRepository $tokenRepository;

    public function __construct()
    {
        $this->tokenRepository = new TokenRepository();
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (random_int(1, 100) === 1) {
            $now = new DateTimeImmutable('now');
            $threshold = $now->sub(Token::getLifetime());
            $this->tokenRepository->deleteOlderThan($threshold);
        }

        return $handler->handle($request);
    }
}
