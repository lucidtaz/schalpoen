<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use schalpoen\infrastructure\components\Db;

class OpenDatabaseConnection implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        Db::connect();
        return $handler->handle($request);
    }
}
