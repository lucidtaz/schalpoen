<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use schalpoen\infrastructure\components\http\SessionData;

/**
 * Packs $_SESSION into a Request attribute and unpacks it afterwards
 */
class PackSessionData implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $session = self::packSessionVariables();
        $request = $request->withAttribute('session', $session);

        $response = $handler->handle($request);

        self::unpackSessionVariables($session);

        return $response;
    }

    private static function packSessionVariables(): SessionData
    {
        $session = new SessionData();
        foreach ($_SESSION as $key => $value) {
            $session->set($key, $value);
        }
        return $session;
    }

    private static function unpackSessionVariables(SessionData $session): void
    {
        foreach ($session->all() as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }
}
