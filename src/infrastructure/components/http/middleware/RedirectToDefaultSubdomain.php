<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Redirect schalpoen.nl to www.schalpoen.nl
 */
class RedirectToDefaultSubdomain implements MiddlewareInterface
{
    private string $basePath;

    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $enabled = strpos($this->basePath, 'www') !== false;
        $triggered = strpos($request->getHeaderLine('Host'), 'www') === false;

        if ($enabled && $triggered) {
            $uri = $request->getUri();
            $location = $this->basePath . $uri->getPath() . $uri->getQuery() . $uri->getFragment();

            return new Response(
                301,
                ['Location' => $location],
                'Redirecting to ' . $location
            );
        }

        return $handler->handle($request);
    }
}
