<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Sets a Content Type if none exist yet
 */
class SetContentType implements MiddlewareInterface
{
    private string $contentType;

    public function __construct(string $contentType)
    {
        $this->contentType = $contentType;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        if ($response->hasHeader('Content-Type')) {
            return $response;
        }
        return $response->withAddedHeader('Content-Type', $this->contentType);
    }
}
