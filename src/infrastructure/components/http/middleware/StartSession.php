<?php

/**
 * This file is adapted from https://github.com/ellipsephp/session
 * Original file: Copyright (c) Pierre Mallinjoud
 * Later changes: Copyright (c) Thijs Zumbrink
 *
 * Reason for adaptation is:
 * - Different use case of handling multiple requests in one execution
 * - PHPUnit may already send output to stdout, triggering a bug in PHP's
 *   session_name() and in PHP's session_start($options) with $options
 *   specified. The same is true for regular multi-request execution sending
 *   each response to stdout.
 * - The original class has too many features which are not needed in this
 *   project
 *
 * Changes aren't sent upstream (yet) because the project doesn't seem active,
 * and by moving the code inside this project it saves one dependency.
 */

declare(strict_types=1);

namespace schalpoen\infrastructure\components\http\middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use RuntimeException;

class StartSession implements MiddlewareInterface
{
    /**
     * The date format for the session headers.
     */
    private const DATE_FORMAT = 'D, d-M-Y H:i:s T';

    /**
     * The date for the expired value of cache limiter header.
     */
    private const EXPIRED = 'Thu, 19 Nov 1981 08:52:00 GMT';

    /**
     * The user defined session cookie params.
     */
    private array $cookie_params;

    /**
     * Set up a session middleware with optional cookie params.
     */
    public function __construct(array $cookie_params = [])
    {
        $this->cookie_params = $cookie_params;
    }

    /**
     * Manage session for the given request handler.
     *
     * @throws RuntimeException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Fail when sessions are disabled.
        if (session_status() === PHP_SESSION_DISABLED) {
            throw new RuntimeException('Sessions are disabled.');
        }

        // Fail when session is already started.
        if (session_status() === PHP_SESSION_ACTIVE) {
            throw new RuntimeException('Session is already started.');
        }

        // Set the user defined session parameters.
        if ($this->cookie_params !== []) {
            $this->setCookieParams($this->cookie_params);
        }

        $name = session_name();

        // Set the session id.
        $id = $request->getCookieParams()[$name] ?? '';
        session_id($id);

        // Start the session
        if (session_start()) {
            // Get a response from the request handler.
            $response = $handler->handle($request);

            // Save the session data and close the session.
            session_write_close();

            // Return a response with session headers attached.
            return $this->attachSessionHeaders($response);
        }

        throw new RuntimeException('Session failed to start.');
    }

    /**
     * Set the given session cookie params.
     */
    private function setCookieParams(array $cookie_params): void
    {
        $params = array_merge(session_get_cookie_params(), array_change_key_case($cookie_params));

        session_set_cookie_params(
            $params['lifetime'],
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httponly']
        );
    }

    /**
     * Attach the session headers to the given response.
     *
     * Trying to emulate the default php 7.0 headers generations. Adapted from
     * Relay.Middleware SessionHeadersHandler.
     *
     * @see https://github.com/relayphp/Relay.Middleware/blob/1.x/src/SessionHeadersHandler.php
     */
    private function attachSessionHeaders(ResponseInterface $response): ResponseInterface
    {
        $time = time();

        $response = $this->attachCacheLimiterHeader($response, $time);
        $response = $this->attachSessionCookie($response, $time);

        return $response;
    }

    /**
     * Attach a session cache limiter header to the given response.
     */
    private function attachCacheLimiterHeader(ResponseInterface $response, int $time): ResponseInterface
    {
        $cache_limiter = session_cache_limiter();

        switch ($cache_limiter) {
            case 'public':
                return $this->attachPublicCacheLimiterHeader($response, $time);
            case 'private':
                return $this->attachPrivateCacheLimiterHeader($response, $time);
            case 'private_no_expire':
                return $this->attachPrivateNoExpireCacheLimiterHeader($response, $time);
            case 'nocache':
                return $this->attachNocacheCacheLimiterHeader($response);
            default:
                return $response;
        }
    }

    /**
     * Attach a public cache limiter header to the given response.
     *
     * @see https://github.com/php/php-src/blob/PHP-7.0/ext/session/session.c#L1267-L1284
     */
    private function attachPublicCacheLimiterHeader(ResponseInterface $response, int $time): ResponseInterface
    {
        $cache_expire = session_cache_expire();

        $max_age = $cache_expire * 60;
        $expires = gmdate(self::DATE_FORMAT, $time + $max_age);
        $cache_control = "public, max-age={$max_age}";
        $last_modified = gmdate(self::DATE_FORMAT, $time);

        return $response
            ->withAddedHeader('Expires', $expires)
            ->withAddedHeader('Cache-Control', $cache_control)
            ->withAddedHeader('Last-Modified', $last_modified);
    }

    /**
     * Attach a private cache limiter header to the given response.
     *
     * @see https://github.com/php/php-src/blob/PHP-7.0/ext/session/session.c#L1297-L1302
     */
    private function attachPrivateCacheLimiterHeader(ResponseInterface $response, int $time): ResponseInterface
    {
        /** @var ResponseInterface $modified */
        $modified = $response->withAddedHeader('Expires', self::EXPIRED);

        return $this->attachPrivateNoExpireCacheLimiterHeader($modified, $time);
    }

    /**
     * Attach a private_no_expire cache limiter header to the given response.
     *
     * @see https://github.com/php/php-src/blob/PHP-7.0/ext/session/session.c#L1286-L1295
     */
    private function attachPrivateNoExpireCacheLimiterHeader(ResponseInterface $response, int $time): ResponseInterface
    {
        $cache_expire = session_cache_expire();

        $max_age = $cache_expire * 60;
        $cache_control = "private, max-age={$max_age}";
        $last_modified = gmdate(self::DATE_FORMAT, $time);

        return $response
            ->withAddedHeader('Cache-Control', $cache_control)
            ->withAddedHeader('Last-Modified', $last_modified);
    }

    /**
     * Attach a nocache cache limiter header to the given response.
     *
     * @see https://github.com/php/php-src/blob/PHP-7.0/ext/session/session.c#L1304-L1314
     */
    private function attachNocacheCacheLimiterHeader(ResponseInterface $response): ResponseInterface
    {
        return $response
            ->withAddedHeader('Expires', self::EXPIRED)
            ->withAddedHeader('Cache-Control', 'no-store, no-cache, must-revalidate')
            ->withAddedHeader('Pragma', 'no-cache');
    }

    /**
     * Attach a session cookie to the given response.
     *
     * @see https://github.com/php/php-src/blob/PHP-7.0/ext/session/session.c#L1402-L1476
     */
    private function attachSessionCookie(ResponseInterface $response, int $time): ResponseInterface
    {
        // Get the session id, name and the cookie options.
        $id = session_id();
        $name = session_name();
        $options = array_merge(session_get_cookie_params(), $this->cookie_params);

        // Create a cookie header.
        $header = urlencode($name) . '=' . urlencode($id);

        if ($options['lifetime'] > 0) {
            $expires = gmdate(self::DATE_FORMAT, $time + $options['lifetime']);
            $header .= "; expires={$expires}; max-age={$options['lifetime']}";
        }

        if ($options['path']) {
            $header .= "; path={$options['path']}";
        }
        if ($options['domain']) {
            $header .= "; domain={$options['domain']}";
        }
        if ($options['secure']) {
            $header .= '; secure';
        }
        if ($options['httponly']) {
            $header .= '; HttpOnly';
        }

        // Return a new response with the cookie header.
        return $response->withAddedHeader('Set-Cookie', $header);
    }
}
