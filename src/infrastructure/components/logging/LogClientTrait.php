<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\logging;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Add this to your class to enable it to log
 *
 * It's very similar to PSR's LoggerAwareTrait with one important difference:
 * the logger will always be available, even if the calling code doesn't inject
 * it. In that case it will fall back to NullLogger but it won't trigger a null
 * pointer exception.
 *
 * The shortcoming of LoggerAwareTrait is that even though the DI container can
 * always automatically call setLogger(), so the class can use $this->logger to
 * be able to log, when *not* using the DI container (for example in testing, or
 * for writing portable code) it will crash.
 *
 * By falling back to NullLogger if no logger was injected, it becomes a caller
 * concern that the implementation doesn't need to worry about, similar to
 * constructor injection but optional.
 */
trait LogClientTrait
{
    /**
     * Configured logger
     *
     * This property is "trait-private" if such a concept would exist. Do not
     * call this yourself, use getLogger().
     */
    private ?LoggerInterface $logger = null;

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->logger ?? new NullLogger();
    }
}
