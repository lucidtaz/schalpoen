<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\view;

/**
 * Layout without any of the blog menu items
 */
class IsolatedLayout implements Layout
{
    /**
     * Non-HTML-encoded page title
     */
    private ?string $title = null;

    /**
     * Non-HTML-encoded page description
     */
    private ?string $description = null;

    /**
     * @var string[] Non-HTML-encoded page keywords
     */
    private array $keywords = [];

    /**
     * @var string[] Extra HTML meta headers
     */
    private array $extraHeaders = [];

    public function render(string $content): string
    {
        ob_start();
        require __DIR__ . '/../../views/layouts/isolated.php';
        return ob_get_clean() ?: '';
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param string[] $keywords
     */
    public function addKeywords(array $keywords): void
    {
        $this->keywords = array_merge($this->keywords, $keywords);
    }

    /**
     * @param string[] $headers
     */
    public function addHtmlHeaders(array $headers): void
    {
        $this->extraHeaders = array_merge($this->extraHeaders, $headers);
    }
}
