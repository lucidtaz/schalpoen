<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\view;

interface Layout
{
    /**
     * Produce the output to be sent to the client
     *
     * @param string $content Page-specific contents for in the content slot.
     */
    public function render(string $content): string;

    public function setTitle(string $title): void;

    public function setDescription(string $description): void;

    /**
     * @param string[] $keywords
     */
    public function addKeywords(array $keywords): void;

    /**
     * @param string[] $header
     */
    public function addHtmlHeaders(array $header): void;
}
