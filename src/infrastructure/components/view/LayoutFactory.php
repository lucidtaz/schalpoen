<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\view;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\TagRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\infrastructure\components\authentication\UsesAuthenticationState;
use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\viewmodels\layouts\MainLayoutViewModel;

class LayoutFactory
{
    use UsesAuthenticationState;

    public const CODE_MAIN = 'main';
    public const CODE_ISOLATED = 'isolated';

    private UserRepository $userRepository;

    private PostRepository $postRepository;

    private TagRepository $tagRepository;

    public function __construct(
        UserRepository $userRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository
    ) {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Make a Layout object which is used to render the result for the client
     *
     * The return value of this method is a potentially stateful object which is
     * only valid for the current request. It can for example contain
     * information that should only be shown to the authenticated user, like a
     * menu with personalized items.
     *
     * @param string $code One of the CODE_* constants of this class
     */
    public function make(ServerRequestInterface $request, string $code = self::CODE_MAIN): Layout
    {
        if ($code === self::CODE_MAIN) {
            return new MainLayout($this->presentMainLayout($request));
        }
        if ($code === self::CODE_ISOLATED) {
            return new IsolatedLayout();
        }
        throw new InvalidArgumentException('Unknown layout code' . $code);
    }

    private function presentMainLayout(ServerRequestInterface $request): MainLayoutViewModel
    {
        $lastPosts = $this->postRepository->retrieveLastPublishedPosts(10);
        $popularTags = $this->tagRepository->retrievePopular(20);

        $lastPostLinks = array_map(fn (Post $post) => $post->getShortDate() . ' ' . $post->href(), $lastPosts);
        $popularTagLinks = array_map(fn (Tag $tag) => UiElements::tag($tag), $popularTags);

        $userId = $this->getAuthenticationState($request)->userId;

        if ($userId === null) {
            return new MainLayoutViewModel(
                false,
                '',
                false,
                '',
                false,
                '',
                $lastPostLinks,
                $popularTagLinks,
            );
        }
        $logUser = $this->userRepository->retrieve($userId);
        $nDrafts = $this->postRepository->countEditable($logUser);
        $nUnpublishedPosts = $this->postRepository->countPublishable();

        if ($nDrafts > 0) {
            $draftsLink = 'Concepten (' . $nDrafts . ')';
        } else {
            $draftsLink = 'Concepten';
        }

        if ($nUnpublishedPosts > 0) {
            $publishLink = 'Publiceren <span style="color: red;">(' . $nUnpublishedPosts . ')</span>';
        } else {
            $publishLink = 'Publiceren';
        }

        return new MainLayoutViewModel(
            true,
            htmlspecialchars($logUser->getName()),
            true,
            $draftsLink,
            true,
            $publishLink,
            $lastPostLinks,
            $popularTagLinks
        );
    }
}
