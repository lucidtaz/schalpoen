<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\components\view;

use schalpoen\infrastructure\viewmodels\layouts\MainLayoutViewModel;

/**
 * Main blog layout
 */
class MainLayout implements Layout
{
    /**
     * Non-HTML-encoded page title
     */
    private ?string $title = null;

    /**
     * Non-HTML-encoded page description
     */
    private ?string $description = null;

    /**
     * @var string[] Non-HTML-encoded page keywords
     */
    private array $keywords = [];

    /**
     * @var string[] Extra HTML meta headers
     */
    private array $extraHeaders = [];

    /**
     * ViewModel which contains fully HTML-encoded values
     */
    private MainLayoutViewModel $model;

    public function __construct(MainLayoutViewModel $model)
    {
        /** @noinspection UnusedConstructorDependenciesInspection  It's used by the template*/
        $this->model = $model;
    }

    public function render(string $content): string
    {
        ob_start();
        require __DIR__ . '/../../views/layouts/main.php';
        return ob_get_clean() ?: '';
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param string[] $keywords
     */
    public function addKeywords(array $keywords): void
    {
        $this->keywords = array_merge($this->keywords, $keywords);
    }

    /**
     * @param string[] $headers
     */
    public function addHtmlHeaders(array $headers): void
    {
        $this->extraHeaders = array_merge($this->extraHeaders, $headers);
    }
}
