<?php

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\infrastructure\components\View;
use schalpoen\application\interactors\GetPostArchive\GetPostArchive;
use schalpoen\application\persistence\TagRepository;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\presenters\ArchivePresenter;

class ArchiveController
{
    private LayoutFactory $layoutFactory;

    private TagRepository $tagRepository;

    private GetPostArchive $getPostArchive;

    private ArchivePresenter $presenter;

    public function __construct(
        LayoutFactory $layoutFactory,
        TagRepository $tagRepository,
        GetPostArchive $getPostArchive,
        ArchivePresenter $presenter
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->tagRepository = $tagRepository;
        $this->getPostArchive = $getPostArchive;
        $this->presenter = $presenter;
    }

    public function actionIndex(ServerRequestInterface $request): ResponseInterface
    {
        $responseModel = $this->getPostArchive->run();
        // TODO: Move tag retrieval to interactor?
        $popularTags = $this->tagRepository->retrievePopular(20);

        $viewModel = $this->presenter->run($responseModel, $popularTags);

        $view = new View(
            __DIR__ . '/../views/archive/',
            $this->layoutFactory->make($request)
        );
        return new Response(200, [], $view->render('index.php', [
            'model' => $viewModel,
        ]));
    }
}
