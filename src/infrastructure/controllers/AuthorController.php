<?php

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\interactors\EditDraft\EditDraft;
use schalpoen\application\interactors\EditDraft\EditDraftInputModel;
use schalpoen\application\interactors\ShowAuthorActions\ShowAuthorActions;
use schalpoen\application\interactors\ShowAuthorActions\ShowAuthorActionsInputModel;
use schalpoen\application\interactors\WriteDraft\WriteDraft;
use schalpoen\application\interactors\WriteDraft\WriteDraftInputModel;
use schalpoen\infrastructure\components\authentication\UsesAuthenticationState;
use schalpoen\infrastructure\components\http\factories\MakesCannedResponses;
use schalpoen\infrastructure\components\View;
use schalpoen\application\interactors\ChangePostState\ChangePostState;
use schalpoen\application\interactors\ChangePostState\ChangePostStateInputModel;
use schalpoen\application\interactors\CreateDraft\CreateDraft;
use schalpoen\application\interactors\CreateDraft\CreateDraftInputModel;
use schalpoen\application\interactors\UpdateDraft\UpdateDraft;
use schalpoen\application\interactors\UpdateDraft\UpdateDraftInputModel;
use schalpoen\domain\models\Post;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\presenters\EditDraftPresenter;
use schalpoen\infrastructure\presenters\ShortPostPresenter;
use schalpoen\infrastructure\presenters\WriteDraftPresenter;
use schalpoen\infrastructure\viewmodels\author\ShowActionsViewModel;
use schalpoen\infrastructure\widgets\editor\Editor;

class AuthorController
{
    use MakesCannedResponses;
    use UsesAuthenticationState;

    private LayoutFactory $layoutFactory;

    private ShowAuthorActions $showAuthorActions;
    private WriteDraft $writeDraft;
    private CreateDraft $createDraft;
    private EditDraft $editDraft;
    private UpdateDraft $updateDraft;
    private ChangePostState $changeState;

    private ShortPostPresenter $postPresenter;
    private EditDraftPresenter $editDraftPresenter;
    private WriteDraftPresenter $writeDraftPresenter;

    public function __construct(
        LayoutFactory $layoutFactory,
        ShowAuthorActions $showAuthorActions,
        WriteDraft $writeDraft,
        CreateDraft $createDraft,
        EditDraft $editDraft,
        UpdateDraft $updateDraft,
        ChangePostState $changePostState,
        ShortPostPresenter $postPresenter,
        EditDraftPresenter $editDraftPresenter,
        WriteDraftPresenter $writeDraftPresenter
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->showAuthorActions = $showAuthorActions;
        $this->writeDraft = $writeDraft;
        $this->createDraft = $createDraft;
        $this->editDraft = $editDraft;
        $this->updateDraft = $updateDraft;
        $this->changeState = $changePostState;
        $this->postPresenter = $postPresenter;
        $this->editDraftPresenter = $editDraftPresenter;
        $this->writeDraftPresenter = $writeDraftPresenter;
    }

    public function actionShowActions(ServerRequestInterface $request): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $input = new ShowAuthorActionsInputModel($userId);

        $responseModel = $this->showAuthorActions->run($input);

        $editablePosts = [];
        foreach ($responseModel->drafts as $post) {
            $editablePosts[$post->getId()] = $this->postPresenter->run($post);
        }
        $finalizedPosts = [];
        foreach ($responseModel->finalizedPosts as $post) {
            $finalizedPosts[$post->getId()] = $this->postPresenter->run($post);
        }

        $viewModel = new ShowActionsViewModel(
            $editablePosts,
            $finalizedPosts,
            empty($editablePosts) && empty($finalizedPosts)
        );

        $view = new View(
            __DIR__ . '/../views/author/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        return new Response(200, [], $view->render('drafts.php', [
            'model' => $viewModel,
        ]));
    }

    public function actionWriteDraft(ServerRequestInterface $request): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $input = new WriteDraftInputModel($userId);

        try {
            $this->writeDraft->run($input);
        } catch (LoginException $e) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        }

        $editor = new Editor();
        if (array_key_exists('submit', $request->getParsedBody())) {
            $editor->setPostValues(
                array_key_exists('id', $request->getParsedBody())
                    ? (int)$request->getParsedBody()['id']
                    : null,
                $request->getParsedBody()['title'] ?? '',
                $request->getParsedBody()['preview'] ?? '',
                $request->getParsedBody()['text'] ?? ''
            );
            $editor->setHasPostedValues(true);
        }

        $view = new View(
            __DIR__ . '/../views/author/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        $viewModel = $this->writeDraftPresenter->run($editor);

        return new Response(200, [], $view->render('edit.php', [
            'editor' => $editor,
            'model' => $viewModel,
        ]));
    }

    public function actionCreateDraft(ServerRequestInterface $request): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $inputModel = new CreateDraftInputModel(
            $userId,
            $request->getParsedBody()['title'] ?? '',
            $request->getParsedBody()['preview'] ?? '',
            $request->getParsedBody()['text'] ?? ''
        );

        try {
            $this->createDraft->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        }

        return new Response(303, ['Location' => '/author/drafts'], 'Draft created');
    }

    public function actionEditDraft(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $inputModel = new EditDraftInputModel(
            $userId,
            (int) $params['id'] ?: -1
        );

        try {
            $post = $this->editDraft->run($inputModel);
        } catch (LoginException $e) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        }

        $editor = new Editor();
        if (array_key_exists('submit', $request->getParsedBody())) {
            $editor->setPostValues(
                array_key_exists('id', $request->getParsedBody())
                    ? (int)$request->getParsedBody()['id']
                    : null,
                $request->getParsedBody()['title'] ?? '',
                $request->getParsedBody()['preview'] ?? '',
                $request->getParsedBody()['text'] ?? ''
            );
            $editor->setHasPostedValues(true);
        } else {
            $editor->setPostValues(
                $post->getId(),
                $post->getTitle(),
                $post->getPreview(),
                $post->getText()
            );
        }

        $viewModel = $this->editDraftPresenter->run($post, $editor);

        $view = new View(
            __DIR__ . '/../views/author/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        return new Response(200, [], $view->render('edit.php', [
            'model' => $viewModel,
            'editor' => $editor,
        ]));
    }

    public function actionUpdateDraft(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $inputModel = new UpdateDraftInputModel(
            $userId,
            (int)$params['id'] ?: -1,
            $request->getParsedBody()['title'] ?? '',
            $request->getParsedBody()['preview'] ?? '',
            $request->getParsedBody()['text'] ?? ''
        );

        try {
            $this->updateDraft->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        }

        return new Response(303, ['Location' => '/author/drafts'], 'Draft updated');
    }

    public function actionFinalize(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $postId = (int) $params['id'];
        $inputModel = new ChangePostStateInputModel($userId, $postId, Post::STATUS_FINISHED);
        $this->changeState->run($inputModel);
        return new Response(303, ['Location' => '/author/drafts'], 'Post finalized');
    }

    public function actionUnfinalize(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $postId = (int) $params['id'];
        $inputModel = new ChangePostStateInputModel($userId, $postId, Post::STATUS_DRAFT);
        $this->changeState->run($inputModel);
        return new Response(303, ['Location' => '/author/drafts'], 'Post unfinalized');
    }

    public function actionDelete(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $postId = (int) $params['id'];
        $inputModel = new ChangePostStateInputModel($userId, $postId, Post::STATUS_DELETED);
        $this->changeState->run($inputModel);
        return new Response(303, ['Location' => '/author/drafts'], 'Post deleted');
    }
}
