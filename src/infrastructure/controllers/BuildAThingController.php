<?php

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\components\view\LayoutFactory;

class BuildAThingController
{
    private LayoutFactory $layoutFactory;

    public function __construct(LayoutFactory $layoutFactory)
    {
        $this->layoutFactory = $layoutFactory;
    }

    public function actionPrivacy(ServerRequestInterface $request): ResponseInterface
    {
        $view = new View(
            __DIR__ . '/../views/buildathing/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_ISOLATED)
        );
        return new Response(200, [], $view->render('privacy.php'));
    }
}
