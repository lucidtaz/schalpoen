<?php

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\interactors\CreateComment\CreateComment;
use schalpoen\application\interactors\CreateComment\CreateCommentInputModel;
use schalpoen\infrastructure\components\authentication\UsesAuthenticationState;
use schalpoen\infrastructure\components\http\factories\MakesCannedResponses;

class CommentController
{
    use MakesCannedResponses;
    use UsesAuthenticationState;

    private CreateComment $createComment;

    public function __construct(CreateComment $createComment)
    {
        $this->createComment = $createComment;
    }

    public function actionCreate(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $inputModel = new CreateCommentInputModel(
            $userId,
            (int) $params['id'] ?: -1,
            trim($request->getParsedBody()['text'] ?? ''),
            (int) $request->getParsedBody()['parent'] ?: null
        );

        $comment = $this->createComment->run($inputModel);

        return new Response(
            303,
            ['Location' => $comment->post->url() . '#reacties'],
            'Comment created'
        );
    }
}
