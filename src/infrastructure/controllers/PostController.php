<?php

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\infrastructure\components\authentication\UsesAuthenticationState;
use schalpoen\infrastructure\components\http\factories\MakesCannedResponses;
use schalpoen\infrastructure\components\View;
use schalpoen\application\interactors\ViewPost\ViewPost;
use schalpoen\application\interactors\ViewPost\ViewPostInputModel;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\presenters\ViewPostPresenter;

class PostController
{
    use MakesCannedResponses;
    use UsesAuthenticationState;

    private LayoutFactory $layoutFactory;

    private ViewPost $viewPost;

    private ViewPostPresenter $viewPostPresenter;

    public function __construct(
        LayoutFactory $layoutFactory,
        ViewPost $viewPost,
        ViewPostPresenter $viewPostPresenter
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->viewPost = $viewPost;
        $this->viewPostPresenter = $viewPostPresenter;
    }

    public function actionView(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $inputModel = new ViewPostInputModel(
            (int) $params['id'],
            $this->getAuthenticationState($request)->userId
        );

        try {
            $responseModel = $this->viewPost->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        } catch (NotFoundException $e) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }

        // Canonicalize the URL if not already so:
        $canonicalUrl = $responseModel->post->url();
        if ($request->getUri()->getPath() !== $canonicalUrl) {
            return new Response(
                301,
                ['Location' => $canonicalUrl],
                'Redirecting to canonical URL: ' . $canonicalUrl
            );
        }

        $viewModel = $this->viewPostPresenter->run($responseModel);

        $view = new View(
            __DIR__ . '/../views/post/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('view.php', [
            'model' => $viewModel,
        ]));
    }
}
