<?php

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\interactors\ShowPublisherActions\ShowPublisherActions;
use schalpoen\application\interactors\ShowPublisherActions\ShowPublisherActionsInputModel;
use schalpoen\application\interactors\TagPost\TagPost;
use schalpoen\application\interactors\TagPost\TagPostInputModel;
use schalpoen\application\interactors\UntagPost\UntagPost;
use schalpoen\application\interactors\UntagPost\UntagPostInputModel;
use schalpoen\infrastructure\components\authentication\UsesAuthenticationState;
use schalpoen\infrastructure\components\http\factories\MakesCannedResponses;
use schalpoen\infrastructure\components\View;
use schalpoen\application\interactors\ChangePostState\ChangePostState;
use schalpoen\application\interactors\ChangePostState\ChangePostStateInputModel;
use schalpoen\application\interactors\ChooseTags\ChooseTags;
use schalpoen\application\interactors\ChooseTags\ChooseTagsInputModel;
use schalpoen\domain\models\Post;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\presenters\ChooseTagPresenter;
use schalpoen\infrastructure\presenters\ShortPostPresenter;
use schalpoen\infrastructure\viewmodels\publisher\PublishablePostViewModel;

class PublisherController
{
    use MakesCannedResponses;
    use UsesAuthenticationState;

    private LayoutFactory $layoutFactory;

    private ChangePostState $changeState;
    private ChooseTags $chooseTags;
    private ShowPublisherActions $showPublisherActions;
    private TagPost $tagPost;
    private UntagPost $untagPost;

    private ShortPostPresenter $postPresenter;
    private ChooseTagPresenter $chooseTagPresenter;

    public function __construct(
        LayoutFactory $layoutFactory,
        ChangePostState $changePostState,
        ChooseTags $chooseTags,
        ShowPublisherActions $showPublisherActions,
        TagPost $tagPost,
        UntagPost $untagPost,
        ShortPostPresenter $postPresenter,
        ChooseTagPresenter $chooseTagPresenter
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->changeState = $changePostState;
        $this->chooseTags = $chooseTags;
        $this->showPublisherActions = $showPublisherActions;
        $this->tagPost = $tagPost;
        $this->untagPost = $untagPost;
        $this->postPresenter = $postPresenter;
        $this->chooseTagPresenter = $chooseTagPresenter;
    }

    public function actionShowActions(ServerRequestInterface $request): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $input = new ShowPublisherActionsInputModel($userId);

        try {
            $responseModel = $this->showPublisherActions->run($input);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        }

        $viewModels = array_map(
            fn (Post $post) => new PublishablePostViewModel(
                $post->getId(),
                $this->postPresenter->run($post)
            ),
            $responseModel->publishablePosts
        );

        $view = new View(
            __DIR__ . '/../views/publisher/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        return new Response(200, [], $view->render('list.php', [
            'models' => $viewModels,
        ]));
    }

    public function actionChooseTag(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        try {
            $inputModel = new ChooseTagsInputModel(
                $userId,
                (int) $params['id']
            );
            $responseModel = $this->chooseTags->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        } catch (NotFoundException $e) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }

        $viewModel = $this->chooseTagPresenter->run($responseModel);

        $view = new View(
            __DIR__ . '/../views/publisher/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );

        return new Response(200, [], $view->render('choose-tag.php', [
            'model' => $viewModel,
        ]));
    }

    public function actionTag(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        try {
            $inputModel = new TagPostInputModel(
                $userId,
                (int) $params['id'],
                $tagId = array_key_exists('tagId', $request->getParsedBody()) ? (int) $request->getParsedBody()['tagId'] : null,
                $tagTitle = $request->getParsedBody()['newTag'] ?? ''
            );

            $this->tagPost->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        } catch (NotFoundException $e) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }

        return new Response(303, ['Location' => '/publisher/posts'], 'Post tagged');
    }

    public function actionUntag(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        try {
            $inputModel = new UntagPostInputModel(
                $userId,
                (int) $params['id'],
                (int) $request->getParsedBody()['tagId'] ?: -1
            );

            $this->untagPost->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        } catch (NotFoundException $e) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }

        return new Response(303, ['Location' => '/publisher/posts'], 'Post untagged');
    }

    public function actionReject(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $inputModel = new ChangePostStateInputModel(
            $userId,
            (int) $params['id'],
            Post::STATUS_REJECTED
        );

        try {
            $this->changeState->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        } catch (NotFoundException $e) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }
        return new Response(303, ['Location' => '/publisher/posts'], 'Post rejected');
    }

    public function actionPublish(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        try {
            $inputModel = new ChangePostStateInputModel(
                $userId,
                (int) $params['id'],
                Post::STATUS_PUBLISHED
            );
            $this->changeState->run($inputModel);
        } catch (ForbiddenException $e) {
            return $this->createForbiddenResponse($request, (string) $request->getUri());
        } catch (NotFoundException $e) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }

        return new Response(303, ['Location' => '/publisher/posts'], 'Post published');
    }
}
