<?php

namespace schalpoen\infrastructure\controllers;

use DateTimeImmutable;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use schalpoen\application\parser\Parser;
use schalpoen\application\parser\StripParserDecorator;
use schalpoen\domain\models\Post;
use schalpoen\infrastructure\components\formatting\RssPostFormatter;
use schalpoen\infrastructure\components\Toolbox;
use schalpoen\infrastructure\components\View;
use schalpoen\application\persistence\PostRepository;
use schalpoen\infrastructure\viewmodels\rss\IndexViewModel;
use schalpoen\infrastructure\viewmodels\rss\PostViewModel;

class RssController
{
    private PostRepository $postRepository;

    private Parser $parser;

    public function __construct(PostRepository $postRepository, Parser $parser)
    {
        $this->postRepository = $postRepository;
        $this->parser = $parser;
    }

    public function actionIndex(): ResponseInterface
    {
        $posts = $this->postRepository->retrieveLastPublishedPosts(20);

        $viewModel = $this->presentIndex($posts);

        $view = new View(__DIR__ . '/../views/rss/', null);
        return new Response(
            200,
            ['Content-Type' => 'application/rss+xml'],
            $view->render('index.php', [
                'model' => $viewModel,
            ])
        );
    }

    /**
     * @param Post[] $posts
     */
    private function presentIndex(array $posts): IndexViewModel
    {
        $parser = new StripParserDecorator($this->parser);
        $formatter = new RssPostFormatter($parser);

        $now = new DateTimeImmutable('now');
        $thisYear = $now->format('Y');

        $postModels = array_map(function (Post $post) use ($formatter) {
            $authorField = null;
            if (!$post->getAuthor()->emailHidden()) {
                $authorField = $post->getAuthor()->getEmail() . ' (' . $post->getAuthor()->getEmail() . ')';
            }
            return new PostViewModel(
                htmlspecialchars($post->getTitle()),
                $formatter->getIntroduction($post),
                $post->url(true),
                $post->url(true),
                $formatter->getDate($post),
                htmlspecialchars($authorField)
            );
        }, $posts);

        $basePath = Toolbox::getConfig()['app']['basepath'];
        $buildDate = $this->calculateBuildDate($posts);
        $formattedBuildDate = $buildDate->format(DateTimeImmutable::RFC2822);

        return new IndexViewModel(
            $basePath,
            "$basePath/rss",
            "Copyright 2010-$thisYear Thijs Zumbrink",
            $formattedBuildDate,
            "$basePath/style/previews/schalpoen.png",
            $postModels
        );
    }

    /**
     * @param Post[] $posts
     */
    private function calculateBuildDate(array $posts): DateTimeImmutable
    {
        $maxPublishedAt = array_reduce($posts, function (DateTimeImmutable $maxPublishedAt, Post $post) {
            if ($post->getPublishedAt() === null) {
                // Programmer error
                throw new InvalidArgumentException('Only published Posts should be displayed');
            }
            if ((int) $post->getPublishedAt()->format('YmdHis') > (int) $maxPublishedAt->format('YmdHis')) {
                return $post->getPublishedAt();
            }
            return $maxPublishedAt;
        }, new DateTimeImmutable('2000-01-01 12:00:00'));

        $now = new DateTimeImmutable('now');
        $startOfThisYear = $now
            ->setTime(0, 0, 0)
            ->setDate((int) $now->format('Y'), 1,1);

        if ($startOfThisYear > $maxPublishedAt) {
            $buildDate = $startOfThisYear;
        } else {
            $buildDate = $maxPublishedAt;
        }

        return $buildDate;
    }
}
