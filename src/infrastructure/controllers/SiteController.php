<?php

namespace schalpoen\infrastructure\controllers;

use Exception;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\parser\Parser;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\infrastructure\components\authentication\Authenticator;
use schalpoen\application\exceptions\ValidationException;
use schalpoen\infrastructure\components\http\SessionData;
use schalpoen\infrastructure\components\View;
use schalpoen\application\interactors\RegisterUser\RegisterUser;
use schalpoen\application\interactors\RegisterUser\RegisterUserInputModel;
use schalpoen\application\persistence\CaptchaRepository;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\TagRepository;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\presenters\ShortPostPresenter;
use schalpoen\infrastructure\viewmodels\site\IndexViewModel;
use schalpoen\infrastructure\viewmodels\site\RegisterViewModel;

class SiteController
{
    private LayoutFactory $layoutFactory;

    private PostRepository $postRepository;
    private TagRepository $tagRepository;
    private CaptchaRepository $captchaRepository;

    private Authenticator $authenticator;
    private Parser $parser;

    private RegisterUser $registerUser;

    private ShortPostPresenter $postPresenter;

    public function __construct(
        LayoutFactory $layoutFactory,
        PostRepository $postRepository,
        TagRepository $tagRepository,
        CaptchaRepository $captchaRepository,
        Authenticator $authenticator,
        Parser $parser,
        RegisterUser $registerUser,
        ShortPostPresenter $postPresenter
    ) {
        $this->layoutFactory = $layoutFactory;

        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
        $this->captchaRepository = $captchaRepository;

        $this->authenticator = $authenticator;
        $this->parser = $parser;

        $this->registerUser = $registerUser;

        $this->postPresenter = $postPresenter;
    }

    public function actionIndex(ServerRequestInterface $request): ResponseInterface
    {
        $popularTags = $this->tagRepository->retrievePopular(10);
        $posts = $this->postRepository->retrieveLastPublishedPosts(5);

        $viewModel = new IndexViewModel(
            array_map(fn (Tag $tag) => $tag->getTitle(), $popularTags),
            array_map(fn (Post $post) => $this->postPresenter->run($post), $posts)
        );

        $view = new View(
            __DIR__ . '/../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('index.php', [
            'model' => $viewModel,
        ]));
    }

    public function actionRegistrationForm(ServerRequestInterface $request): ResponseInterface
    {
        $captcha = $this->captchaRepository->retrieveRandom();

        /** @var SessionData $session */
        $session = $request->getAttribute('session');
        $session->set('registrationQuestionId', $captcha->id);

        $viewModel = $this->presentRegistrationForm(
            $captcha->question,
            '',
            []
        );

        $view = new View(
            __DIR__ . '/../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('register.php', [
            'model' => $viewModel,
        ]));
    }

    public function actionRegister(ServerRequestInterface $request): ResponseInterface
    {
        /** @var SessionData $session */
        $session = $request->getAttribute('session');

        $username = trim($request->getParsedBody()['username'] ?? '');
        $password = trim($request->getParsedBody()['password'] ?? '');

        $inputModel = new RegisterUserInputModel(
            $username,
            $session->has('registrationQuestionId') ? (int) $session->get('registrationQuestionId') : -1,
            $request->getParsedBody()['answer'] ??  ''
        );

        $errors = [];
        try {
            $user = $this->registerUser->run($inputModel);
            $authenticationResult = $this->authenticator->handleRegistration($user->id, $username, $password, true, $request);

            $response = new Response(302, ['Location' => '/'], 'Registration successful');
            foreach ($authenticationResult->responseModifications as $responseModification) {
                $response = $responseModification->apply($response);
            }

            return $response;
        } catch (ValidationException $e) {
            $errors = $e->getErrors();
        } catch (Exception $e) {
            $errors[] = 'Er is iets onbekends misgegaan. Als het probleem aanhoudt, contacteer <a href="mailto:thijs@schalpoen.nl">thijs@schalpoen.nl</a> dan s.v.p.';
        }

        $captcha = $this->captchaRepository->retrieveRandom();
        $session->set('registrationQuestionId', $captcha->id);

        $viewModel = $this->presentRegistrationForm(
            $captcha->question,
            $request->getParsedBody()['username'] ?? '',
            $errors // TODO: Consider doing a redirect with a flash message instead
        );

        $view = new View(
            __DIR__ . '/../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('register.php', [
            'model' => $viewModel,
        ]));
    }

    /**
     * @param string[] $errors
     */
    private function presentRegistrationForm(string $question, string $username, array $errors): RegisterViewModel
    {
        return new RegisterViewModel(
            $this->parser->inlineParse($question),
            htmlspecialchars($username),
            $errors
        );
    }

    public function actionLoginForm(ServerRequestInterface $request): ResponseInterface
    {
        $view = new View(
            __DIR__ . '/../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('login.php', [
            'loginError' => null,
        ]));
    }

    public function actionLogin(ServerRequestInterface $request): ResponseInterface
    {
        $authenticationResult = $this->authenticator->handleLogin(
            $request->getParsedBody()['userName'] ?? '',
            $request->getParsedBody()['passwd'] ?? '',
            (bool) ($request->getParsedBody()['persistent'] ?? false),
            $request
        );
        if ($authenticationResult->authenticationState->loggedIn) {
            $response = new Response(302, ['Location' => '/'], 'Login successful');
            foreach ($authenticationResult->responseModifications as $responseModification) {
                $response = $responseModification->apply($response);
            }
            return $response;
        }

        $view = new View(
            __DIR__ . '/../views/site/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('login.php', [
            'loginError' => 'Inloggen mislukt!', // FIXME: Maybe we should redirect to GET /login with a flash message?
        ]));
    }

    public function actionLogout(ServerRequestInterface $request): ResponseInterface
    {
        $responseModifications = $this->authenticator->handleLogout($request);

        $response = new Response(302, ['Location' => '/'], 'Logged out');
        foreach ($responseModifications as $responseModification) {
            $response = $responseModification->apply($response);
        }
        return $response;
    }
}
