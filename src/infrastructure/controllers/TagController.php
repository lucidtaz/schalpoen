<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\interactors\ViewTag\ViewTag;
use schalpoen\application\interactors\ViewTag\ViewTagInputModel;
use schalpoen\infrastructure\components\http\factories\MakesCannedResponses;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\presenters\ViewTagPresenter;

class TagController
{
    use MakesCannedResponses;

    private LayoutFactory $layoutFactory;

    private ViewTag $viewTag;

    private ViewTagPresenter $presenter;

    public function __construct(
        LayoutFactory $layoutFactory,
        ViewTag $viewTag,
        ViewTagPresenter $presenter
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->viewTag = $viewTag;
        $this->presenter = $presenter;
    }

    public function actionView(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $inputModel = new ViewTagInputModel((int) $params['id']);

        try {
            $responseModel = $this->viewTag->run($inputModel);
        } catch (NotFoundException $e) {
            return $this->createNotFoundResponse($request, (string) $request->getUri());
        }

        $viewModel = $this->presenter->present($responseModel);

        $view = new View(
            __DIR__ . '/../views/tag/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('view.php', [
            'model' => $viewModel,
        ]));
    }
}
