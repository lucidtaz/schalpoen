<?php

namespace schalpoen\infrastructure\controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use schalpoen\domain\models\User;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\infrastructure\components\authentication\UsesAuthenticationState;
use schalpoen\infrastructure\components\http\factories\MakesCannedResponses;
use schalpoen\infrastructure\components\View;
use schalpoen\application\interactors\UpdateProfile\UpdateProfile;
use schalpoen\application\interactors\UpdateProfile\UpdateProfileInputModel;
use schalpoen\application\persistence\UserRepository;
use schalpoen\infrastructure\components\view\LayoutFactory;
use schalpoen\infrastructure\viewmodels\user\ProfileViewModel;

class UserController
{
    use MakesCannedResponses;
    use UsesAuthenticationState;

    private LayoutFactory $layoutFactory;

    private UserRepository $userRepository;

    public function __construct(
        LayoutFactory $layoutFactory,
        UserRepository $userRepository
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->userRepository = $userRepository;
    }

    public function actionView(ServerRequestInterface $request, array $params): ResponseInterface
    {
        $id = (int) $params['id'];
        $user = $this->userRepository->retrieve($id);

        $view = new View(
            __DIR__ . '/../views/user/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('view.php', [
            'user' => $user, // TODO: Refactor to use ViewModel and Presenter
        ]));
    }

    public function actionEditProfile(ServerRequestInterface $request): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $user = $this->userRepository->retrieve($userId);

        $viewModel = $this->presentViewProfile($user, $this->getAuthenticationState($request)->safe);

        $view = new View(
            __DIR__ . '/../views/user/',
            $this->layoutFactory->make($request, LayoutFactory::CODE_MAIN)
        );
        return new Response(200, [], $view->render('profile.php', [
            'model' => $viewModel,
        ]));
    }

    private function presentViewProfile(User $user, bool $isSafeLogin): ProfileViewModel
    {
        // Controller is also the Presenter in this case
        // (http://www.plainionist.net/Implementing-Clean-Architecture-Controller-Presenter/)
        $lastLetter = strtolower(substr($user->getName(), -1));
        if ($lastLetter === 's' || $lastLetter === 'z') {
            $possessive = "'";
        } else {
            $possessive = "'s";
        }
        return new ProfileViewModel(
            htmlspecialchars($user->getName() . $possessive) . ' profile',
            htmlspecialchars($user->getName()),
            htmlspecialchars($user->getEmail() ?? ''),
            $isSafeLogin,
            $user->emailHidden(),
            $user->wantsReplyNotify()
        );
    }

    /**
     * @throws LoginException If the user should login again to increase their
     * authentication's safety
     * @throws NotFoundException Not normally possible
     */
    public function actionUpdateProfile(ServerRequestInterface $request): ResponseInterface
    {
        $userId = $this->getAuthenticationState($request)->userId;
        if ($userId === null) {
            return $this->createNotAuthenticatedResponse($request, (string) $request->getUri());
        }

        $inputModel = new UpdateProfileInputModel(
            $userId,
            $this->getAuthenticationState($request)->safe,
            $request->getParsedBody()['displayName'] ?? '',
            $request->getParsedBody()['email'] ?? '',
            (bool) ($request->getParsedBody()['emailHidden'] ?? null),
            (bool) ($request->getParsedBody()['notifyReplies'] ?? null)
        );
        $interactor = new UpdateProfile($this->userRepository);
        $interactor->run($inputModel);

        return new Response(303, ['Location' => '/profile'], 'Profile updated');
    }
}
