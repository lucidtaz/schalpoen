<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\email;

use schalpoen\application\email\Email;
use schalpoen\application\email\EmailGateway;

class SimpleEmailGateway implements EmailGateway
{
    public function send(Email $email): void
    {
        $headers  = [
            "MIME-Version: 1.0\r\n",
            "Content-type: text/html; charset=iso-8859-1\r\n",
        ];
        if ($email->fromName !== null) {
            $headers[] = "From: $email->fromName <$email->fromEmail>\r\n";
        } else {
            $headers[] = "From: $email->fromEmail\r\n";
        }
        foreach ($email->to as $toEmail => $toName) {
            if ($toName !== null) {
                $headers[] = "To: $toName <$toEmail>\r\n";
            } else {
                $headers[] = "To: $toEmail\r\n";
            }
        }
        mail('', $email->subject, $email->body, implode('', $headers));
    }
}