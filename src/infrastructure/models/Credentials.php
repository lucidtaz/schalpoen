<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\models;

class Credentials
{
    public ?int $id;

    public int $userId;

    public string $username;

    public string $passwordHash;

    public static function make(int $userId, string $username, string $passwordHash): self
    {
        $credentials = new self();
        $credentials->userId = $userId;
        $credentials->username = $username;
        $credentials->passwordHash = $passwordHash;
        return $credentials;
    }
}
