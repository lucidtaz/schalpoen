<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\models;

use DateInterval;
use DateTimeImmutable;

class Token
{
    private const LIFETIME_DAYS = 120;

    public int $userId;

    public string $value;

    public DateTimeImmutable $created;

    public function __construct(int $userId, string $value)
    {
        $this->userId = $userId;
        $this->value = $value;
        $this->created = new DateTimeImmutable('now');
    }

    public static function getLifetime(): DateInterval
    {
        return new DateInterval('P' . self::LIFETIME_DAYS . 'D');
    }

    public function isValid(): bool
    {
        $lifetime = self::getLifetime();
        $expiry = $this->created->add($lifetime);
        $now = new DateTimeImmutable();
        return $expiry > $now;
    }
}
