<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\persistence;

use RuntimeException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\infrastructure\models\Credentials;

class CredentialsRepository
{
    public function insert(Credentials $credentials): void
    {
        $sql = '
            INSERT INTO credentials ("userId", username, "passwordHash", created, updated)
            VALUES ($1, $2, $3, NOW(), NOW())
            RETURNING id
        ';
        $result = pg_query_params($sql, [
            $credentials->userId,
            $credentials->username,
            $credentials->passwordHash,
        ]);
        $row = pg_fetch_row($result);
        if ($row === false) {
            throw new RuntimeException('Could not insert credentials.');
        }
        [$id] = $row;
        $credentials->id = (int) $id;
    }

    /**
     * @throws NotFoundException
     */
    public function retrieveByUsername(string $username): Credentials
    {
        $result = pg_query_params('
            SELECT *
            FROM credentials
            WHERE username = $1
        ', [
            $username,
        ]);
        $values = pg_fetch_array($result);
        if ($values === false) {
            throw new NotFoundException('Credentials for ' . $username . ' not found!');
        }
        return $this->populate($values);
    }

    public function isUsernameFree(string $username): bool
    {
        $result = pg_query_params('
            SELECT 1
            FROM credentials
            WHERE username = $1
        ', [
            $username,
        ]);
        $values = pg_fetch_array($result);
        return $values === false;
    }

    private function populate(array $values): Credentials
    {
        $credentials = new Credentials();
        $credentials->id = (int) $values['id'];
        $credentials->userId = (int) $values['userId'];
        $credentials->username = $values['username'];
        $credentials->passwordHash = $values['passwordHash'];
        return $credentials;
    }

    public function update(Credentials $credentials): void
    {
        pg_query_params('
            UPDATE credentials
            SET username = $2,
            passwordHash = $3,
            updated = NOW()
            WHERE id = $1
        ', [
            $credentials->id,
            $credentials->username,
            $credentials->passwordHash,
        ]);
    }
}
