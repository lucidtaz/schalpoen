<?php

namespace schalpoen\infrastructure\persistence;

use schalpoen\application\persistence\CommentRepository;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Comment;
use schalpoen\domain\models\Post;

class DbCommentRepository implements CommentRepository
{
    private PostRepository $postRepository;
    private UserRepository $userRepository;

    public function __construct(PostRepository $postRepository, UserRepository $userRepository)
    {
        // TODO: Remove dependence between repositories by separating the callers
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    public function insert(Comment $comment): void
    {
        if ($comment->parent !== null) {
            $result = pg_query_params('
                INSERT INTO comments (post, author, text, created, reply)
                VALUES ($1, $2, $3, NOW(), $4)
                RETURNING id
            ', [
                $comment->post->getId(),
                $comment->author->getId(),
                $comment->text,
                $comment->parent->id,
            ]);
        } else {
            $result = pg_query_params('
                INSERT INTO comments (post, author, text, created)
                VALUES ($1, $2, $3, NOW())
                RETURNING id
            ', [
                $comment->post->getId(),
                $comment->author->getId(),
                $comment->text,
            ]);
        }
        [$id] = pg_fetch_row($result);
        $comment->id = (int) $id;
    }

    public function retrieve(int $id): Comment
    {
        $result = pg_query_params('
            SELECT *, EXTRACT(epoch FROM created) AS "createdUnix"
            FROM comments
            WHERE id = $1
        ', [
            $id,
        ]);
        $values = pg_fetch_array($result);
        if ($values === false) {
            throw new NotFoundException('Comment #' . $id . ' niet gevonden!');
        }
        return $this->populateWithParentRetrieval($values);
    }

    private function populateWithParentRetrieval(array $values): Comment
    {
        $comment = $this->populate($values);
        if (isset($values['reply'])) {
            $comment->setParent($this->retrieve($values['reply']));
        }
        return $comment;
    }

    private function populate(array $values): Comment
    {
        $post = $this->postRepository->retrieve($values['post']);
        $author = $this->userRepository->retrieve($values['author']);

        $comment = new Comment($post, $author, $values['text']);

        $comment->id = (int) $values['id'];
        $comment->unixTime = (int) $values['createdUnix'];

        return $comment;
    }

    /**
     * @return Comment[]
     */
    public function retrieveRootsForPost(Post $post): array
    {
        $result = pg_query_params('
            SELECT *, EXTRACT(epoch from created) AS "createdUnix"
            FROM comments
            WHERE post = $1
            ORDER BY id ASC
        ', [
            $post->getId(),
        ]);
        $comments = []; //string key gebruiken ipv integer key omdat we anders in de problemen kunnen komen met geheugen denk ik
        $parentReferences = []; // (string)id => parent-id
        $roots = [];
        while ($values = pg_fetch_array($result)) {
            $comment = $this->populate($values);
            $comments[(string)$comment->id] = $comment;
            $parentReferences[(string)$comment->id] = $values['reply'];
            if (!isset($values['reply'])) {
                $roots[] = $comment;
            }
        }
        foreach ($comments as $stringId => $comment) {
            if (isset($parentReferences[$stringId])) {
                $comments[$stringId]->setParent($comments[$parentReferences[$stringId]]);
            }
        }
        return $roots;
    }

    public function countByPost(Post $post): int
    {
        $result = pg_query_params('
            SELECT COUNT(1)
            FROM comments
            WHERE post = $1
            GROUP BY post
        ', [
            $post->id,
        ]);
        [$n] = pg_fetch_array($result);
        return (int) $n;
    }
}
