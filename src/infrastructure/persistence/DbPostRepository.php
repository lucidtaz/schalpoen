<?php

namespace schalpoen\infrastructure\persistence;

use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\domain\models\User;

class DbPostRepository implements PostRepository
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        // TODO: Remove dependence between repositories by separating the callers
        $this->userRepository = $userRepository;
    }

    public function insert(Post $post): void
    {
        $sql = '
            INSERT INTO posts (title, preview, text, status, edited, created, author)
            VALUES ($1, $2, $3, $4, NOW(), NOW(), $5)
            RETURNING id
        ';
        $result = pg_query_params($sql, [
            $post->title,
            $post->preview ?? '', // TODO: Make nullable
            $post->text,
            $post->status,
            $post->getAuthor()->getId(),
        ]);
        [$id] = pg_fetch_row($result);
        $post->id = (int) $id;

        // TODO: Associate tags in junction table
    }

    public function retrieve(int $id): Post
    {
        $result = pg_query_params('
            SELECT *, EXTRACT(epoch from edited) AS "editedUnixTime", EXTRACT(epoch from published) AS "publishedUnixTime"
            FROM posts
            WHERE id = $1
        ', [
            $id,
        ]);
        $values = pg_fetch_array($result);
        if ($values === false) {
            throw new NotFoundException('Post #' . $id . ' niet gevonden!');
        }

        $tagIds = $this->retrieveTagIdsByPostIds([$id]);

        return $this->populate($values, $tagIds);
    }

    /**
     * @param int[] $postIds
     * @return int[][] plain arrays inside an associative array, indexed by post id
     */
    private function retrieveTagIdsByPostIds(array $postIds): array
    {
        $idList = '{' . implode(', ', $postIds) . '}';
        $result = pg_query_params('
            SELECT "post", "tag"
            FROM "postTags"
            WHERE post = ANY($1)
        ', [
            $idList
        ]);

        $tagIdsByPostId = [];
        while ($values = pg_fetch_array($result)) {
            $postId = (int) $values['post'];
            $tagId = (int) $values['tag'];

            if (!array_key_exists($postId, $tagIdsByPostId)) {
                $tagIdsByPostId[$postId] = [];
            }
            $tagIdsByPostId[$postId][] = $tagId;
        }

        return $tagIdsByPostId;
    }

    private function populate(array $values, array $tagIds): Post
    {
        $author = $this->userRepository->retrieve($values['author']);

        $post = new Post($author, $values['title'], $values['text']);
        $post->id = (int) $values['id'];
        $post->publishedUnixTime = $values['publishedUnixTime'] !== null ? (int) $values['publishedUnixTime'] : null;
        $post->editedUnixTime = $values['editedUnixTime'] !== null ? (int) $values['editedUnixTime'] : null;
        $post->preview = $values['preview'];
        $post->status = $values['status'];
        $post->commentCount = $values['commentCount'];

        $post->tagIds = $tagIds;

        return $post;
    }

    /**
     * @return Post[]
     */
    public function retrieveAll(): array
    {
        $result = pg_query('
            SELECT *, EXTRACT(epoch from edited) AS "editedUnixTime", EXTRACT(epoch from published) AS "publishedUnixTime"
            FROM posts
        ');

        $posts = [];
        while ($values = pg_fetch_array($result)) {
            $postId = $values['id'];
            $tagIds = $this->retrieveTagIdsByPostIds([$postId])[$postId] ?? []; // TODO: Lazy load
            $posts[] = $this->populate($values, $tagIds);
        }
        return $posts;
    }

    /**
     * @return Post[]
     */
    public function retrieveLastPublishedPosts(int $limit = 0): array
    {
        if ($limit === 0) {
            $limit = 999;
        }
        $result = pg_query_params('
            SELECT *, EXTRACT(epoch from edited) AS "editedUnixTime", EXTRACT(epoch from published) AS "publishedUnixTime"
            FROM posts
            WHERE status = \'published\'
            ORDER BY published DESC
            LIMIT $1
        ', [
            $limit,
        ]);
        $posts = [];
        while ($values = pg_fetch_array($result)) {
            $postId = $values['id'];
            $tagIds = $this->retrieveTagIdsByPostIds([$postId])[$postId] ?? []; // TODO: Lazy load
            $posts[] = $this->populate($values, $tagIds);
        }
        return $posts;
    }

    public function countPublishable(): int
    {
        $result = pg_query('
            SELECT COUNT(1)
            FROM posts
            WHERE status = \'finished\'
        ');
        [$n] = pg_fetch_array($result);
        return (int) $n;
    }

    /**
     * @return Post[]
     */
    public function retrieveEditableBy(User $author): array
    {
        // TODO: Move more logic to caller
        $result = pg_query_params('
            SELECT *, EXTRACT(epoch from edited) AS "editedUnixTime", EXTRACT(epoch from published) AS "publishedUnixTime"
            FROM posts
            WHERE status IN (\'draft\', \'rejected\')
            AND author = $1
            ORDER BY status != \'rejected\' DESC, created DESC
        ', [
            $author->getId(),
        ]);
        $posts = [];
        while ($values = pg_fetch_array($result)) {
            $postId = $values['id'];
            $tagIds = $this->retrieveTagIdsByPostIds([$postId])[$postId] ?? []; // TODO: Lazy load
            $posts[] = $this->populate($values, $tagIds);
        }
        return $posts;
    }

    public function countEditable(User $author): int
    {
        $result = pg_query_params('
            SELECT COUNT(1)
            FROM posts
            WHERE status IN (\'draft\', \'rejected\')
            AND author = $1
        ', [
            $author->getId(),
        ]);
        [$n] = pg_fetch_array($result);
        return (int) $n;
    }

    /**
     * @return Post[]
     */
    public function retrieveFinalizedBy(User $author): array
    {
        // TODO: Move more logic to caller
        $result = pg_query_params('
            SELECT *, EXTRACT(epoch from edited) AS "editedUnixTime", EXTRACT(epoch from published) AS "publishedUnixTime"
            FROM posts
            WHERE status IN (\'finished\')
            AND author = $1
            ORDER BY created DESC
        ', [
            $author->getId(),
        ]);
        $posts = [];
        while ($values = pg_fetch_array($result)) {
            $postId = $values['id'];
            $tagIds = $this->retrieveTagIdsByPostIds([$postId])[$postId] ?? []; // TODO: Lazy load
            $posts[] = $this->populate($values, $tagIds);
        }
        return $posts;
    }

    /**
     * @return Post[]
     */
    public function retrieveByTag(Tag $tag): array
    {
        $result = pg_query_params('
            SELECT p.*, EXTRACT(epoch from p.edited) AS "editedUnixTime", EXTRACT(epoch from p.published) AS "publishedUnixTime"
            FROM "postTags" pt
            INNER JOIN posts p ON p.id = pt.post
            WHERE pt.tag = $1
            ORDER BY published DESC
        ', [
            $tag->getId(),
        ]);
        $posts = [];
        while ($values = pg_fetch_array($result)) {
            $postId = $values['id'];
            $tagIds = $this->retrieveTagIdsByPostIds([$postId])[$postId] ?? []; // TODO: Lazy load
            $posts[] = $this->populate($values, $tagIds);
        }
        return $posts;
    }

    public function update(Post $post): void
    {
        pg_query_params('
            UPDATE posts
            SET title = $1,
            preview = $2,
            text = $3,
            published = TO_TIMESTAMP($4),
            edited = TO_TIMESTAMP($5),
            status = $6,
            "commentCount" = $7
            WHERE id = $8
        ', [
            $post->title,
            $post->preview ?? '', // TODO: Make nullable
            $post->text,
            $post->getPublishedAt() !== null ? $post->getPublishedAt()->getTimestamp() : null,
            $post->getEditedAt() !== null ? $post->getEditedAt()->getTimestamp() : null,
            $post->status,
            $post->commentCount,
            $post->id,
        ]);

        // TODO: Update tag association in junction table
    }
}
