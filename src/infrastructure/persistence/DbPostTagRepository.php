<?php

namespace schalpoen\infrastructure\persistence;

use schalpoen\application\persistence\PostTagRepository;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;

class DbPostTagRepository implements PostTagRepository
{
    public function link(Post $post, Tag $tag): void
    {
        pg_query_params('
            INSERT INTO "postTags" (post, tag)
            VALUES ($1, $2)
        ', [
            $post->id,
            $tag->id,
        ]);
    }

    public function unlink(Post $post, Tag $tag): void
    {
        pg_query_params('
            DELETE FROM "postTags"
            WHERE post = $1
            AND tag = $2
        ', [
            $post->id,
            $tag->id,
        ]);
    }
}
