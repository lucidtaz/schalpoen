<?php

namespace schalpoen\infrastructure\persistence;

use schalpoen\application\persistence\SmileyRepository;
use schalpoen\domain\models\Smiley;

class DbSmileyRepository implements SmileyRepository
{
    private function populate(array $values): Smiley
    {
        return new Smiley(
            (int) $values['id'],
            $values['filePath'],
            $values['link'],
            (int) $values['width'],
            (int) $values['height']
        );
    }

    /**
     * @return Smiley[]
     */
    public function retrieveAll(): array
    {
        $query = '
            SELECT *
            FROM smileys
        ';
        $smileyResult = pg_query($query);
        $smileys = [];
        while ($values = pg_fetch_array($smileyResult)) {
            $smileys[] = $this->populate($values);
        }
        return $smileys;
    }
}
