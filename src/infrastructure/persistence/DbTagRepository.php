<?php

namespace schalpoen\infrastructure\persistence;

use schalpoen\application\persistence\TagRepository;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use RuntimeException;

class DbTagRepository implements TagRepository
{
    public function insert(Tag $tag): void
    {
        $result = pg_query_params('
            INSERT INTO tags (title) VALUES ($1)
            RETURNING id
        ', [
            $tag->title,
        ]);
        if ($result === false) {
            throw new RuntimeException('Could not create tag. Does it already exist?');
        }
        [$id] = pg_fetch_row($result);
        $tag->id = (int) $id;
    }

    public function retrieve(int $id): Tag
    {
        $result = pg_query_params('
            SELECT *
            FROM tags
            WHERE id = $1
        ', [
            $id,
        ]);
        $values = pg_fetch_array($result);
        if ($values === false) {
            throw new NotFoundException('Tag #' . $id . ' niet gevonden!');
        }
        return $this->populate($values);
    }

    private function populate(array $values): Tag
    {
        $tag = new Tag($values['title']);
        $tag->id = (int) $values['id'];
        return $tag;
    }

    /**
     * @return Tag[]
     */
    public function retrieveByPost(Post $post): array
    {
        //Get all tags that a post is tagged with
        $result = pg_query_params('
            SELECT t.*
            FROM "postTags" pt
            LEFT JOIN tags t ON t.id = pt.tag
            WHERE pt.post = $1
        ', [
            $post->getId(),
        ]);
        $tags = array();
        while ($values = pg_fetch_array($result)) {
            $tags[] = $this->populate($values);
        }
        return $tags;
    }

    /**
     * @return Tag[]
     */
    public function retrieveAll(): array
    {
        $result = pg_query('
            SELECT *
            FROM tags
            ORDER BY title ASC
        ');
        $tags = array();
        while ($values = pg_fetch_array($result)) {
            $tags[] = $this->populate($values);
        }
        return $tags;
    }

    /**
     * @return Tag[]
     */
    public function retrievePopular(int $n): array
    {
        $result = pg_query_params('
            SELECT t.*
            FROM "postTags" pt
            LEFT JOIN posts p ON p.id = pt.post
            LEFT JOIN tags t ON t.id = pt.tag
            WHERE p.status = \'published\'
            GROUP BY t.id
            ORDER BY COUNT(1) DESC, t.title ASC
            LIMIT $1
        ', [
            $n
        ]);
        $tags = array();
        while ($values = pg_fetch_array($result)) {
            $tags[] = $this->populate($values);
        }
        return $tags;
    }
}
