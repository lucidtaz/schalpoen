<?php

namespace schalpoen\infrastructure\persistence;

use schalpoen\application\persistence\UserRepository;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\User;
use RuntimeException;

class DbUserRepository implements UserRepository
{
    public function insert(User $user): void
    {
        $result = pg_query_params('
            INSERT INTO users ("displayName", email, "emailHidden", "notifyReplies", "isAuthor", "isPublisher", created)
            VALUES ($1, $2, $3, $4, $5, $6, NOW())
            RETURNING id
        ', [
            $user->displayName,
            $user->email,
            $user->emailHidden ? 't' : 'f',
            $user->notifyReplies ? 't' : 'f',
            $user->isAuthor ? '1' : '0',
            $user->isPublisher ? '1' : '0',
        ]);
        if ($result === false) {
            throw new RuntimeException('Could not create user. Does it already exist?');
        }
        [$id] = pg_fetch_row($result);
        $user->id = (int) $id;
    }

    public function retrieve(int $id): User
    {
        $result = pg_query_params('
            SELECT *
            FROM users
            WHERE id = $1
        ', [
            $id,
        ]);
        $values = pg_fetch_array($result);
        if ($values === false) {
            throw new NotFoundException('User #' . $id . ' niet gevonden!');
        }
        return $this->populate($values);
    }

    private function populate(array $values): User
    {
        $user = new User($values['displayName']);
        $user->id = (int) $values['id'];
        $user->email = $values['email'];
        $user->emailHidden = $values['emailHidden'] === 't';
        $user->notifyReplies = $values['notifyReplies'] === 't';
        $user->isAuthor = (int) $values['isAuthor'] === 1;
        $user->isPublisher = (int) $values['isPublisher'] === 1;
        $user->lastLogin = $values['lastLogin'];
        return $user;
    }

    /**
     * @deprecated
     */
    public function isDisplaynameFree(string $displayName): bool
    {
        $result = pg_query_params('
            SELECT 1
            FROM users
            WHERE "displayName" LIKE $1
        ', [
            $displayName,
        ]);
        return pg_num_rows($result) === 0;
    }

    public function update(User $user): void
    {
        $sql = '
            UPDATE users
            SET "displayName" = $1,
            email = $2,
            "emailHidden" = $3,
            "notifyReplies" = $4,
            "isAuthor" = $5,
            "isPublisher" = $6,
            "lastLogin" = $7
            WHERE id = $8
        ';
        pg_query_params($sql, [
            $user->displayName,
            $user->email,
            $user->emailHidden ? 't' : 'f',
            $user->notifyReplies ? 't' : 'f',
            $user->isAuthor ? '1' : '0',
            $user->isPublisher ? '1' : '0',
            $user->lastLogin,
            $user->id,
        ]);
    }
}
