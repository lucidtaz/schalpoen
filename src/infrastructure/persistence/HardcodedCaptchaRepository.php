<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\persistence;

use schalpoen\application\persistence\CaptchaRepository;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Captcha;

class HardcodedCaptchaRepository implements CaptchaRepository
{
    private const QUESTIONS = [
        [
            'q' => 'Hoeveel oren heeft een aap?',
            'a' => ['2', 'twee']
        ],
        [
            'q' => 'Hoeveel armen heeft een aap?',
            'a' => ['2', 'twee']
        ],
        [
            'q' => 'Wat is de kleur van broccoli?',
            'a' => ['groen']
        ],
        [
            'q' => 'Wat is de kleur van een sinaasappel?',
            'a' => ['oranje']
        ],
        [
            'q' => 'Welke is het grootst? Maan, Olifant, Ananas, Sinaasappel.',
            'a' => ['maan']
        ],
        [
            'q' => 'Welke is het kleinst? Mier, Olifant, Maan, Huis.',
            'a' => ['mier']
        ],
        [
            'q' => 'Welke heeft de grootste lach? 1: ^^, 2: :P, 3: ((:',
            'a' => ['3', 'drie', '((:']
        ],
        [
            'q' => 'Welk woord is doorgestreept? "Een ijsje is [s]nooit[/s] lekker."',
            'a' => ['nooit']
        ],
        [
            'q' => 'Welk woord is rood? "Een ijsje is [color=red]altijd[/color] lekker."',
            'a' => ['altijd']
        ],
        [
            'q' => 'Uit hoeveel woorden bestaat deze vraag?',
            'a' => ['6', 'zes']
        ],
    ];

    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): Captcha
    {
        if (!array_key_exists($id, self::QUESTIONS)) {
            throw new NotFoundException('Captcha ' . $id . ' not found.');
        }
        return $this->populate($id, self::QUESTIONS[$id]);
    }

    private function populate(int $id, array $values): Captcha
    {
        return new Captcha(
            $id,
            $values['q'],
            $values['a']
        );
    }

    /**
     * @throws NotFoundException If there are no Captcha's at all
     */
    public function retrieveRandom(): Captcha
    {
        $index = random_int(0, \count(self::QUESTIONS) - 1);
        return $this->retrieve($index);
    }
}
