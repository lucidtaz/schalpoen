<?php

namespace schalpoen\infrastructure\persistence;

use DateTimeImmutable;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\infrastructure\models\Token;

class TokenRepository
{
    public function insert(Token $token): void
    {
        $sql = '
            INSERT INTO tokens ("uId", token, created)
            VALUES ($1, $2, $3)
        ';
        pg_query_params($sql, [
            $token->userId,
            $token->value,
            $token->created->format('Y-m-d H:i:s')
        ]);
    }

    /**
     * @throws NotFoundException
     */
    public function retrieveByValue(string $tokenValue): Token
    {
        $result = pg_query_params('
            SELECT *
            FROM tokens
            WHERE token = $1
        ', [
            $tokenValue,
        ]);
        $values = pg_fetch_array($result);
        if ($values === false) {
            throw new NotFoundException('Token not found.');
        }
        return $this->populate($values);
    }

    private function populate(array $values): Token
    {
        $token = new Token((int) $values['uId'], $values['token']);
        $token->created = new DateTimeImmutable($values['created']);
        return $token;
    }

    public function delete(Token $token): void
    {
        pg_query_params('
            DELETE FROM tokens
            WHERE "uId" = $1
            AND token = $2
        ', [
            $token->userId,
            $token->value,
        ]);
    }

    public function deleteOlderThan(DateTimeImmutable $threshold): void
    {
        pg_query_params('
            DELETE FROM tokens
            WHERE created < $1
        ', [
            $threshold->format('Y-m-d H:i:s'),
        ]);
    }
}
