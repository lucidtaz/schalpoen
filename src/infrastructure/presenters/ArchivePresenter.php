<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use schalpoen\application\interactors\GetPostArchive\GetPostArchiveResponseModel;
use schalpoen\domain\models\Tag;
use schalpoen\infrastructure\viewmodels\archive\ArchiveViewModel;
use schalpoen\infrastructure\viewmodels\archive\MonthViewModel;
use schalpoen\infrastructure\viewmodels\archive\YearViewModel;

class ArchivePresenter
{
    private MinimalPostPresenter $postPresenter;

    public function __construct(MinimalPostPresenter $postPresenter)
    {
        $this->postPresenter = $postPresenter;
    }

    /**
     * @param Tag[] $popularTags
     */
    public function run(GetPostArchiveResponseModel $responseModel, array $popularTags): ArchiveViewModel
    {
        $yearModels = [];
        foreach ($responseModel->postsByYearAndMonth as $year => $postsByMonth) {
            $monthModels = [];
            foreach ($postsByMonth as $month => $posts) {
                $postModels = [];
                foreach ($posts as $post) {
                    $postModels[] = $this->postPresenter->run($post);
                }
                $monthModels[] = new MonthViewModel(
                    $this->formatMonth($year, $month),
                    $postModels
                );
            }

            $postCount = $this->formatPostCountOfYear($postsByMonth);
            $yearModels[] = new YearViewModel(
                "$year ($postCount)",
                $monthModels
            );
        }

        $popularTagTitles = array_map(fn (Tag $tag) => $tag->getTitle(), $popularTags);

        return new ArchiveViewModel($yearModels, $popularTagTitles);
    }

    private function formatPostCountOfYear($yearPosts): string
    {
        $count = $this->countPostsOfYear($yearPosts);
        if ($count === 1) {
            return '1 artikel';
        }
        return "$count artikelen";
    }

    private function countPostsOfYear(array $yearPosts): int
    {
        $result = 0;
        foreach ($yearPosts as $monthPosts) {
            $result += count($monthPosts);
        }
        return $result;
    }

    private function formatMonth(int $year, int $monthNumber): string
    {
        //Example: December 2011
        static $months = ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'];
        $month = $months[$monthNumber - 1];
        return "$month $year";
    }
}
