<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use schalpoen\application\interactors\ChooseTags\ChooseTagsResponseModel;
use schalpoen\domain\models\Tag;
use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\viewmodels\publisher\ChooseTagViewModel;

class ChooseTagPresenter
{
    public function run(ChooseTagsResponseModel $responseModel): ChooseTagViewModel
    {
        return new ChooseTagViewModel(
            $this->renderTagDropdownOptions($responseModel->availableTags),
            '/publisher/posts/' . $responseModel->post->getId() . '/tags',
            $this->renderUntagForms($responseModel->post->getId(), $responseModel->post->getTags())
        );
    }

    /**
     * @param Tag[] $tags
     * @return string[]
     */
    private function renderTagDropdownOptions(array $tags): array
    {
        $tagOptions = array_map(
            fn (Tag $tag) => '<option value="' . $tag->getId() . '">' . htmlspecialchars($tag->getTitle()) . '</option>',
            $tags
        );
        array_unshift($tagOptions, '<option value="-1" disabled selected>Kies een tag</option>');
        return $tagOptions;
    }

    /**
     * @param Tag[] $tags
     * @return string[]
     */
    private function renderUntagForms(int $postId, array $tags): array
    {
        $forms = [];
        foreach ($tags as $tag) {
            $forms[] = $this->renderUntagForm($postId, $tag);
        }
        return $forms;
    }

    private function renderUntagForm(int $postId, Tag $tag): string
    {
        $submitRoute = '/publisher/posts/' . $postId . '/untag';
        $form = '<form action="' . $submitRoute . '" method="post" class="singleButtonForm">';
        $form .= '<input type="hidden" name="tagId" value="' . $tag->getId() . '" />';
        $form .= UiElements::submitButton($tag->getTitle(), '', 'delete');
        $form .= '</form>';
        return $form;
    }
}
