<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use schalpoen\application\parser\Parser;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\infrastructure\components\Toolbox;
use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\viewmodels\post\LongPostViewModel;

class LongPostPresenter
{
    private Parser $parser;

    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    public function run(Post $post): LongPostViewModel
    {
        return new LongPostViewModel(
            $post->url(),
            $post->getPreviewUrl(),
            $post->getAuthor()->niceName(),
            $post->isPublished() ? Toolbox::getNiceTime($post->getPublishedAt()) : 'Nog niet gepubliceerd',
            htmlspecialchars($post->getTitle()),
            $this->parser->blockParse($post->getText()),
            array_map(fn (Tag $tag) => UiElements::tag($tag), $post->getTags())
        );
    }
}
