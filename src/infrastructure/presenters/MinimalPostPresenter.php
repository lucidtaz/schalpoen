<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\viewmodels\post\MinimalPostViewModel;

class MinimalPostPresenter
{
    public function run(Post $post): MinimalPostViewModel
    {
        return new MinimalPostViewModel(
            $post->url(),
            $post->getPreviewUrl(),
            htmlspecialchars($post->getTitle()),
            array_map(fn (Tag $tag) => UiElements::tag($tag), $post->getTags())
        );
    }
}
