<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use schalpoen\application\parser\Parser;
use schalpoen\infrastructure\viewmodels\author\PreviewDraftViewModel;
use schalpoen\infrastructure\widgets\editor\Editor;

class PreviewDraftPresenter
{
    private Parser $parser;

    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @todo Take the result of the interactor as argument, instead of the
     * Editor object which is created by the controller. This requires removal
     * of the Editor component for a simpler, more straightforward solution.
     */
    public function run(Editor $editor): PreviewDraftViewModel
    {
        return new PreviewDraftViewModel(
            '/uploads/content/previews/' . htmlspecialchars($editor->getPreview()),
            $editor->getTitle(),
            $this->parser->blockParse($editor->getText())
        );
    }
}
