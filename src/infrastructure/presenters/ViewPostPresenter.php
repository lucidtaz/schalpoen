<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use Opengraph\Opengraph;
use Opengraph\Writer;
use schalpoen\application\interactors\ViewPost\ViewPostResponseModel;
use schalpoen\application\parser\Parser;
use schalpoen\application\parser\StripParserDecorator;
use schalpoen\domain\models\Comment;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\infrastructure\components\formatting\OgPostFormatter;
use schalpoen\infrastructure\components\Toolbox;
use schalpoen\infrastructure\viewmodels\post\CommentViewModel;
use schalpoen\infrastructure\viewmodels\post\ViewPostViewModel;

class ViewPostPresenter
{
    private LongPostPresenter $longPostPresenter;

    private Parser $parser;

    public function __construct(LongPostPresenter $longPostPresenter, Parser $parser)
    {
        $this->longPostPresenter = $longPostPresenter;
        $this->parser = $parser;
    }

    public function run(ViewPostResponseModel $response): ViewPostViewModel
    {
        $parser = new StripParserDecorator($this->parser);

        return new ViewPostViewModel(
            $response->post->getTitle(),
            $parser->blockParse($response->post->getIntroduction()),
            $this->longPostPresenter->run($response->post),
            $this->createHtmlHeaders($response->post),
            array_map(fn (Tag $tag) => $tag->getTitle(), $response->post->getTags()),
            $response->post->isPublished(),
            array_map(
                fn (Comment $comment) => $this->presentComment($comment, $response->userCanComment),
                $response->post->getRootComments()
            ),
            !$response->post->isPublished(),
            $response->userCanComment,
            $response->userCanEdit,
            $response->userCanPublish,
            '/posts/' . $response->post->getId() . '/comment',
            '/author/posts/' . $response->post->getId(),
            '/publisher/posts/' . $response->post->getId() . '/publish'
        );
    }

    /**
     * @return string[]
     */
    private function createHtmlHeaders(Post $post): array
    {
        $ogFormatter = new OgPostFormatter(new StripParserDecorator($this->parser));

        $openGraph = new Writer();
        $openGraph->append(Opengraph::OG_TITLE, $post->getTitle());
        $openGraph->append(Opengraph::OG_TYPE, Opengraph::TYPE_ARTICLE);
        $openGraph->append(Opengraph::OG_IMAGE, $post->getAbsolutePreviewUrl());
        $openGraph->append(Opengraph::OG_URL, $post->url(true));
        $openGraph->append(Opengraph::OG_DESCRIPTION, $ogFormatter->getIntroduction($post));
        $openGraph->append(Opengraph::OG_SITE_NAME, 'Het Schalpoen');

        $openGraph->append(Opengraph::ARTICLE_PUBLISHED_TIME, $ogFormatter->getDate($post));
        $openGraph->append(Opengraph::ARTICLE_SECTION, 'Technology'); // Let's just assume that!
        foreach ($post->getTags() as $tag) {
            $openGraph->append(Opengraph::ARTICLE_TAG, $tag->getTitle());
        }

        $headers = [];
        foreach ($openGraph->getMetas() as $meta) {
            /* @var $meta \Opengraph\Meta */
            $headers[] = $meta->render();
        }

        $headers[] = "<link rel='canonical' href='" . $post->url(true) . "' />";

        return $headers;
    }

    private function presentComment(Comment $comment, bool $userCanComment, int $depth = 0): CommentViewModel
    {
        return new CommentViewModel(
            $comment->id,
            ($depth % 2) === 0,
            $comment->author->niceName(),
            Toolbox::getNiceTime($comment->getPostedAt()),
            $this->parser->blockParse($comment->text, false), // false means do not trust URLs
            $userCanComment,
            array_map(
                fn (Comment $reply) => $this->presentComment($reply, $userCanComment, $depth + 1),
                $comment->children
            )
        );
    }
}
