<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use schalpoen\application\interactors\ViewTag\ViewTagResponseModel;
use schalpoen\domain\models\Post;
use schalpoen\infrastructure\viewmodels\tag\TagViewModel;

class ViewTagPresenter
{
    private ShortPostPresenter $postPresenter;

    public function __construct(ShortPostPresenter $postPresenter)
    {
        $this->postPresenter = $postPresenter;
    }

    public function present(ViewTagResponseModel $responseModel): TagViewModel
    {
        $presentedPosts = array_map(
            function (Post $post) {
                return $this->postPresenter->run($post);
            },
            $responseModel->posts
        );

        return new TagViewModel(
            htmlspecialchars(ucfirst($responseModel->tag->getTitle())),
            ucfirst($responseModel->tag->getTitle()),
            $presentedPosts
        );
    }
}
