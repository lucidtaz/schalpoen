<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\presenters;

use schalpoen\infrastructure\viewmodels\author\EditDraftViewModel;
use schalpoen\infrastructure\widgets\editor\Editor;

class WriteDraftPresenter
{
    private PreviewDraftPresenter $previewDraftPresenter;

    public function __construct(PreviewDraftPresenter $previewDraftPresenter)
    {
        $this->previewDraftPresenter = $previewDraftPresenter;
    }

    /**
     * @todo Take the result of the interactor as argument, instead of the
     * Editor object which is created by the controller. This requires removal
     * of the Editor component for a simpler, more straightforward solution.
     */
    public function run(Editor $editor): EditDraftViewModel
    {
        if ($editor->hasPostedValues()) {
            $previewDraft = $this->previewDraftPresenter->run($editor);
        } else {
            $previewDraft = null;
        }

        return new EditDraftViewModel(
            '/author/posts/new',
            '/author/posts/new/preview',
            $previewDraft
        );
    }
}
