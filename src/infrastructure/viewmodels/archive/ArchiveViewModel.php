<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\archive;

class ArchiveViewModel
{
    /**
     * @var YearViewModel[]
     */
    public array $yearModels;

    /**
     * @var string[]
     */
    public array $keywords;

    /**
     * @param YearViewModel[] $yearModels
     * @param string[] $keywords
     */
    public function __construct(array $yearModels, array $keywords)
    {
        $this->yearModels = $yearModels;
        $this->keywords = $keywords;
    }
}
