<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\archive;

use schalpoen\infrastructure\viewmodels\post\MinimalPostViewModel;

class MonthViewModel
{
    public string $heading;

    /**
     * @var MinimalPostViewModel[]
     */
    public array $postModels;

    /**
     * @param MinimalPostViewModel[] $postModels
     */
    public function __construct(string $heading, array $postModels)
    {
        $this->heading = $heading;
        $this->postModels = $postModels;
    }
}
