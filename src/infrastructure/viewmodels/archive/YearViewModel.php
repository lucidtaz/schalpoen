<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\archive;

class YearViewModel
{
    public string $heading;

    /**
     * @var MonthViewModel[]
     */
    public array $monthModels;

    /**
     * @param MonthViewModel[] $monthModels
     */
    public function __construct(string $heading, array $monthModels)
    {
        $this->heading = $heading;
        $this->monthModels = $monthModels;
    }
}
