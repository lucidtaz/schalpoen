<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\author;

class EditDraftViewModel
{
    /**
     * Route to submit any draft updates to
     */
    public string $submitRoute;

    /**
     * Route to display a preview of the draft
     */
    public string $previewRoute;

    /**
     * To display a preview of the draft. Only set if the user chose to preview
     * it explicitly by clicking the preview button.
     */
    public ?PreviewDraftViewModel $previewDraft;

    public function __construct(
        string $submitRoute,
        string $previewRoute,
        ?PreviewDraftViewModel $previewDraft
    ) {
        $this->submitRoute = $submitRoute;
        $this->previewRoute = $previewRoute;
        $this->previewDraft = $previewDraft;
    }
}
