<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\author;

class PreviewDraftViewModel
{
    /**
     * URL to the icon of the draft
     */
    public string $previewUrl;

    /**
     * Parsed title of the draft
     */
    public string $title;

    /**
     * Parsed text of the draft
     */
    public string $text;

    public function __construct(
        string $previewUrl,
        string $title,
        string $text
    ) {
        $this->previewUrl = $previewUrl;
        $this->title = $title;
        $this->text = $text;
    }
}
