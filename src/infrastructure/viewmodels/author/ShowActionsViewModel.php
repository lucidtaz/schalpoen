<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\author;

use schalpoen\infrastructure\viewmodels\post\ShortPostViewModel;

class ShowActionsViewModel
{
    /**
     * @var ShortPostViewModel[]
     */
    public array $editablePosts;

    /**
     * @var ShortPostViewModel[]
     */
    public array $finalizedPosts;

    public bool $showNoActionsMessage;

    /**
     * @param ShortPostViewModel[] $editablePosts
     * @param ShortPostViewModel[] $finalizedPosts
     */
    public function __construct(array $editablePosts, array $finalizedPosts, bool $showNoActionsMessage)
    {
        $this->editablePosts = $editablePosts;
        $this->finalizedPosts = $finalizedPosts;
        $this->showNoActionsMessage = $showNoActionsMessage;
    }
}
