<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\layouts;

/**
 * DTO for things that appear on every page with the "main" Layout
 */
class MainLayoutViewModel
{
    public bool $userIsLoggedIn;

    public string $usernameHeader;

    public bool $showAuthorMenuItems;

    public string $draftsLinkText;

    public bool $showPublisherMenuItems;

    public string $publishablePostsLinkText;

    /**
     * @var string[]
     */
    public array $lastPostLinks;

    /**
     * @var string[]
     */
    public array $popularTagLinks;

    /**
     * @param string[] $lastPostLinks
     * @param string[] $popularTagLinks
     */
    public function __construct(
        bool $userIsLoggedIn,
        string $usernameHeader,
        bool $showAuthorMenuItems,
        string $draftsLinkText,
        bool $showPublisherMenuItems,
        string $publishablePostsLinkText,
        array $lastPostLinks,
        array $popularTagLinks
    ) {
        $this->userIsLoggedIn = $userIsLoggedIn;
        $this->usernameHeader = $usernameHeader;
        $this->showAuthorMenuItems = $showAuthorMenuItems;
        $this->draftsLinkText = $draftsLinkText;
        $this->showPublisherMenuItems = $showPublisherMenuItems;
        $this->publishablePostsLinkText = $publishablePostsLinkText;
        $this->lastPostLinks = $lastPostLinks;
        $this->popularTagLinks = $popularTagLinks;
    }
}
