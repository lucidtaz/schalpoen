<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\post;

class CommentViewModel
{
    public int $id;

    /**
     * Whether to highlight or not, for alternating colors in a comment chain
     */
    public bool $highlight;

    public string $authorLink;

    public string $moment;

    public string $text;

    public bool $showReplyBox;

    /**
     * @var CommentViewModel[]
     */
    public array $replies;

    /**
     * @param CommentViewModel[] $replies
     */
    public function __construct(
        int $id,
        bool $highlight,
        string $authorLink,
        string $moment,
        string $text,
        bool $showReplyBox,
        array $replies
    ) {
        $this->id = $id;
        $this->highlight = $highlight;
        $this->authorLink = $authorLink;
        $this->moment = $moment;
        $this->text = $text;
        $this->showReplyBox = $showReplyBox;
        $this->replies = $replies;
    }
}
