<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\post;

class MinimalPostViewModel
{
    /**
     * URL to the post (may be relative)
     */
    public string $url;

    /**
     * URL to the post icon (may be relative)
     */
    public string $previewUrl;

    /**
     * Title of the post
     */
    public string $title;

    /**
     * @var string[] Set of HTML pieces for the clickable tags
     */
    public array $tagLinks;

    public function __construct(
        string $url,
        string $previewUrl,
        string $title,
        array $tagLinks
    ) {
        $this->url = $url;
        $this->previewUrl = $previewUrl;
        $this->title = $title;
        $this->tagLinks = $tagLinks;
    }
}
