<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\post;

class ShortPostViewModel
{
    /**
     * URL to the post (may be relative)
     */
    public string $url;

    /**
     * URL to the post icon (may be relative)
     */
    public string $previewUrl;

    /**
     * HTML for the clickable author
     */
    public string $authorLink;

    /**
     * Formatted publish date and time
     */
    public string $publishedAt;

    /**
     * Title of the post
     */
    public string $title;

    /**
     * Parsed and formatted start of post
     */
    public string $text;

    /**
     * @var string[] Set of HTML pieces for the clickable tags
     */
    public array $tagLinks;

    /**
     * Formatted text like "1 reaction", "2 reactions"
     */
    public string $commentsText;

    public function __construct(
        string $url,
        string $previewUrl,
        string $authorLink,
        string $publishedAt,
        string $title,
        string $text,
        array $tagLinks,
        string $commentsText
    ) {
        $this->url = $url;
        $this->previewUrl = $previewUrl;
        $this->authorLink = $authorLink;
        $this->publishedAt = $publishedAt;
        $this->title = $title;
        $this->text = $text;
        $this->tagLinks = $tagLinks;
        $this->commentsText = $commentsText;
    }
}
