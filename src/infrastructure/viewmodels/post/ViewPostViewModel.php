<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\post;

/**
 * View model for the "view post" functionality
 */
class ViewPostViewModel
{
    public string $title;

    public string $description;

    public LongPostViewModel $post;

    /**
     * @var string[]
     */
    public array $htmlHeaderTags;

    /**
     * @var string[]
     */
    public array $tagTitles;

    public bool $showComments;

    /**
     * @var CommentViewModel[]
     */
    public array $comments;

    /**
     * Whether to show the block of author and publisher action buttons.
     */
    public bool $showActions;

    public bool $userCanComment;

    public bool $userCanEdit;

    public bool $userCanPublish;

    public string $commentRoute;

    public string $editRoute;

    public string $publishRoute;

    /**
     * @param string[] $htmlHeaderTags
     * @param string[] $tagTitles
     * @param CommentViewModel[] $comments
     */
    public function __construct(
        string $title,
        string $description,
        LongPostViewModel $post,
        array $htmlHeaderTags,
        array $tagTitles,
        bool $showComments,
        array $comments,
        bool $showActions,
        bool $userCanComment,
        bool $userCanEdit,
        bool $userCanPublish,
        string $commentRoute,
        string $editRoute,
        string $publishRoute
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->post = $post;
        $this->htmlHeaderTags = $htmlHeaderTags;
        $this->tagTitles = $tagTitles;
        $this->showComments = $showComments;
        $this->comments = $comments;
        $this->showActions = $showActions;
        $this->userCanComment = $userCanComment;
        $this->userCanEdit = $userCanEdit;
        $this->userCanPublish = $userCanPublish;
        $this->commentRoute = $commentRoute;
        $this->editRoute = $editRoute;
        $this->publishRoute = $publishRoute;
    }
}
