<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\publisher;

class ChooseTagViewModel
{
    /**
     * @var string[] HTML option elements for use in a select
     */
    public array $availableTagsDropdownOptions;

    /**
     * Route where the tag selection should be posted to
     */
    public string $submitRoute;

    /**
     * @var string[] Prerendered HTML POST forms to remove current tags
     */
    public array $untagForms;

    /**
     * @param string[] $availableTagsDropdownOptions
     * @param string[] $untagForms
     */
    public function __construct(
        array $availableTagsDropdownOptions,
        string $submitRoute,
        array $untagForms
    ) {
        $this->availableTagsDropdownOptions = $availableTagsDropdownOptions;
        $this->submitRoute = $submitRoute;
        $this->untagForms = $untagForms;
    }
}
