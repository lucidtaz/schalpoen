<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\publisher;

use schalpoen\infrastructure\viewmodels\post\ShortPostViewModel;

class PublishablePostViewModel
{
    public int $id;

    public ShortPostViewModel $post;

    public function __construct(int $id, ShortPostViewModel $post)
    {
        $this->id = $id;
        $this->post = $post;
    }
}
