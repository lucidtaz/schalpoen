<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\rss;

class IndexViewModel
{
    public string $link;

    public string $rssLink;

    public string $copyright;

    public string $lastBuildDate;

    public string $imageUrl;

    /**
     * @var PostViewModel[]
     */
    public array $posts;

    /**
     * @param PostViewModel[] $posts
     */
    public function __construct(
        string $link,
        string $rssLink,
        string $copyright,
        string $lastBuildDate,
        string $imageUrl,
        array $posts
    ) {
        $this->link = $link;
        $this->rssLink = $rssLink;
        $this->copyright = $copyright;
        $this->lastBuildDate = $lastBuildDate;
        $this->imageUrl = $imageUrl;
        $this->posts = $posts;
    }
}
