<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\rss;

class PostViewModel
{
    /**
     * Title of the post
     */
    public string $title;

    /**
     * Parsed and formatted short version of the post
     */
    public string $introduction;

    /**
     * Absolute URL where the full post can be read
     */
    public string $link;

    /**
     * Globally unique id, can use the same value as for link
     */
    public string $guid;

    /**
     * RSS formatted publish date
     */
    public string $publishDate;

    /**
     * The author email and name if applicable
     */
    public ?string $author;

    public function __construct(
        string $title,
        string $introduction,
        string $link,
        string $guid,
        string $publishDate,
        ?string $author
    ) {
        $this->title = $title;
        $this->introduction = $introduction;
        $this->link = $link;
        $this->guid = $guid;
        $this->publishDate = $publishDate;
        $this->author = $author;
    }
}
