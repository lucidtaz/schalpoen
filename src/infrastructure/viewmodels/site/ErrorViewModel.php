<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\site;

class ErrorViewModel
{
    public string $title;

    public string $message;

    public function __construct(string $title, string $message)
    {
        $this->title = $title;
        $this->message = $message;
    }
}
