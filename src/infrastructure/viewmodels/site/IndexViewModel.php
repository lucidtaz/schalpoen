<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\site;

use schalpoen\infrastructure\viewmodels\post\ShortPostViewModel;

class IndexViewModel
{
    /**
     * @var string[]
     */
    public array $popularTagTitles;

    /**
     * @var ShortPostViewModel[]
     */
    public array $posts;

    /**
     * @param string[] $popularTagTitles
     * @param ShortPostViewModel[] $posts
     */
    public function __construct(array $popularTagTitles, array $posts)
    {
        $this->popularTagTitles = $popularTagTitles;
        $this->posts = $posts;
    }
}
