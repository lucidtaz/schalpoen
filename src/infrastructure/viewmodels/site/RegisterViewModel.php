<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\site;

class RegisterViewModel
{
    /**
     * The HTML-formatted captcha question
     */
    public string $question;

    /**
     * The entered username, or empty if nothing was entered yet
     */
    public string $username;

    /**
     * @var string[] Validation errors, if any
     */
    public array $errors;

    /**
     * @param string[] $errors
     */
    public function __construct(string $question, string $username, array $errors)
    {
        $this->question = $question;
        $this->username = $username;
        $this->errors = $errors;
    }
}
