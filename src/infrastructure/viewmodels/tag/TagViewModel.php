<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\tag;

use schalpoen\infrastructure\viewmodels\post\ShortPostViewModel;

class TagViewModel
{
    /**
     * HTML-encoded title
     */
    public string $title;

    /**
     * Raw, unescaped title
     */
    public string $rawTitle;

    /**
     * @var ShortPostViewModel[]
     */
    public array $posts;

    /**
     * @param ShortPostViewModel[] $posts
     */
    public function __construct(string $title, string $rawTitle, array $posts)
    {
        $this->title = $title;
        $this->rawTitle = $rawTitle;
        $this->posts = $posts;
    }
}
