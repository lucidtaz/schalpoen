<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\viewmodels\user;

class ProfileViewModel
{
    /**
     * Title of the profile page
     */
    public string $title;

    /**
     * Display name of the user
     */
    public string $name;

    /**
     * User email address
     */
    public string $email;

    /**
     * Whether the user can edit the email address, or a fresh authentication is
     * needed
     */
    public bool $canEditEmail;

    /**
     * Whether other users can see this user's email address
     */
    public bool $emailHidden;

    /**
     * Whether the user wants to receive an email when someone replies on their
     * comment
     */
    public bool $wantsReplyEmails;

    public function __construct(
        string $title,
        string $name,
        string $email,
        bool $canEditEmail,
        bool $emailHidden,
        bool $wantsReplyEmails
    ) {
        $this->title = $title;
        $this->name = $name;
        $this->email = $email;
        $this->canEditEmail = $canEditEmail;
        $this->emailHidden = $emailHidden;
        $this->wantsReplyEmails = $wantsReplyEmails;
    }
}
