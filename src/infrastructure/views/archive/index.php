<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\archive\ArchiveViewModel;

/* @var $this View */
/* @var $model ArchiveViewModel */

$this->layout->setTitle('Archief');
$this->layout->setDescription('Archiefpagina van het Schalpoen, waar alle posts te zien zijn.');
$this->layout->addKeywords($model->keywords);

?>
<h4>Archief</h4>

<ul class="collapsible" data-collapsible="expandable">
    <?php foreach ($model->yearModels as $yearModel): ?>
        <li>
            <div class="collapsible-header"><?= $yearModel->heading ?></div>
            <div class="collapsible-body" style="display: none;">
                <?php foreach ($yearModel->monthModels as $monthModel): ?>
                    <section class="card">
                        <div class="card-panel">
                            <span class="card-title"><?= $monthModel->heading ?></span>

                            <?php foreach ($monthModel->postModels as $post): ?>
                                <?= $this->renderPartial('../post/_minimal.php', ['model' => $post]) ?>
                            <?php endforeach; ?>
                        </div>
                    </section>
                <?php endforeach; ?>
            </div>
        </li>
    <?php endforeach; ?>
</ul>
