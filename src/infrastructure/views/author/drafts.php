<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\author\ShowActionsViewModel;

/* @var $this View */
/* @var $model ShowActionsViewModel */

$this->layout->setTitle('Concepten');
$this->layout->setDescription('Hier kan een auteur zijn of haar artikelen zien om wijzigingen aan te brengen.');
$this->layout->addKeywords(['drafts', 'concepten', 'wijzig', 'edit', 'wijzigen', 'auteur']);

?>
<h4>Concepten</h4>

<?php if ($model->showNoActionsMessage): ?>
    <div>
        Al je artikelen zijn gepubliceerd en kunnen daarom niet gewijzigd worden.
    </div>
<?php endif; ?>

<?php foreach ($model->finalizedPosts as $postId => $post): ?>
    <form action="/author/posts/<?= $postId ?>/unfinalize" method="post" class="singleButtonForm">
        <?= UiElements::submitButton('Terughalen', '', 'mode_edit') ?>
    </form>
    <form action="/author/posts/<?= $postId ?>/delete" method="post" class="singleButtonForm">
        <?= UiElements::submitButton('Verwijderen', '', 'delete') ?>
    </form>
    <?= $this->renderPartial('../post/_short.php', ['model' => $post]) ?>
<?php endforeach; ?>

<?php foreach ($model->editablePosts as $postId => $post): ?>
    <form action="/author/posts/<?= $postId ?>/finalize" method="post" class="singleButtonForm">
        <?= UiElements::submitButton('Afronden', '', 'mode_edit') ?>
    </form>
    <?= UiElements::button('Wijzigen', 'mode_edit', '/author/posts/' . $postId) ?>
    <form action="/author/posts/<?= $postId ?>/delete" method="post" class="singleButtonForm">
        <?= UiElements::submitButton('Verwijderen', '', 'delete') ?>
    </form>
    <?= $this->renderPartial('../post/_short.php', ['model' => $post]) ?>
<?php endforeach; ?>
