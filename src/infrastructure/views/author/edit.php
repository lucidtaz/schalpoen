<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\author\EditDraftViewModel;
use schalpoen\infrastructure\widgets\editor\Editor;

/* @var $this View */
/* @var $model EditDraftViewModel */
/* @var $editor Editor */

$this->layout->setTitle('Artikel bewerken');
$this->layout->setDescription('Hier kunnen Schalpoenposts worden bewerkt.');
$this->layout->addKeywords(['write', 'schrijven', 'artikel', 'maken']);

?>

<?php if($model->previewDraft !== null): ?>
    <article class="card">
        <div class="card-panel">
            <span class="card-title">Voorbeeld</span>

            Icoon:<br />
            <div class='shortPostPreview' style='float: none; display: inline-block;'>
                <img src='<?= $model->previewDraft->previewUrl ?>' width='120' height='120' alt='Thumbnail' />
            </div>

            <span class="card-title"><?= $model->previewDraft->title ?></span>
            <div>
                <?= $model->previewDraft->text ?>
            </div>
        </div>
    </article>
<?php endif; ?>

<div class='contentPane'>
    <div class="card-panel">
        <span class="card-title">Artikel bewerken</span>
        <?= $editor->render($model->submitRoute, $model->previewRoute) ?>
    </div>
</div>
