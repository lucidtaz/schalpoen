<?php

use schalpoen\infrastructure\components\View;

/* @var $this View */

$this->layout->setTitle('Build a Thing privacy policy');
$this->layout->setDescription('Build a Thing privacy policy');
$this->layout->addKeywords(['Build a Thing', 'privacy policy']);

?>
<h4>Build a Thing</h4>

<article class='card'>
    <div class="card-panel">
        <span class="card-title">Privacy policy</span>

        <p>
            Build a Thing does not transmit any data whatsoever. It does not
            collect any personal data.
        </p>

        <p>
            The only data stored are the settings accessible from the settings
            menu, and the player&apos;s high score as shown on the main menu.
        </p>
    </div>
</article>
