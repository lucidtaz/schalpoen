<?php

use schalpoen\infrastructure\components\View;

/* @var $this View */

$this->layout->setTitle('Crazy Coin Machine privacy policy');
$this->layout->setDescription('Crazy Coin Machine privacy policy');
$this->layout->addKeywords(['Crazy Coin Machine', 'privacy policy']);

?>
<h4>Crazy Coin Machine</h4>

<article class='card'>
    <div class="card-panel">
        <span class="card-title">Privacy policy</span>

        <p>
            Crazy Coin Machine does not transmit any data whatsoever. It does
            not collect any personal data.
        </p>

        <p>
            The only data stored are the settings accessible from the settings
            menu, and the player&apos;s high score as shown on the main menu.
        </p>
    </div>
</article>
