<?php

use schalpoen\infrastructure\components\Toolbox;
use schalpoen\infrastructure\components\view\IsolatedLayout;

/* @var $this IsolatedLayout */
/* @var $content string */

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= htmlspecialchars($this->title) ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="/css/schalpoen.css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <?php if (!empty($this->description)): ?>
            <meta name="description" content="<?= htmlspecialchars($this->description) ?>" />
        <?php endif; ?>
        <?php if (!empty($this->keywords)): ?>
            <meta name="keywords" content="<?= implode(', ', array_map('htmlspecialchars', $this->keywords)) ?>" />
        <?php endif; ?>
        <?php foreach ($this->extraHeaders as $extraHeader): ?>
            <?= $extraHeader ?>
        <?php endforeach; ?>
    </head>

    <body class="container-fluid">

        <nav>
            <div class="nav-wrapper">
                <a href="/" class="brand-logo">Het Schalpoen</a>
            </div>
        </nav>

        <div class="row">
            <main class="col s12 l8 offset-l2"><?= $content ?></main>
        </div>

        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <!--<h5 class="white-text">Footer Content</h5>-->
                        <p class="grey-text text-lighten-4">Please don&apos;t hesitate to contact me!</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="/">Blog</a></li>
                            <li><a class="grey-text text-lighten-3" href="http://portfolio.schalpoen.nl/">Portfolio</a></li>
                            <li><a class="grey-text text-lighten-3" href="https://github.com/LucidTaZ">GitHub</a></li>
                            <li><a class="grey-text text-lighten-3" href="https://twitter.com/Zumbrink">Twitter</a></li>
                            <li><a class="grey-text text-lighten-3" href="https://www.linkedin.com/in/thijs-zumbrink">LinkedIn</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    &copy;2010-<?= date('Y') ?> Thijs Zumbrink
                    <!--<a class="grey-text text-lighten-4 right" href="#!">More Links</a>-->
                </div>
            </div>
        </footer>

        <script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="/js/materialize.min.js"></script>
        <script type="text/javascript">
            $(".button-collapse").sideNav();
            $('select').material_select();
            $('.collapsible').collapsible();
        </script>
        <?php if (Toolbox::showAnalytics()): ?>
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '<?= Toolbox::getAnalyticsId() ?>', 'auto');
                ga('send', 'pageview');
            </script>
        <?php endif; ?>
    </body>
</html>
