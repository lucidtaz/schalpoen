<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\post\CommentViewModel;

/* @var $this View */
/* @var $model CommentViewModel */

$highlight = $model->highlight ? 'highlightBg' : 'lowlightBg';
$replyHighlight = $model->highlight ? 'lowlightBg' : 'highlightBg';

?>
<article class='card'>
    <div class="card-panel <?= $highlight ?>">
        <?= $model->authorLink ?>, <?= $model->moment ?>:<br />
        <?= $model->text ?><br />

        <?php if ($model->showReplyBox): ?>
            <!-- Reply linkje -->
            <br />
            <a id='replyAnchor<?= $model->id ?>' href='#replyAnchor<?= $model->id ?>' onclick='makeReplyBox(<?= $model->id ?>); return false;'>Beantwoorden</a>
            <div id='reply<?= $model->id ?>' class='comment <?= $replyHighlight ?> commentReplyForm' style='display: none;'></div>
            <br />
        <?php endif; ?>

        <?php if(!empty($model->replies)): ?>
            <br />
            <section class='commentReplies'>
                <?php foreach ($model->replies as $reply): ?>
                    <?= $this->renderPartial('_comment.php', ['model' => $reply]) ?>
                <?php endforeach; ?>
            </section>
        <?php endif; ?>
    </div>
</article>
