<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\post\LongPostViewModel;

/* @var $this View */
/* @var $model LongPostViewModel */

?>
<article class='card'>
    <div class="card-panel">

        <span class="preview">
            <img src='<?= $model->previewUrl ?>' alt='Thumbnail' />
        </span>

        <span class='card-title'><?= $model->title ?></span>

        <p>
            Door: <?= $model->authorLink ?><br />
            <?= $model->publishedAt ?>
        </p>

        <p>
            <?= $model->text ?><br />
        </p>

        <?php if (!empty($model->tagLinks)): ?>
            <p>
                <?= implode('', $model->tagLinks) ?>
            </p>
        <?php endif; ?>
    </div>
</article>
