<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\post\MinimalPostViewModel;

/* @var $this View */
/* @var $model MinimalPostViewModel */

?>
<section class="card">
    <div class="card-panel center">
        <div>
            <p>
                <a href='<?= $model->url ?>'>
                    <img src='<?= $model->previewUrl ?>' alt='Thumbnail' />
                </a>
            </p>

            <span class="card-title"><a href="<?= $model->url ?>"><?= $model->title ?></a></span>

            <?php if (!empty($model->tagLinks)): ?>
                <br />
                <?= implode('', $model->tagLinks) ?><br />
            <?php endif; ?>
        </div>
    </div>
</section>
