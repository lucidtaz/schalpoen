<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\post\ShortPostViewModel;

/* @var $this View */
/* @var $model ShortPostViewModel */

?>
<article class="card">
    <div class="card-panel">
        <span class="preview">
            <img onclick="document.location.href='<?= $model->url ?>';" style="cursor: pointer;" src="<?= $model->previewUrl ?>" width="120" height="120" alt="Thumbnail" />
        </span>
        <section>
            <span class="card-title"><a href="<?= $model->url ?>"><?= $model->title ?></a></span>
            <p>
                Door: <?= $model->authorLink ?><br />
                <?= $model->publishedAt ?>
            </p>

            <p>
                <?= $model->text ?>
            </p>

            <?php if (!empty($model->tagLinks)): ?>
                <p>
                    <?= implode('', $model->tagLinks) ?>
                </p>
            <?php endif; ?>

            <section class="card-action">
                <a href="<?= $model->url ?>">Lees verder</a>
                <a href="<?= $model->url ?>#reacties"><?= $model->commentsText ?></a>
            </section>
        </section>
    </div>
</article>
