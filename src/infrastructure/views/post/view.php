<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\post\ViewPostViewModel;

/* @var $this View */
/* @var $model ViewPostViewModel */

// FIXME: Double HTML encoding (probably) going on here:
$this->layout->setTitle($model->title);
$this->layout->setDescription($model->description);
$this->layout->addKeywords($model->tagTitles);
$this->layout->addHtmlHeaders($model->htmlHeaderTags);

?>

<?= $this->renderPartial('_long.php', ['model' => $model->post]) ?>

<?php if ($model->showComments): ?>
    <section class='card'>
        <div class="card-panel">
            <span class="card-title">Reacties</span>
            <a id='reacties'></a>

            <?php if ($model->userCanComment): ?>
                <form action='<?= $model->commentRoute ?>' method='post'>
                    <div class="input-field">
                        <textarea id="comment-input" class="materialize-textarea" name='text'></textarea>
                        <label for="comment-input">Reactie...</label>
                    </div>
                    <?= UiElements::submitButton('Reageer') ?><br />
                </form>
                <!-- Stukje javascript voor de reply linkjes: -->
                <script type='text/javascript'>
                    function makeReplyBox (parentCommentId) {
                        var replyBox = document.getElementById('reply' + parentCommentId);
                        replyBox.innerHTML =
                            '<form action=\"<?= $model->commentRoute ?>\" method=\"post\">' +
                            '<input type=\"hidden\" name=\"parent\" value=\"' + parentCommentId + '\" />' +
                            '<label for="text_' + parentCommentId + '">Reactie</label>' +
                            '<div class=\"input-field\">' +
                            '<textarea name="text" id="text_' + parentCommentId + '" class="materialize-textarea" style="width: 70%"></textarea>' +
                            '</div>' +
                            '<p>' +
                            '<?= addslashes(UiElements::submitButton('Reageer')) ?>' +
                            '</p>' +
                            '</form>';
                        replyBox.style.display = 'block';
                        document.getElementById('replyAnchor' + parentCommentId).style.display = 'none';
                    }
                </script>
            <?php else: ?>
                <br />
                <a href='/login'>Log in</a> of <a href='/register'>registreer</a> om reacties te plaatsen.<br />
            <?php endif; ?>

            <?php foreach ($model->comments as $comment): ?>
                <?= $this->renderPartial('_comment.php', ['model' => $comment]) ?>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
<?php if ($model->showActions): ?>
    <section class="card">
        <div class="card-panel">
            <a id='acties'></a>
            <span class="card-title">Acties</span><br />
            <?= $model->userCanEdit ? UiElements::button('Wijzigen', 'mode_edit', $model->editRoute) : '' ?>
            <?= $model->userCanPublish ? UiElements::button('Publiceren', 'check', $model->publishRoute) : '' ?>
        </div>
    </section>
<?php endif; ?>
