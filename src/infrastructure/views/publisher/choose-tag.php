<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\publisher\ChooseTagViewModel;

/* @var $this View */
/* @var $model ChooseTagViewModel */

$this->layout->setTitle('Publiceren');
$this->layout->setDescription('Hier kunnen Schalpoenposts worden gepubliceerd.');
$this->layout->addKeywords(['publish', 'publiceren', 'publicatie', 'redactie', 'redacteur']);

?>
<section class="card">
    <div class="card-panel">
        <span class="card-title">Tags toevoegen</span>
        <form name="chooseTag" action="<?= $model->submitRoute ?>" method="post">

            <div class="input-field">
                <select name="tagId">
                    <?= implode("\n", $model->availableTagsDropdownOptions) ?>
                </select>
                <label for="tagId">Beschikbare tags</label>
            </div>

            <div class="input-field">
                <input type='text' name='newTag' id="newTag"" />
                <label for="newTag">Nieuwe tag</label>
            </div>

            <p>
                <?= UiElements::submitButton('OK') ?>
            </p>

        </form>
    </div>
</section>

<?php if (!empty($model->untagForms)): ?>
    <section class="card">
        <div class="card-panel">
            <span class="card-title">Tags verwijderen</span>
            <p>
                <?= implode("\n", $model->untagForms) ?>
            </p>
        </div>
    </section>
<?php endif; ?>
