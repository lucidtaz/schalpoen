<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\publisher\PublishablePostViewModel;

/* @var $this View */
/* @var $models PublishablePostViewModel[] */

$this->layout->setTitle('Publiceren');
$this->layout->setDescription('Hier kunnen Schalpoenposts worden gepubliceerd.');
$this->layout->addKeywords(['publish', 'publiceren', 'publicatie', 'redactie', 'redacteur']);

?>
<h4>Publiceren</h4>

<?php if (empty($models)): ?>
    <div>
        Er staan geen posts klaar om gepubliceerd te worden.
    </div>
<?php else: ?>
    <?php foreach ($models as $model): ?>
        <form action="/publisher/posts/<?= $model->id ?>/publish" method="post" class="singleButtonForm">
            <?= UiElements::submitButton('Publiceren', '', 'check') ?>
        </form>
        <?= UiElements::button('Tags bewerken', 'mode_edit', '/publisher/posts/' . $model->id . '/tags') ?>
        <form action="/publisher/posts/<?= $model->id ?>/reject" method="post" class="singleButtonForm">
            <?= UiElements::submitButton('Afkeuren', '', 'thumb_down') ?>
        </form>
        <br />
        <?= $this->renderPartial('../post/_short.php', ['model' => $model->post]) ?>
    <?php endforeach; ?>
<?php endif; ?>
