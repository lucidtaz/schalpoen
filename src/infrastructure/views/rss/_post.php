<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\rss\PostViewModel;

/* @var $this View */
/* @var $model PostViewModel */

?>
<item>
    <title><?= $model->title ?></title>
    <description><?= $model->introduction ?></description>
    <link><?= $model->link ?></link>
    <guid isPermaLink='true'><?= $model->link ?></guid>
    <pubDate><?= $model->publishDate ?></pubDate>

    <?php if ($model->author !== null): ?>
        <author><?= $model->author ?></author>
    <?php endif; ?>

</item>
