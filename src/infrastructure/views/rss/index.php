<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\rss\IndexViewModel;

/* @var $this View */
/* @var $model IndexViewModel */

//RSS specification: http://cyber.law.harvard.edu/rss/rss.html

$posts = '';
foreach ($model->posts as $post) {
    $posts .= $this->renderPartial('_post.php', ['model' => $post]);
}

$xml = <<<XML
<?xml version="1.0"?>
<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>
    <channel>
        <description>De immer boeiende verhalen van Het Schalpoen!</description>
        <title>Het Schalpoen</title>
        <link>{$model->link}</link>
        <atom:link href='{$model->rssLink}' rel='self' type='application/rss+xml' />
        <language>nl-nl</language>
        <copyright>{$model->copyright}</copyright>
        <managingEditor>thijs@schalpoen.nl (Thijs Zumbrink)</managingEditor>
        <webMaster>thijs@schalpoen.nl (Thijs Zumbrink)</webMaster>
        <lastBuildDate>{$model->lastBuildDate}</lastBuildDate>
        <image>
            <url>{$model->imageUrl}</url>
            <width>120</width>
            <height>120</height>
            <title>Het Schalpoen</title>
            <link>{$model->link}</link>
        </image>
        $posts
    </channel>
</rss>
XML;

echo $xml;
