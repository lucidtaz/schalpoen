<?php

declare(strict_types=1);

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\site\ErrorViewModel;

/* @var $this View */
/* @var $model ErrorViewModel */

// Note: we don't use $model->title as page title here because it shouldn't be double-HTML encoded
$this->layout->setTitle('Error');

?>
<section class="card">
    <div class="card-panel">
        <h4><?= $model->title ?></h4>

        <img src='/style/error.jpg' class="scaleWidth" alt="Woops, I divided by zero!" /><br />
        <?= $model->message ?>
    </div>
</section>
