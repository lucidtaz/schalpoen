<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\site\IndexViewModel;

/* @var $this View */
/* @var $model IndexViewModel */

$this->layout->setTitle('Het Schalpoen');
$this->layout->setDescription('De enige blog waar je het spannende leven van Thijs kunt volgen!');

$this->layout->addKeywords(['Thijs Zumbrink', 'Thijs', 'Zumbrink', 'Schalpoen']);
$this->layout->addKeywords($model->popularTagTitles);

?>
<?php foreach ($model->posts as $post): ?>
    <?= $this->renderPartial('../post/_short.php', ['model' => $post]) ?>
<?php endforeach; ?>
