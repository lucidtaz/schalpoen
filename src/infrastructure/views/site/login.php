<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;

/* @var $this View */
/* @var $loginError string|null */

$this->layout->setTitle('Inloggen');
$this->layout->setDescription('Inlogpagina voor je Schalpoen-account.');

?>
<?php if ($loginError !== null): ?>
    <div class='error'><?= $loginError ?></div>
<?php endif; ?>
<section class='card'>
    <div class="card-panel">
        <h4>Inloggen</h4>
        <form name='loginForm' action='/login' method='post'>
            <div class="input-field">
                <input type='text' name='userName' id="userName" />
                <label for="userName">Gebruikersnaam</label>
            </div>
            <div class="input-field">
                <input type='password' name='passwd' id='passwd' />
                <label for="passwd">Wachtwoord</label>
            </div>
            <p>
                <input type='checkbox' name='persistent' id='persistent' value='1' />
                <label for='persistent'>Onthoud mij</label>
            </p>
            <p>
                <?= UiElements::submitButton('Inloggen', '', 'perm_identity') ?>
            </p>
        </form>
        <br />
        <br />
        Nog geen account? Registreren is heel makkelijk en kost slechts een paar seconden! <a href='/register'>Klik hier!</a>
    </div>
</section>
