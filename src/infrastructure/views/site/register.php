<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\site\RegisterViewModel;

/* @var $this View */
/* @var $model RegisterViewModel */

$this->layout->setTitle('Registreren');
$this->layout->setDescription('Maak hier je eigen Schalpoen-account!');

?>
<section class='card'>
    <div class="card-panel">
        <h4>Registreren</h4>

        <?php if (!empty($model->errors)): ?>
            <div class='error'>
                Registratie is mislukt.<br />
                <ul>
                    <?php foreach ($model->errors as $error): ?>
                        <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
       <?php endif; ?>

        <form name='registrationForm' action='/register' method='post'>
            <div class="input-field">
                <input type='text' name='username' id="username" value="<?= $model->username ?>" />
                <label for="username">Gebruikersnaam</label>
            </div>
            <div class="input-field">
                <input type='password' name='password' id='password' />
                <label for="password">Wachtwoord</label>
            </div>
            <?= $model->question ?><br />
            <div class="input-field">
                <input type='text' name='answer' id="answer" value='' /><br />
                <label for="answer">Antwoord</label>
            </div>
            <p>
                <?= UiElements::submitButton('Registreren', '', 'perm_identity') ?>
            </p>
        </form>
    </div>
</section>
