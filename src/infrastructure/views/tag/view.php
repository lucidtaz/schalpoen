<?php

use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\tag\TagViewModel;

/* @var $this View */
/* @var $model TagViewModel */

$this->layout->setTitle('Tag: ' . $model->rawTitle);
$this->layout->setDescription('Overzicht van alle berichten met de tag ' . $model->rawTitle . '.');
$this->layout->addKeywords([$model->rawTitle]);

?>
<h4><?= $model->title ?></h4>

<?php foreach ($model->posts as $post): ?>
    <?= $this->renderPartial('../post/_short.php', ['model' => $post]) ?>
<?php endforeach; ?>
