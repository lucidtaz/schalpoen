<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\viewmodels\user\ProfileViewModel;

/* @var $this View */
/* @var $model ProfileViewModel */

// FIXME: Double HTML encoding going on here:
$this->layout->setTitle($model->title);

?>
<section class='card'>
    <div class="card-panel">
        <span class="card-title"><?= $model->title ?></span>
        <form name='profileForm' action='/profile' method='post'>
            <input type='hidden' name='submitted' value='1' />

            <div class="input-field">
                <input type='text' name='displayName' id="displayName" value="<?= $model->name ?>" />
                <label for="displayName">Weergave naam</label>
            </div>

            <div class="input-field">
                <?php if ($model->canEditEmail): ?>
                    <input type='email' name='email' id="displayName" value="<?= $model->email ?>" />
                <?php else: ?>
                    <input type='email' name='email' id="displayName" value="<?= $model->email ?>" disabled />
                    <div class="red-text">
                        Om je e-mail adres te wijzigen dien je eerst opnieuw in te loggen.
                    </div>
                <?php endif; ?>
                <label for="email">E-mail adres</label>
            </div>

            <p>
                <input type="hidden" name="emailHidden" value="0" />
                <input type='checkbox' id='emailHidden' name='emailHidden' value='1' <?= $model->emailHidden ? 'checked="checked"' : '' ?> />
                <label for='emailHidden'>Verberg mijn e-mail adres voor anderen</label>
            </p>

            <p>
                <input type="hidden" name="notifyReplies" value="0" />
                <input type='checkbox' id='notifyReplies' name='notifyReplies' value='1' <?= $model->wantsReplyEmails ? 'checked="checked"' : '' ?> />
                <label for='notifyReplies'>E-mail me als iemand reageert op mijn reacties</label>
            </p>

            <p>
                <?= UiElements::submitButton('Wijzigen') ?>
            </p>
        </form>
    </div>
</section>
