<?php

use schalpoen\infrastructure\components\View;
use schalpoen\domain\models\User;

/* @var $this View */
/* @var $user User */

$this->layout->setTitle($user->getName());
$this->layout->setDescription('Profielpagina voor ' . $user->getName() . '.');
$this->layout->addKeywords([$user->getName()]);

?>
<article class='card'>
    <div class="card-content">
        <span class="card-title"><?= htmlspecialchars($user->getName()) ?></span>
    </div>
</article>
