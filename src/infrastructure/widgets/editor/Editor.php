<?php

declare(strict_types=1);

namespace schalpoen\infrastructure\widgets\editor;

use schalpoen\infrastructure\components\View;

class Editor
{
    /**
     * Post id being updated
     */
    private ?int $id;

    private string $title = '';

    /**
     * Preview icon filename
     */
    private string $preview = 'schalpoen.png';

    private string $text = '';

    /**
     * Whether the data in the editor came from a previous (http) post,
     * indicating that the editor should show a rendered preview of the input so
     * far.
     */
    private bool $hasPostedValues = false;

    public function render(string $target, string $previewTarget): string
    {
        $view = new View(__DIR__ . '/views/', null);
        return $view->render('_editor.php', [
            'editor' => $this,
            'previewTarget' => $previewTarget,
            'target' => $target,
        ]);
    }

    public function getUpdateId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPreview(): string
    {
        return $this->preview;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function hasPostedValues(): bool
    {
        return $this->hasPostedValues;
    }

    public function setHasPostedValues(bool $hasPostedValues): void
    {
        $this->hasPostedValues = $hasPostedValues;
    }

    public function setPostValues(?int $id, string $title, string $preview, string $text): void
    {
        $this->id = $id;
        $this->title = $title;
        $this->preview = $preview;
        $this->text = $text;
    }
}
