<?php

use schalpoen\infrastructure\components\UiElements;
use schalpoen\infrastructure\components\View;
use schalpoen\infrastructure\widgets\editor\Editor;

/* @var $this View */
/* @var $editor Editor */
/* @var $target string */
/* @var $previewTarget string */

?>
<form action='<?= htmlspecialchars($target) ?>' method='post'>
    <?= $editor->getUpdateId() !== null ? '<input type="hidden" name="id" value="' . $editor->getUpdateId() . '" />' : '' ?>
    <div class="input-field">
        <input type='text' name='title' id="title" value="<?= htmlspecialchars($editor->getTitle()) ?>" />
        <label for="title">Titel</label>
    </div>

    <div class="input-field">
        <input type='text' name='preview' id="preview" value="<?= htmlspecialchars($editor->getPreview()) ?>" />
        <label for="preview">Preview</label>
    </div>

    <div class="input-field">
        <textarea id="text" class="materialize-textarea" name='text'><?= htmlspecialchars($editor->getText()) ?></textarea>
        <label for="text">Tekst</label>
    </div>

    <div class="center">
        <?= UiElements::submitButton('Voorbeeld', 'submit', 'send', $previewTarget) ?>
        <?= UiElements::submitButton('OK', 'submit') ?>
    </div>
</form>
