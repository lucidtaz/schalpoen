<?php

declare(strict_types=1);

namespace schalpoen\tests\integration;

class HomeTest extends IntegrationTestCase
{
    public function testIndex(): void
    {
        $request = $this->factory->createServerRequest('GET', '/');
        $response = $this->application->handle($request);

        $this->assertEquals(200, $response->getStatusCode());

        $responseBody = (string) $response->getBody();

        $this->assertStringContainsString('<title>Het Schalpoen</title>', $responseBody);
    }

    public function testContentTypeIsSetByDefault(): void
    {
        $request = $this->factory->createServerRequest('GET', '/');
        $response = $this->application->handle($request);

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaderLine('Content-Type');

        $this->assertEquals('text/html; charset=utf-8', $contentType);
    }
}
