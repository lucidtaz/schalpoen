<?php

declare(strict_types=1);

namespace schalpoen\tests\integration;

use Http\Factory\Guzzle\ServerRequestFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Server\RequestHandlerInterface;
use schalpoen\infrastructure\components\Application;
use schalpoen\infrastructure\components\Toolbox;

/**
 * TestCase for making application-level tests
 *
 * These use HTTP inputs and outputs to the Kernel object. In effect testing the
 * entire application except the application server and web server containers.
 */
class IntegrationTestCase extends TestCase
{
    protected ServerRequestFactoryInterface $factory;

    protected RequestHandlerInterface $application;

    protected function setUp(): void
    {
        parent::setUp();
        $this->factory = $this->instantiateFactory();
        $this->application = $this->instantiateApplication();
    }

    protected function instantiateFactory(): ServerRequestFactoryInterface
    {
        return new ServerRequestFactory();
    }

    protected function instantiateApplication(): RequestHandlerInterface
    {
        $config = Toolbox::getConfig();
        $application = new Application($config);
        return $application->getHttpKernel();
    }
}
