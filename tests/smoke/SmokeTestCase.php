<?php

declare(strict_types=1);

namespace schalpoen\tests\smoke;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use PHPUnit\Framework\TestCase;

/**
 * TestCase for making real HTTP requests to the web server container
 */
class SmokeTestCase extends TestCase
{
    protected ClientInterface $client;

    protected string $baseUri = 'http://web';

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = $this->instantiateHttpClient();
    }

    protected function instantiateHttpClient(): ClientInterface
    {
        return new Client(['base_uri' => $this->baseUri]);
    }
}
