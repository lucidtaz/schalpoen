<?php

declare(strict_types=1);

namespace schalpoen\tests\smoke;

use Psr\Http\Message\ResponseInterface;

class WebserverTest extends SmokeTestCase
{
    public function testIndex(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->client->get('/');
        $this->assertEquals(200, $response->getStatusCode());

        $responseBody = (string) $response->getBody();

        $this->assertStringContainsString('<title>Het Schalpoen</title>', $responseBody);
        // Check for error absence. We can't assert absence of the word "error"
        // because it can appear in the seeded Lorem Ipsum.
        $this->assertStringNotContainsString(
            '.php',
            $responseBody,
            'There must be no sign of a displayed error'
        );
    }

    public function testSecurityHeaders(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->client->get('/');

        $this->assertEquals(200, $response->getStatusCode());

        $cspHeaders = $response->getHeader('Content-Security-Policy');
        $this->assertNotEmpty($cspHeaders);
        $cspHeader = $cspHeaders[0];

        $this->assertStringContainsString("default-src 'none'", $cspHeader, 'All rules are explicitly allowed');
        $this->assertStringContainsString("style-src 'self' 'unsafe-inline' fonts.googleapis.com", $cspHeader);
        $this->assertStringContainsString("font-src 'self' fonts.gstatic.com", $cspHeader);
        $this->assertStringContainsString("script-src 'self' 'unsafe-inline' fonts.googleapis.com", $cspHeader);
        $this->assertStringContainsString('img-src *', $cspHeader);
        $this->assertStringContainsString('frame-src https://www.youtube.com', $cspHeader);

        $contentTypeOptionsHeaders = $response->getHeader('X-Content-Type-Options');
        $this->assertNotEmpty($contentTypeOptionsHeaders);
        $this->assertEquals('nosniff', $contentTypeOptionsHeaders[0]);

        $frameOptionsHeaders = $response->getHeader('X-Frame-Options');
        $this->assertNotEmpty($frameOptionsHeaders);
        $this->assertEquals('deny', $frameOptionsHeaders[0]);

        $xssProtectionHeaders = $response->getHeader('X-XSS-Protection');
        $this->assertNotEmpty($xssProtectionHeaders);
        $this->assertEquals('1', $xssProtectionHeaders[0]);
    }

    public function testSessionCookie(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->client->get('/');

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertArrayHasKey('Set-Cookie', $response->getHeaders());
        $cookieHeaders = $response->getHeader('Set-Cookie');
        $this->assertCount(1, $cookieHeaders);

        // Note: in production the cookie has the secure attribute as well, but
        // this is turned off in the local stack.
        $expected = '#PHPSESSID=[a-zA-Z0-9]+; path=/; HttpOnly#';
        $this->assertRegExp(
            $expected,
            $cookieHeaders[0],
            'Session ID cookie must contain secure and HttpOnly'
        );
    }
}
