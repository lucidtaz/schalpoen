<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\ChangePostState;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\exceptions\ValidationException;
use schalpoen\application\interactors\ChangePostState\ChangePostState;
use schalpoen\application\interactors\ChangePostState\ChangePostStateInputModel;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;
use PHPUnit\Framework\TestCase;

class ChangePostStateTest extends TestCase
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;

    private ChangePostState $interactor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->postRepository = new MemoryPostRepository();
        $this->interactor = new ChangePostState($this->postRepository, $this->userRepository);
    }

    public function testDraftIsFinalized(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $draft = new Post($author, 'Test title', 'Test text');
        $this->postRepository->insert($draft);

        $inputModel = new ChangePostStateInputModel($author->id, $draft->id, Post::STATUS_FINISHED);
        $updatedDraft = $this->interactor->run($inputModel);

        $this->assertEquals($draft->id, $updatedDraft->id);
        $this->assertEquals(Post::STATUS_FINISHED, $updatedDraft->status);
    }

    public function testPublicationUpdatesTimestamp(): void
    {
        $authorAndPublisher = new User('Author and publisher');
        $authorAndPublisher->isAuthor = true;
        $authorAndPublisher->isPublisher = true;
        $this->userRepository->insert($authorAndPublisher);

        $finalizedPost = new Post($authorAndPublisher, 'Test title', 'Test text');
        $finalizedPost->setState(Post::STATUS_FINISHED);
        $this->postRepository->insert($finalizedPost);

        $inputModel = new ChangePostStateInputModel($authorAndPublisher->id, $finalizedPost->id, Post::STATUS_PUBLISHED);
        $post = $this->interactor->run($inputModel);

        $this->assertEquals($finalizedPost->id, $post->id);
        $this->assertTrue($post->isPublished());
        $this->assertNotNull($post->getPublishedAt());
    }

    public function testPostMustExist(): void
    {
        $guest = new User('Guest');
        $this->userRepository->insert($guest);

        $inputModel = new ChangePostStateInputModel($guest->id, 1337, Post::STATUS_FINISHED);

        $this->expectException(NotFoundException::class);

        $this->interactor->run($inputModel);
    }

    public function testOnlyAuthorCanFinalize(): void
    {
        $guest = new User('Guest');
        $this->userRepository->insert($guest);

        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $draft = new Post($author, 'Test title', 'Test text');
        $this->postRepository->insert($draft);

        $inputModel = new ChangePostStateInputModel($guest->id, $draft->id, Post::STATUS_FINISHED);

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Not authorized.');

        $this->interactor->run($inputModel);
    }

    public function testOnlyPublisherCanPublish(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $author->isPublisher = false;
        $this->userRepository->insert($author);

        $draft = new Post($author, 'Test title', 'Test text');
        $draft->setState(Post::STATUS_FINISHED);
        $this->postRepository->insert($draft);

        $inputModel = new ChangePostStateInputModel($author->id, $draft->id, Post::STATUS_PUBLISHED);

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('You are no publisher.');

        $this->interactor->run($inputModel);
    }

    public function testStatusTransitionIsChecked(): void
    {
        $authorAndPublisher = new User('Publisher');
        $authorAndPublisher->isAuthor = true;
        $authorAndPublisher->isPublisher = true;
        $this->userRepository->insert($authorAndPublisher);

        $draft = new Post($authorAndPublisher, 'Test title', 'Test text');
        $this->postRepository->insert($draft);

        $inputModel = new ChangePostStateInputModel($authorAndPublisher->id, $draft->id, Post::STATUS_PUBLISHED);

        try {
            $this->interactor->run($inputModel);
            $this->fail('A ValidationException must be thrown.');
        } catch (ValidationException $e) {
            $this->assertEquals(['Not a valid state transition.'], $e->getErrors());
        }
    }
}
