<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\CreateComment;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\parser\Parser;
use schalpoen\application\interactors\CreateComment\CreateComment;
use schalpoen\application\interactors\CreateComment\CreateCommentInputModel;
use schalpoen\domain\models\Comment;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\application\persistence\CommentRepository;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\application\parser\NullParser;
use schalpoen\application\email\MockEmailGateway;
use schalpoen\tests\unit\mocks\persistence\MemoryCommentRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;
use PHPUnit\Framework\TestCase;

class CreateCommentTest extends TestCase
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;
    private CommentRepository $commentRepository;

    private MockEmailGateway $emailGateway;
    private Parser $parser;

    /**
     * The system under test
     */
    private CreateComment $interactor;

    /**
     * Prepared test data
     */
    private Post $post;

    /**
     * Prepared test data
     */
    private User $newCommentAuthor;

    /**
     * Prepared test data
     */
    private Comment $topLevelComment;

    /**
     * Prepared test data
     */
    private Comment $authorsTopLevelComment;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->postRepository = new MemoryPostRepository();
        $this->commentRepository = new MemoryCommentRepository();
        $this->emailGateway = new MockEmailGateway();
        $this->parser = new NullParser();

        $this->interactor = new CreateComment(
            $this->commentRepository,
            $this->postRepository,
            $this->userRepository,
            $this->emailGateway,
            $this->parser
        );

        /*
         * The test data is as follows:
         * - One post, written by "Unit Test Post Author Name" (unittest_post_author@example.com)
         *   $this->post
         * - One user that is supposed to write a comment
         *   $this->newCommentAuthor
         * - One top-level comment, written by "Unit Test Parent Author Name" (unittest_parent_author@example.com)
         *   $this->topLevelComment
         * - Another top-level comment, written by the post author
         *   $this->authorsTopLevelComment
         */
        $topLevelParentAuthor = new User('Unit Test Parent Comment Author Name');
        $topLevelParentAuthor->email = 'unittest_parent_author@example.com';
        $topLevelParentAuthor->notifyReplies = true;

        $postAuthor = new User('Unit Test Post Author Name');
        $postAuthor->email = 'unittest_post_author@example.com';

        $this->newCommentAuthor = new User('Unit Test Comment Author Name');

        $this->post = new Post($postAuthor, 'Unit Test Post', 'Test text');
        $this->post->status = Post::STATUS_PUBLISHED;

        $this->topLevelComment = new Comment($this->post, $topLevelParentAuthor, 'Top-level comment');

        $this->authorsTopLevelComment = new Comment($this->post, $postAuthor, 'Authors top-level comment');

        $this->userRepository->insert($topLevelParentAuthor);
        $this->userRepository->insert($postAuthor);
        $this->userRepository->insert($this->newCommentAuthor);
        $this->postRepository->insert($this->post);
        $this->commentRepository->insert($this->topLevelComment);
    }

    public function testCommentIsCreated(): void
    {
        $inputModel = new CreateCommentInputModel(
            $this->post->getAuthor()->id, // The author is commenting on their own post
            $this->post->id,
            'Some text',
            null
        );
        $comment = $this->interactor->run($inputModel);

        $this->assertEquals($this->post, $comment->post);
        $this->assertEquals($comment, $this->commentRepository->retrieve($comment->id));
    }

    public function testCommentCanReplyToParent(): void
    {
        $inputModel = new CreateCommentInputModel(
            $this->post->getAuthor()->id, // The commenter is commenting on their own post
            $this->post->id,
            'Some text',
            $this->topLevelComment->id
        );
        $comment = $this->interactor->run($inputModel);

        $this->assertEquals($this->post, $comment->post);
        $this->assertEquals($this->topLevelComment, $comment->parent);
    }

    public function testCommentingIsForbiddenForUnpublishedPosts(): void
    {
        $post = new Post(
            $this->post->getAuthor(),
            'Unit Test',
            'This post should be unpublished.'
        );
        $this->postRepository->insert($post);

        $inputModel = new CreateCommentInputModel(
            $this->newCommentAuthor->id,
            $post->id,
            'Some text',
            null
        );

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Not authorized.');
        $this->interactor->run($inputModel);
    }

    public function testEmailIsSentToPostAuthor(): void
    {
        $inputModel = new CreateCommentInputModel(
            $this->newCommentAuthor->id,
            $this->post->id,
            'Some text',
            null
        );
        $this->interactor->run($inputModel);

        $this->assertCount(1, $this->emailGateway->sentEmails);
        $recipientAddresses = array_keys($this->emailGateway->sentEmails[0]->to);
        $recipientNames = array_values($this->emailGateway->sentEmails[0]->to);
        $this->assertEquals($this->post->getAuthor()->getEmail(), $recipientAddresses[0]);
        $this->assertEquals($this->post->getAuthor()->getName(), $recipientNames[0]);
        $this->assertEquals('Reactie op Unit Test Post', $this->emailGateway->sentEmails[0]->subject);
    }

    public function testEmailIsSentToParentCommenter(): void
    {
        // If post author and top level comment author would be the same, there would be only one email, not two
        $this->assertNotEquals($this->post->getAuthor(), $this->topLevelComment->author, 'Test precondition');

        $inputModel = new CreateCommentInputModel(
            $this->newCommentAuthor->id,
            $this->post->id,
            'Some text',
            $this->topLevelComment->id
        );
        $this->interactor->run($inputModel);

        $this->assertCount(2, $this->emailGateway->sentEmails);
        $firstEmailRecipientAddresses = array_keys($this->emailGateway->sentEmails[0]->to);
        $firstEmailRecipientNames = array_values($this->emailGateway->sentEmails[0]->to);
        $this->assertEquals($this->topLevelComment->author->getEmail(), $firstEmailRecipientAddresses[0]);
        $this->assertEquals($this->topLevelComment->author->getName(), $firstEmailRecipientNames[0]);
        $this->assertEquals('Antwoord op je reactie in artikel: Unit Test Post', $this->emailGateway->sentEmails[0]->subject);

        $secondEmailRecipientAddresses = array_keys($this->emailGateway->sentEmails[1]->to);
        $secondEmailRecipientNames = array_values($this->emailGateway->sentEmails[1]->to);
        $this->assertEquals($this->post->getAuthor()->getEmail(), $secondEmailRecipientAddresses[0]);
        $this->assertEquals($this->post->getAuthor()->getName(), $secondEmailRecipientNames[0]);
        $this->assertEquals('Reactie op Unit Test Post', $this->emailGateway->sentEmails[1]->subject);
    }

    public function testEmailIsNotSentTwice(): void
    {
        // If the post author is also the parent comment author, prevent two mails from being sent
        $this->assertEquals($this->post->getAuthor(), $this->authorsTopLevelComment->author, 'Test precondition');

        $inputModel = new CreateCommentInputModel(
            $this->newCommentAuthor->id,
            $this->post->id,
            'Some text',
            $this->authorsTopLevelComment->id
        );
        $this->interactor->run($inputModel);

        $this->assertCount(1, $this->emailGateway->sentEmails);
        $firstEmailRecipientAddresses = array_keys($this->emailGateway->sentEmails[0]->to);
        $firstEmailRecipientNames = array_values($this->emailGateway->sentEmails[0]->to);
        $this->assertEquals($this->post->getAuthor()->getEmail(), $firstEmailRecipientAddresses[0]);
        $this->assertEquals($this->post->getAuthor()->getName(), $firstEmailRecipientNames[0]);
        $this->assertEquals('Reactie op Unit Test Post', $this->emailGateway->sentEmails[0]->subject);
    }

    public function testNoEmailIsNotSentForCommentingOnOwnPost(): void
    {
        $inputModel = new CreateCommentInputModel(
            $this->post->getAuthor()->id,
            $this->post->id,
            'Some text',
            $this->authorsTopLevelComment->id
        );
        $this->interactor->run($inputModel);

        $this->assertCount(0, $this->emailGateway->sentEmails);
    }

    public function testPostCommentCountIsIncremented(): void
    {
        // Explicitly retrieve, even though we have a reference already, for
        // consistency with the postcondition assertions
        $postBefore = $this->postRepository->retrieve($this->post->id);
        $this->assertEquals(0, $postBefore->commentCount, 'Test precondition');

        $inputModel = new CreateCommentInputModel(
            $this->newCommentAuthor->id,
            $this->post->id,
            'Some text',
            null
        );
        $this->interactor->run($inputModel);

        $postAfter = $this->postRepository->retrieve($this->post->id);
        $this->assertEquals(1, $postAfter->commentCount);
    }
}
