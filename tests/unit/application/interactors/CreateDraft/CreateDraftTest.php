<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\CreateDraft;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\interactors\CreateDraft\CreateDraft;
use schalpoen\application\interactors\CreateDraft\CreateDraftInputModel;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;
use PHPUnit\Framework\TestCase;

class CreateDraftTest extends TestCase
{
    private MemoryUserRepository $userRepository;
    private MemoryPostRepository $postRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->postRepository = new MemoryPostRepository();
    }

    public function testPostIsCreated(): void
    {
        $mockAuthor = new User('Mock author');
        $mockAuthor->isAuthor = true;
        $this->userRepository->insert($mockAuthor);

        $inputModel = new CreateDraftInputModel(
            $mockAuthor->id,
            'Test title',
            'test_preview.png',
            'Lorem ipsum dolor sit amet.'
        );
        $interactor = new CreateDraft($this->userRepository, $this->postRepository);

        $post = $interactor->run($inputModel);

        $this->assertEquals($mockAuthor, $post->getAuthor());
        $this->assertEquals('Test title', $post->getTitle());
        $this->assertEquals('Lorem ipsum dolor sit amet.', $post->getText());
        $this->assertEquals('test_preview.png', $post->getPreview());
        $this->assertEquals(Post::STATUS_DRAFT, $post->status);

        $retrievedPost = $this->postRepository->retrieve($post->id);
        $this->assertEquals($post, $retrievedPost);
    }

    public function testAuthorMustExist(): void
    {
        $inputModel = new CreateDraftInputModel(
            1337,
            'Test title',
            'test_preview.png',
            'Lorem ipsum dolor sit amet.'
        );
        $interactor = new CreateDraft($this->userRepository, $this->postRepository);

        $this->expectException(NotFoundException::class);

        $interactor->run($inputModel);
    }

    public function testNonAuthorsCannotCreateDrafts(): void
    {
        $mockGuest = new User('Mock regular guest');
        $this->userRepository->insert($mockGuest);

        $inputModel = new CreateDraftInputModel(
            $mockGuest->id,
            'Test title',
            'test_preview.png',
            'Lorem ipsum dolor sit amet.'
        );
        $interactor = new CreateDraft($this->userRepository, $this->postRepository);

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Only authors can create drafts.');

        $interactor->run($inputModel);
    }
}
