<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\EditDraft;

use PHPUnit\Framework\TestCase;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\interactors\EditDraft\EditDraft;
use schalpoen\application\interactors\EditDraft\EditDraftInputModel;
use schalpoen\application\persistence\PostRepository;
use schalpoen\application\persistence\UserRepository;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;

class EditDraftTest extends TestCase
{
    private UserRepository $userRepository;
    private PostRepository $postRepository;

    private EditDraft $interactor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->postRepository = new MemoryPostRepository();
        $this->interactor = new EditDraft($this->userRepository, $this->postRepository);
    }

    public function testValidUserAndPost(): void
    {
        $user = new User('Author');
        $user->isAuthor = true;
        $this->userRepository->insert($user);

        $post = new Post($user, 'Unit test', 'Unit test post');
        $post->setPreview('preview.png');
        $this->postRepository->insert($post);

        $input = new EditDraftInputModel($user->getId(), $post->getId());

        $result = $this->interactor->run($input);

        $this->assertEquals($post, $result);
    }

    public function testUserNotFound(): void
    {
        $post = new Post(new User('Author'), 'Unit test', 'Unit test post');
        $this->postRepository->insert($post);

        $input = new EditDraftInputModel(1337, $post->getId());

        $this->expectException(LoginException::class);
        $this->expectExceptionMessage('You must be logged in.');

        $this->interactor->run($input);
    }

    public function testPostNotFound(): void
    {
        $user = new User('Author');
        $user->isAuthor = true;
        $this->userRepository->insert($user);

        $input = new EditDraftInputModel($user->getId(), 1337);

        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Post not found.');

        $this->interactor->run($input);
    }

    public function testUserNotAuthorized(): void
    {
        $user = new User('Guest');
        $this->assertFalse($user->isAuthor(), 'Test precondition');
        $this->userRepository->insert($user);

        $post = new Post($user, 'Unit test', 'Unit test post');
        $this->postRepository->insert($post);

        $input = new EditDraftInputModel($user->getId(), $post->getId());

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Only authors can edit drafts.');

        $this->interactor->run($input);
    }
}
