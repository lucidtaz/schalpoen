<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\GetPostArchive;

use schalpoen\application\interactors\GetPostArchive\GetPostArchive;
use schalpoen\domain\models\Post;
use schalpoen\application\persistence\PostRepository;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use PHPUnit\Framework\TestCase;

class GetPostArchiveTest extends TestCase
{
    private PostRepository $postRepository;

    private GetPostArchive $interactor;

    protected function setUp(): void
    {
        parent::setUp();

        $this->postRepository = new MemoryPostRepository();
        $this->interactor = new GetPostArchive($this->postRepository);

        $author = new User('Author');

        $publishedPost1 = new Post($author, 'Published Post 1', 'Test text');
        $publishedPost1->status = Post::STATUS_PUBLISHED;
        $publishedPost1->publishedUnixTime = 12345;
        $this->postRepository->insert($publishedPost1);

        $publishedPost2 = new Post($author, 'Published Post 2', 'Test text');
        $publishedPost2->status = Post::STATUS_PUBLISHED;
        $publishedPost2->publishedUnixTime = 23456;
        $this->postRepository->insert($publishedPost2);

        $unpublishedPost = new Post($author, 'Draft Post', 'Test text');
        $unpublishedPost->publishedUnixTime = 34567;
        $this->postRepository->insert($unpublishedPost);
    }

    public function testReturnValueIsAKeyedArray(): void
    {
        $result = $this->interactor->run();
        $this->assertArrayHasKey(1970, $result->postsByYearAndMonth);
        $this->assertArrayHasKey(1, $result->postsByYearAndMonth[1970]);
    }

    public function testOnlyPublishedPostsAreReturned(): void
    {
        $result = $this->interactor->run();
        $flattened = $this->flattenResult($result->postsByYearAndMonth);

        $this->assertCount(2, $flattened);
        $this->assertCount(2, array_filter($flattened, fn(Post $post) => $post->isPublished()));
    }

    /**
     * @return Post[]
     */
    private function flattenResult(array $postsByMonthAndYear): array
    {
        $result = [];
        foreach ($postsByMonthAndYear as $year => $postsByMonth) {
            foreach ($postsByMonth as $month => $posts) {
                foreach ($posts as $post) {
                    $result[] = $post;
                }
            }
        }
        return $result;
    }

    public function testPostsInTheSameBucketAreSorted(): void
    {
        $result = $this->interactor->run();
        $january1970Posts = $result->postsByYearAndMonth[1970][1];

        $this->assertCount(2, $january1970Posts);
        $this->assertTrue($this->isSorted(
            $january1970Posts,
            fn(Post $a, Post $b) => $a->getPublishedAt() <=> $b->getPublishedAt())
        );
    }

    /**
     * @param Post[] $posts
     */
    private function isSorted(array $posts, callable $comparator): bool
    {
        $_posts = array_values($posts);
        $n = \count($_posts);
        for ($i = 1; $i < $n; $i++) {
            if ($comparator($_posts[$i-1], $_posts[$i]) !== 1) {
                return false;
            }
        }
        return true;
    }
}
