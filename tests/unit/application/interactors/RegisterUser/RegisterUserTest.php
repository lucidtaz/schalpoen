<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\RegisterUser;

use schalpoen\application\exceptions\ValidationException;
use schalpoen\application\interactors\RegisterUser\RegisterUser;
use schalpoen\application\interactors\RegisterUser\RegisterUserInputModel;
use schalpoen\domain\models\Captcha;
use schalpoen\domain\models\User;
use schalpoen\infrastructure\persistence\HardcodedCaptchaRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;
use PHPUnit\Framework\TestCase;

class RegisterUserTest extends TestCase
{
    private MemoryUserRepository $userRepository;
    private HardcodedCaptchaRepository $captchaRepository;

    private RegisterUser $interactor;

    private Captcha $testCaptcha;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->captchaRepository = new HardcodedCaptchaRepository();
        $this->interactor = new RegisterUser($this->userRepository, $this->captchaRepository);

        $this->testCaptcha = $this->captchaRepository->retrieve(0);
    }

    public function testUserIsCreatedAndInserted(): void
    {
        $inputModel = new RegisterUserInputModel(
            'new-name',
            $this->testCaptcha->id,
            $this->testCaptcha->answers[0]
        );

        $user = $this->interactor->run($inputModel);

        $this->assertNotNull($user->getId());

        $storedUser = $this->userRepository->retrieve($user->getId());
        $this->assertSame($user, $storedUser);
    }

    public function testDisplaynameMustNotAlreadyBeTaken(): void
    {
        $this->userRepository->insert(
            new User('taken-name')
        );

        $inputModel = new RegisterUserInputModel(
            'taken-name',
            $this->testCaptcha->id,
            $this->testCaptcha->answers[0]
        );

        try {
            $this->interactor->run($inputModel);
            $this->fail('ValidationException is expected');
            $this->addToAssertionCount(1);
        } catch (ValidationException $e) {
            $this->addToAssertionCount(1);
            $this->assertEquals(['De gebruikersnaam is bezet.'], $e->getErrors());
        }
    }

    public function testCaptchaIsValidated(): void
    {
        $inputModel = new RegisterUserInputModel(
            'new-name',
            $this->testCaptcha->id,
            'some-wrong-answer'
        );

        try {
            $this->interactor->run($inputModel);
            $this->fail('ValidationException is expected');
            $this->addToAssertionCount(1);
        } catch (ValidationException $e) {
            $this->addToAssertionCount(1);
            $this->assertEquals(['Vul de verificatievraag correct in s.v.p.'], $e->getErrors());
        }
    }
}
