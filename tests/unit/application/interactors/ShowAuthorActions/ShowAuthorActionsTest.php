<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\ShowAuthorActions;

use PHPUnit\Framework\TestCase;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\interactors\ShowAuthorActions\ShowAuthorActions;
use schalpoen\application\interactors\ShowAuthorActions\ShowAuthorActionsInputModel;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;

class ShowAuthorActionsTest extends TestCase
{
    private MemoryUserRepository $userRepository;
    private MemoryPostRepository $postRepository;

    private ShowAuthorActions $interactor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->postRepository = new MemoryPostRepository();
        $this->interactor = new ShowAuthorActions($this->userRepository, $this->postRepository);
    }

    public function testUserMustExist(): void
    {
        $inputModel = new ShowAuthorActionsInputModel(1);

        $this->expectException(LoginException::class);
        $this->expectExceptionMessage('Please login.');

        $this->interactor->run($inputModel);
    }

    public function testUserMustBeAuthor(): void
    {
        $user = new User('Guest');
        $user->isAuthor = false;
        $this->userRepository->insert($user);

        $inputModel = new ShowAuthorActionsInputModel($user->getId());

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('User is not an author.');

        $this->interactor->run($inputModel);
    }

    public function testPostsAreReturned(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $this->insertDraft($author); // shows up in draft list
        $this->insertFinalizedDraft($author); // shows up in finalized list
        $this->insertRejectedDraft($author); // shows up in draft list
        $this->insertPublishedPost($author); // should not show up
        $this->insertDeletedPost($author); // should not show up

        $inputModel = new ShowAuthorActionsInputModel($author->getId());

        $responseModel = $this->interactor->run($inputModel);

        $this->assertCount(2, $responseModel->drafts);
        $this->assertCount(1, $responseModel->finalizedPosts);
    }

    private function insertDraft(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $this->postRepository->insert($post);
    }

    private function insertFinalizedDraft(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_FINISHED);
        $this->postRepository->insert($post);
    }

    private function insertRejectedDraft(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_REJECTED);
        $this->postRepository->insert($post);
    }

    private function insertPublishedPost(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_PUBLISHED);
        $this->postRepository->insert($post);
    }

    private function insertDeletedPost(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_DELETED);
        $this->postRepository->insert($post);
    }
}
