<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\ShowPublisherActions;

use PHPUnit\Framework\TestCase;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\interactors\ShowPublisherActions\ShowPublisherActions;
use schalpoen\application\interactors\ShowPublisherActions\ShowPublisherActionsInputModel;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;

class ShowPublisherActionsTest extends TestCase
{
    private MemoryUserRepository $userRepository;
    private MemoryPostRepository $postRepository;

    private ShowPublisherActions $interactor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->postRepository = new MemoryPostRepository();
        $this->interactor = new ShowPublisherActions($this->userRepository, $this->postRepository);
    }

    public function testUserMustExist(): void
    {
        $inputModel = new ShowPublisherActionsInputModel(1);

        $this->expectException(LoginException::class);
        $this->expectExceptionMessage('Please login.');

        $this->interactor->run($inputModel);
    }

    public function testUserMustBePublisher(): void
    {
        $user = new User('Guest');
        $user->isPublisher = false;
        $this->userRepository->insert($user);

        $inputModel = new ShowPublisherActionsInputModel($user->getId());

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('User is not a publisher.');

        $this->interactor->run($inputModel);
    }

    public function testPostsAreReturned(): void
    {
        $authorAndPublisher = new User('Author and publisher');
        $authorAndPublisher->isPublisher = true;
        $this->userRepository->insert($authorAndPublisher);

        $this->insertDraft($authorAndPublisher); // should not show up
        $this->insertFinalizedDraft($authorAndPublisher);
        $this->insertRejectedDraft($authorAndPublisher); // should not show up
        $this->insertPublishedPost($authorAndPublisher); // should not show up
        $this->insertDeletedPost($authorAndPublisher); // should not show up

        $inputModel = new ShowPublisherActionsInputModel($authorAndPublisher->getId());

        $responseModel = $this->interactor->run($inputModel);

        $this->assertCount(1, $responseModel->publishablePosts);
        /** @var Post $post */
        $post = array_pop($responseModel->publishablePosts);
        $this->assertTrue($post->isPublishable());
    }

    public function testPostsFromDifferentAuthorsAreReturned(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $publisher = new User('Publisher');
        $publisher->isPublisher = true;
        $this->userRepository->insert($publisher);

        $this->insertFinalizedDraft($author);

        $inputModel = new ShowPublisherActionsInputModel($publisher->getId());

        $responseModel = $this->interactor->run($inputModel);

        $this->assertCount(1, $responseModel->publishablePosts);
        /** @var Post $post */
        $post = array_pop($responseModel->publishablePosts);
        $this->assertTrue($post->isPublishable());
    }

    private function insertDraft(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $this->postRepository->insert($post);
    }

    private function insertFinalizedDraft(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_FINISHED);
        $this->postRepository->insert($post);
    }

    private function insertRejectedDraft(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_REJECTED);
        $this->postRepository->insert($post);
    }

    private function insertPublishedPost(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_PUBLISHED);
        $this->postRepository->insert($post);
    }

    private function insertDeletedPost(User $author): void
    {
        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_DELETED);
        $this->postRepository->insert($post);
    }
}
