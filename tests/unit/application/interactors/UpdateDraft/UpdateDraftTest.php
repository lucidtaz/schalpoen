<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\UpdateDraft;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\interactors\UpdateDraft\UpdateDraft;
use schalpoen\application\interactors\UpdateDraft\UpdateDraftInputModel;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;
use PHPUnit\Framework\TestCase;

class UpdateDraftTest extends TestCase
{
    private MemoryUserRepository $userRepository;
    private MemoryPostRepository $postRepository;

    private User $mockAuthor;

    private Post $mockPost;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->postRepository = new MemoryPostRepository();

        $this->mockAuthor = new User('Mock author');
        $this->mockAuthor->isAuthor = true;
        $this->userRepository->insert($this->mockAuthor);

        $this->mockPost = new Post($this->mockAuthor, 'Test title', 'Test text');
        $this->postRepository->insert($this->mockPost);
    }

    public function testPostIsUpdated(): void
    {
        $inputModel = new UpdateDraftInputModel(
            $this->mockAuthor->id,
            $this->mockPost->id,
            'Test title',
            'test_preview.png',
            'Lorem ipsum dolor sit amet.'
        );
        $interactor = new UpdateDraft($this->userRepository, $this->postRepository);

        $post = $interactor->run($inputModel);

        $this->assertEquals('Test title', $post->getTitle());
        $this->assertEquals('Lorem ipsum dolor sit amet.', $post->getText());
        $this->assertEquals('test_preview.png', $post->getPreview());
        $retrievedPost = $this->postRepository->retrieve($post->id);
        $this->assertEquals($post, $retrievedPost);
    }

    public function testPostMustExist(): void
    {
        $inputModel = new UpdateDraftInputModel(
            $this->mockAuthor->id,
            1337,
            'Test title',
            'test_preview.png',
            'Lorem ipsum dolor sit amet.'
        );
        $interactor = new UpdateDraft($this->userRepository, $this->postRepository);

        $this->expectException(NotFoundException::class);

        $interactor->run($inputModel);
    }

    public function testAuthorMustExist(): void
    {
        $inputModel = new UpdateDraftInputModel(
            1337,
            $this->mockPost->id,
            'Test title',
            'test_preview.png',
            'Lorem ipsum dolor sit amet.'
        );
        $interactor = new UpdateDraft($this->userRepository, $this->postRepository);

        $this->expectException(NotFoundException::class);

        $interactor->run($inputModel);
    }

    public function testOnlyAuthorCanUpdateDraft(): void
    {
        $mockGuest = new User('Mock guest');
        $this->userRepository->insert($mockGuest);

        $inputModel = new UpdateDraftInputModel(
            $mockGuest->id,
            $this->mockPost->id,
            'Test title',
            'test_preview.png',
            'Lorem ipsum dolor sit amet.'
        );
        $interactor = new UpdateDraft($this->userRepository, $this->postRepository);

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Only draft authors can update their drafts.');

        $interactor->run($inputModel);
    }
}
