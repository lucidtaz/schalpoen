<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\UpdateProfile;

use schalpoen\application\exceptions\LoginException;
use schalpoen\application\interactors\UpdateProfile\UpdateProfile;
use schalpoen\application\interactors\UpdateProfile\UpdateProfileInputModel;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;
use PHPUnit\Framework\TestCase;

class UpdateProfileTest extends TestCase
{
    private MemoryUserRepository $userRepository;

    private UpdateProfile $interactor;

    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->interactor = new UpdateProfile($this->userRepository);

        $this->user = new User('The Display Name');
        $this->user->email = 'the@email.com';
        $this->userRepository->insert($this->user);
    }

    public function testProfileIsUpdated(): void
    {
        $inputModel = new UpdateProfileInputModel(
            $this->user->id,
            true,
            'New Display Name',
            'new@email.com',
            false,
            true
        );
        $updatedUser = $this->interactor->run($inputModel);

        $this->assertEquals($this->user->id, $updatedUser->id);
        $this->assertEquals('New Display Name', $updatedUser->getName());
        $this->assertEquals('new@email.com', $updatedUser->email);
        $this->assertFalse($updatedUser->emailHidden());
        $this->assertTrue($updatedUser->wantsReplyNotify());
    }

    public function testDisplaynameIsTruncated(): void
    {
        $inputModel = new UpdateProfileInputModel(
            $this->user->id,
            true,
            'New Display Name That Is Too Long For This Site',
            'new@email.com',
            false,
            true
        );
        $updatedUser = $this->interactor->run($inputModel);

        $this->assertEquals('New Display Name That Is Too L', $updatedUser->getName());
    }

    public function testSafeFieldsAreUpdatedWithoutReliableAuthentication(): void
    {
        $inputModel = new UpdateProfileInputModel(
            $this->user->id,
            false,
            'New Display Name',
            'the@email.com',
            false,
            true
        );
        $updatedUser = $this->interactor->run($inputModel);

        $this->assertEquals($this->user->id, $updatedUser->id);
        $this->assertEquals('New Display Name', $updatedUser->getName());
        $this->assertEquals('the@email.com', $updatedUser->email, 'Email address is unchanged');
        $this->assertFalse($updatedUser->emailHidden());
        $this->assertTrue($updatedUser->wantsReplyNotify());
    }

    public function testEmailUpdateIsBlockedWithoutReliableAuthentication(): void
    {
        $inputModel = new UpdateProfileInputModel(
            $this->user->id,
            false,
            'New Display Name',
            'new@email.com',
            false,
            true
        );

        $this->expectException(LoginException::class);
        $this->expectExceptionMessage('Please login again before updating your email address');
        $this->interactor->run($inputModel);
    }
}
