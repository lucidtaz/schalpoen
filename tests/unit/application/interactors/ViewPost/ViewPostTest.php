<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\ViewPost;

use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\interactors\ViewPost\ViewPost;
use schalpoen\application\interactors\ViewPost\ViewPostInputModel;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;
use PHPUnit\Framework\TestCase;

class ViewPostTest extends TestCase
{
    private MemoryPostRepository $postRepository;
    private MemoryUserRepository $userRepository;

    private ViewPost $interactor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->postRepository = new MemoryPostRepository();
        $this->userRepository = new MemoryUserRepository();
        $this->interactor = new ViewPost($this->postRepository, $this->userRepository);
    }

    public function testDraftPostIsDisplayedToAuthor(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $post = new Post($author, 'Test title', 'Test text');
        $this->postRepository->insert($post);

        $inputModel = new ViewPostInputModel($post->id, $author->id);

        $responseModel = $this->interactor->run($inputModel);

        $this->assertEquals($post, $responseModel->post);
        $this->assertTrue($responseModel->userCanEdit);
        $this->assertFalse($responseModel->userCanPublish);
    }

    public function testPublishedPostIsDisplayedToGuest(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $post = new Post($author, 'Test title', 'Test text');
        $post->setState(Post::STATUS_PUBLISHED);
        $this->postRepository->insert($post);

        $inputModel = new ViewPostInputModel($post->id, null);

        $responseModel = $this->interactor->run($inputModel);

        $this->assertEquals($post, $responseModel->post);
        $this->assertFalse($responseModel->userCanEdit);
        $this->assertFalse($responseModel->userCanPublish);
    }

    public function testUnpublishedIsForbiddenForGuest(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $post = new Post($author, 'Test title', 'Test text');
        $this->postRepository->insert($post);

        $inputModel = new ViewPostInputModel($post->id, null);

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('This post is not published yet.');
        $this->interactor->run($inputModel);
    }

    public function testFinishedPostIsDisplayedToPublisher(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $publisher = new User('Publisher');
        $publisher->isPublisher = true;
        $this->userRepository->insert($publisher);

        $post = new Post($author, 'Test title', 'Test text');
        $post->status = Post::STATUS_FINISHED;
        $this->postRepository->insert($post);

        $inputModel = new ViewPostInputModel($post->id, $publisher->id);

        $responseModel = $this->interactor->run($inputModel);

        $this->assertEquals($post, $responseModel->post);
        $this->assertFalse($responseModel->userCanEdit);
        $this->assertTrue($responseModel->userCanPublish);
    }

    public function testUnfinishedPostIsForbiddenForPublisher(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $this->userRepository->insert($author);

        $publisher = new User('Publisher');
        $publisher->isPublisher = true;
        $this->userRepository->insert($publisher);

        $post = new Post($author, 'Test title', 'Test text');
        $this->postRepository->insert($post);

        $inputModel = new ViewPostInputModel($post->id, $publisher->id);

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Not authorized.');
        $this->interactor->run($inputModel);
    }

    public function testFinishedPostIsForbiddenForOtherAuthors(): void
    {
        $postAuthor = new User('Author');
        $postAuthor->isAuthor = true;
        $this->userRepository->insert($postAuthor);

        $otherAuthor = new User('Author');
        $otherAuthor->isAuthor = true;
        $this->userRepository->insert($otherAuthor);

        $post = new Post($postAuthor, 'Test title', 'Test text');
        $post->status = Post::STATUS_FINISHED;
        $this->postRepository->insert($post);

        $inputModel = new ViewPostInputModel($post->id, $otherAuthor->id);


        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Not authorized.');
        $this->interactor->run($inputModel);
    }
}
