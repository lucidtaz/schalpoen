<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\ViewTag;

use PHPUnit\Framework\TestCase;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\interactors\ViewTag\ViewTag;
use schalpoen\application\interactors\ViewTag\ViewTagInputModel;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryPostRepository;
use schalpoen\tests\unit\mocks\persistence\MemoryTagRepository;

class ViewTagTest extends TestCase
{
    private MemoryTagRepository $tagRepository;
    private MemoryPostRepository $postRepository;

    private ViewTag $interactor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->tagRepository = new MemoryTagRepository();
        $this->postRepository = new MemoryPostRepository();
        $this->interactor = new ViewTag($this->tagRepository, $this->postRepository);
    }

    public function testRequestedTagIsReturned(): void
    {
        $a = new Tag('Tag A');
        $b = new Tag('Tag B');
        $c = new Tag('Tag C');

        $this->tagRepository->insert($a);
        $this->tagRepository->insert($b);
        $this->tagRepository->insert($c);

        $inputModel = new ViewTagInputModel($b->getId());
        $responseModel = $this->interactor->run($inputModel);

        $this->assertEquals($b, $responseModel->tag);
    }

    public function testPostlessTagIsFound(): void
    {
        $tag = new Tag('Tag A');

        $this->tagRepository->insert($tag);

        $inputModel = new ViewTagInputModel($tag->getId());
        $responseModel = $this->interactor->run($inputModel);

        $this->assertEmpty($responseModel->posts);
    }

    public function testPostsAreReturnedWithATag(): void
    {
        $tag = new Tag('Tag A');
        $this->tagRepository->insert($tag);

        $author = new User('Unit test author');
        $author->isAuthor = true;

        $unassociatedPublishedPost = new Post($author, 'Unassociated post', 'Some text');
        $unassociatedPublishedPost->setState(Post::STATUS_PUBLISHED);

        $associatedDraftPost = new Post($author, 'Associated post 1', 'Some text');
        $associatedDraftPost->tagIds = [$tag->id];

        $associatedPublishedPost = new Post($author, 'Associated post 2', 'Some text');
        $associatedPublishedPost->tagIds = [$tag->id];
        $associatedPublishedPost->setState(Post::STATUS_PUBLISHED);

        $this->postRepository->insert($unassociatedPublishedPost);
        $this->postRepository->insert($associatedDraftPost);
        $this->postRepository->insert($associatedPublishedPost);

        $inputModel = new ViewTagInputModel($tag->getId());
        $responseModel = $this->interactor->run($inputModel);

        $this->assertCount(1, $responseModel->posts);
        $this->assertNotContains($unassociatedPublishedPost, $responseModel->posts);
        $this->assertNotContains($associatedDraftPost, $responseModel->posts);
        $this->assertContains($associatedPublishedPost, $responseModel->posts);
    }

    public function testNonExistingTagRaisesAnException(): void
    {
        $inputModel = new ViewTagInputModel(1337);

        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Tag not found.');

        $this->interactor->run($inputModel);
    }
}
