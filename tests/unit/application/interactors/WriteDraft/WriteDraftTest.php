<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\interactors\WriteDraft;

use PHPUnit\Framework\TestCase;
use schalpoen\application\exceptions\ForbiddenException;
use schalpoen\application\exceptions\LoginException;
use schalpoen\application\interactors\WriteDraft\WriteDraft;
use schalpoen\application\interactors\WriteDraft\WriteDraftInputModel;
use schalpoen\domain\models\User;
use schalpoen\tests\unit\mocks\persistence\MemoryUserRepository;

class WriteDraftTest extends TestCase
{
    private MemoryUserRepository $userRepository;

    private WriteDraft $interactor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new MemoryUserRepository();
        $this->interactor = new WriteDraft($this->userRepository);
    }

    public function testValidUser(): void
    {
        $user = new User('Author');
        $user->isAuthor = true;
        $this->userRepository->insert($user);

        $input = new WriteDraftInputModel($user->getId());

        $this->interactor->run($input);

        $this->addToAssertionCount(1); // No Exception was thrown
    }

    public function testUserNotFound(): void
    {
        $input = new WriteDraftInputModel(1337);

        $this->expectException(LoginException::class);
        $this->expectExceptionMessage('You must be logged in.');

        $this->interactor->run($input);
    }

    public function testUserNotAuthorized(): void
    {
        $user = new User('Guest');
        $this->assertFalse($user->isAuthor(), 'Test precondition');
        $this->userRepository->insert($user);

        $input = new WriteDraftInputModel($user->getId());

        $this->expectException(ForbiddenException::class);
        $this->expectExceptionMessage('Only authors can write drafts.');

        $this->interactor->run($input);
    }
}
