<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\application\parser;

use PHPUnit\Framework\TestCase;
use schalpoen\application\parser\BbParser;
use schalpoen\domain\models\Smiley;
use schalpoen\tests\unit\mocks\persistence\MemorySmileyRepository;

class BbParserTest extends TestCase
{
    private MemorySmileyRepository $smileyRepository;

    private BbParser $parser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->smileyRepository = new MemorySmileyRepository();
        $this->parser = new BbParser($this->smileyRepository);
    }

    public function testInlineParse(): void
    {
        $input = '[b]Some text[/b]';
        $expected = '<span style=\'font-weight:bold;\'>Some text</span>';

        $parsed = $this->parser->inlineParse($input);

        $this->assertEquals($expected, $parsed);
    }

    public function testInlineParseIgnoresBlockTags(): void
    {
        $input = '[code]Some text[/code]';
        $expected = '[code]Some text[/code]';

        $parsed = $this->parser->inlineParse($input);

        $this->assertEquals($expected, $parsed);
    }

    public function testBlockParse(): void
    {
        $input = '[code]Some text[/code]';
        $expected = '<div style=\'padding: 0.5em; white-space: pre; font-family: "Courier New", Courier, monospace; border: 1px solid black; overflow: auto;\'>Some text</div>';

        $parsed = $this->parser->blockParse($input);

        $this->assertEquals($expected, $parsed);
    }

    public function testBlockParseWithInlineTags(): void
    {
        $input = '[b]Some text[/b]';
        $expected = '<span style=\'font-weight:bold;\'>Some text</span>';

        $parsed = $this->parser->blockParse($input);

        $this->assertEquals($expected, $parsed);
    }

    public function testSmileys(): void
    {
        $smiley = new Smiley(0, 'the/path.png', ':D', 30, 40);
        $this->smileyRepository->add($smiley);

        $input = 'Some smiley: :D';
        $expected = 'Some smiley: <img src="http://localhost:8080/uploads/smileys/the/path.png" style="border: none;" alt=":D" />';

        $parsed = $this->parser->inlineParse($input);

        $this->assertEquals($expected, $parsed);
    }

    public function testSmileysMatchTheLongestLink(): void
    {
        $this->markTestSkipped('Bug: Smiley alt text output is parsed again and broken as a result.');

        $smile = new Smiley(1, 'smile.png', ':D', 30, 40);
        $drool = new Smiley(2, 'drool.png', ':D~', 30, 40);

        $smileys = [$smile, $drool];
        shuffle($smileys);
        foreach ($smileys as $smiley) {
            $this->smileyRepository->add($smiley);
        }

        $input = ':D~';
        $expected = 'Some smiley: <img src="http://localhost/uploads/smileys/drool.png" style="border: none;" alt=":D~" />';

        $parsed = $this->parser->inlineParse($input);

        $this->assertEquals($expected, $parsed);
    }
}
