<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\domain\models;

use schalpoen\domain\models\Captcha;
use PHPUnit\Framework\TestCase;

class CaptchaTest extends TestCase
{
    public function testDifferentlyFormattedAnswersAlsoWork(): void
    {
        $captcha = new Captcha(0, 'Test question', [
            'test answer',
            'Second Possibility',
        ]);

        $validAnswers = [
            'test answer',
            'Second Possibility',
            'TEST ANSWER',
            'second POSSIBILITY',
            ' test answer ',
            'Test answer.',
            ' Test answer . ',
        ];

        foreach ($validAnswers as $validAnswer) {
            $this->assertTrue(
                $captcha->answerMatches($validAnswer),
                '"' . $validAnswer . '" must be a valid answer'
            );
        }
    }

    public function testWrongAnswersAreRecognized(): void
    {
        $captcha = new Captcha(0, 'Test question', [
            'test answer',
        ]);

        $invalidAnswers = [
            'not an answer',
            'test answer but with extra words',
            'extra words and then test answer',
        ];

        foreach ($invalidAnswers as $invalidAnswer) {
            $this->assertFalse(
                $captcha->answerMatches($invalidAnswer),
                '"' . $invalidAnswer . '" must be an invalid answer'
            );
        }
    }
}
