<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\domain\models;

use DateTimeImmutable;
use schalpoen\domain\models\Comment;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use PHPUnit\Framework\TestCase;

class CommentTest extends TestCase
{
    public function testIsRoot(): void
    {
        $post = new Post(new User('Author'), 'Test title', 'Test text');

        $rootComment = new Comment(
            $post,
            new User('Guest'),
            'Comment text'
        );
        $childComment = new Comment(
            $post,
            new User('Guest'),
            'Comment text'
        );
        $childComment->setParent($rootComment);

        $this->assertTrue($rootComment->isRoot());
        $this->assertFalse($childComment->isRoot());
    }

    public function testAuthorAlsoWrotePost(): void
    {
        $author = new User('Author');
        $author->id = 1;

        $differentUser = new User('Guest');
        $differentUser->id = 2;

        $post = new Post($author, 'Test title', 'Test text');

        $sameAuthorComment = new Comment(
            $post,
            $author,
            'Comment text'
        );

        $differentAuthorComment = new Comment(
            $post,
            $differentUser,
            'Comment text'
        );

        $this->assertTrue($sameAuthorComment->authorAlsoWrotePost());
        $this->assertFalse($differentAuthorComment->authorAlsoWrotePost());
    }

    public function testGetPostedAtIsTimeOfConstruction(): void
    {
        $post = new Post(new User('Author'), 'Test title', 'Test text');

        $now = new DateTimeImmutable();

        $comment = new Comment(
            $post,
            new User('Guest'),
            'Some text'
        );

        $difference = $now->diff($comment->getPostedAt(), true);
        $this->assertLessThanOrEqual(1, (int) $difference->format('YmdHis'));
    }

    public function testGetPostedAtConvertsFromUnixTime(): void
    {
        $post = new Post(new User('Author'), 'Test title', 'Test text');

        $comment = new Comment(
            $post,
            new User('Guest'),
            'Some text'
        );
        $comment->unixTime = 1580109201;

        $expected = '2020-01-27T07:13:21+00:00';
        $actual = $comment->getPostedAt()->format(DATE_ATOM);
        $this->assertEquals($expected, $actual);
    }
}
