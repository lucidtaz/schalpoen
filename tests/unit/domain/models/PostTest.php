<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\domain\models;

use DateTimeImmutable;
use DateTimeZone;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\User;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    public function testDefaultAttributes(): void
    {
        $author = new User('Author');
        $post = new Post($author, 'Test title', 'Test text');

        $this->assertSame($author, $post->user);
        $this->assertSame('Test title', $post->title);
        $this->assertSame('Test text', $post->text);
        $this->assertNull($post->preview);
        $this->assertSame(Post::STATUS_DRAFT, $post->status);
        $this->assertNull($post->getPublishedAt());
        $this->assertNull($post->getEditedAt());

        $this->assertTrue($post->isEditable());
        $this->assertFalse($post->isPublished());
        $this->assertFalse($post->isPublishable());
        $this->assertFalse($post->isFinalized());

        $this->assertSame('schalpoen.png', $post->getPreview());
    }

    /**
     * @dataProvider introductionsDataProvider
     */
    public function testGetIntroduction(string $postText, string $expected): void
    {
        $author = new User('Author');
        $post = new Post($author, 'title', $postText);

        $introduction = $post->getIntroduction();

        $this->assertEquals($expected, $introduction);
    }

    public function introductionsDataProvider(): array
    {
        return [
            'single-line' => ['Hello, world!', 'Hello, world!'],
            'multi-line' => ["Hello, world!\n\nSecond paragraph...", 'Hello, world!'],
        ];
    }

    public function testFinishedPostCanBeAccessedByAuthor(): void
    {
        $author = new User('Author');
        $author->isAuthor = true;
        $author->isPublisher = false;
        $author->id = 1;

        $post = new Post($author, 'Test title', 'Test text');
        $post->status = Post::STATUS_FINISHED;

        $post->authorize($author);
        $this->addToAssertionCount(1); // No exception was thrown
    }

    public function testHref(): void
    {
        $post = new Post(new User('Author'), 'Unit Test', 'Test text');
        $post->id = 3;

        $href = $post->href();

        $this->assertEquals('<a href="/posts/3/unit-test">Unit Test</a>', $href);
    }

    public function testPublishedAt(): void
    {
        $post = new Post(new User('Author'), 'Unit Test', 'Test text');
        $post->id = 3;

        $this->assertNull($post->getPublishedAt());

        $now = new DateTimeImmutable('now');

        $post->setState(Post::STATUS_PUBLISHED);

        $publishedAt = $post->getPublishedAt();
        $this->assertNotNull($publishedAt);
        $this->assertInstanceOf(DateTimeImmutable::class, $publishedAt);

        $utc = new DateTimeZone('UTC');
        $secondsDifference = (int) $publishedAt->setTimezone($utc)->getTimestamp()
            - (int) $now->setTimezone($utc)->getTimestamp();
        $this->assertGreaterThanOrEqual(0, $secondsDifference);
        $this->assertLessThanOrEqual(1, $secondsDifference);
    }
}
