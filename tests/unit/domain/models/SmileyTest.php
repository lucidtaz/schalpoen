<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\domain\models;

use schalpoen\domain\models\Smiley;
use PHPUnit\Framework\TestCase;

class SmileyTest extends TestCase
{
    public function testLinkOptionsAreSplit(): void
    {
        $smiley = new Smiley(0, 'the-path.png', ':),:D', 30, 40);
        $this->assertEquals([':)', ':D'], $smiley->getLinkOptions());
    }
}
