<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\domain\models;

use schalpoen\domain\models\Tag;
use PHPUnit\Framework\TestCase;

class TagTest extends TestCase
{
    public function testHref(): void
    {
        $tag = new Tag('Unit Test');
        $tag->id = 3;

        $href = $tag->href();

        $this->assertEquals('<a href="/tags/3/Unit-Test">Unit Test</a>', $href);
    }
}
