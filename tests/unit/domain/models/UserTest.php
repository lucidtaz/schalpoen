<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\domain\models;

use DateTimeImmutable;
use schalpoen\domain\models\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testHref(): void
    {
        $user = new User('Unit Test');
        $user->id = 3;

        $href = $user->href();

        $this->assertEquals('<a href="/users/3/Unit-Test">Unit Test</a>', $href);
    }

    public function testUpdateLoginTime(): void
    {
        $user = new User('Unit Test');
        $user->updateLoginTime();

        $now = new DateTimeImmutable();

        $lastLoginValue = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $user->lastLogin);
        $difference = $now->diff($lastLoginValue, true);
        $this->assertLessThanOrEqual(1, (int) $difference->format('YmdHis'));
    }
}
