<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\infrastructure\components;

use schalpoen\infrastructure\components\PasswordStrategy;
use PHPUnit\Framework\TestCase;

class PasswordStrategyTest extends TestCase
{
    private PasswordStrategy $passwordStrategy;

    protected function setUp(): void
    {
        parent::setUp();
        $this->passwordStrategy = new PasswordStrategy();
    }

    public function testWrongPasswordIsBlocked(): void
    {
        $hash = '$2y$10$O9w.R9gzcRJeQpVjhlOsJe/Xi8WCzkZl9dMt8b.llZfGythY7yF0G'; // 'the-password'

        $result = $this->passwordStrategy->verify('wrong-password', $hash);

        $this->assertFalse($result);
    }

    public function testCorrectPasswordIsAllowed(): void
    {
        $hash = '$2y$10$O9w.R9gzcRJeQpVjhlOsJe/Xi8WCzkZl9dMt8b.llZfGythY7yF0G'; // 'the-password'

        $result = $this->passwordStrategy->verify('the-password', $hash);

        $this->assertTrue($result);
    }

    public function testWeakPasswordNeedsRehash(): void
    {
        // Created by password_hash('the-password', PASSWORD_DEFAULT, ['cost' => 5])
        $hash = '$2y$05$aGLKPnAy8LnMoCgmoGwgnuT1mcqTeiJSxtHbDLi1df8CxvL3fqFxG';

        $result = $this->passwordStrategy->needsRehash($hash);

        $this->assertTrue($result);
    }

    public function testStrongPasswordNeedsNoRehash(): void
    {
        // This test may fail in the future when the default strength is increased.
        $hash = '$2y$10$O9w.R9gzcRJeQpVjhlOsJe/Xi8WCzkZl9dMt8b.llZfGythY7yF0G';

        $result = $this->passwordStrategy->needsRehash($hash);

        $this->assertFalse($result);
    }
}
