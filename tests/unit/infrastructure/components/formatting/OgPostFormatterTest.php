<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\infrastructure\components\formatting;

use schalpoen\domain\models\User;
use schalpoen\infrastructure\components\formatting\OgPostFormatter;
use schalpoen\domain\models\Post;
use schalpoen\application\parser\NullParser;
use PHPUnit\Framework\TestCase;

class OgPostFormatterTest extends TestCase
{
    private OgPostFormatter $formatter;

    protected function setUp(): void
    {
        $this->formatter = new OgPostFormatter(new NullParser());
    }

    public function testShortIntroductionIsFormatted(): void
    {
        $post = new Post(new User('Author'), 'Test title', 'Hello, world...');

        $formatted = $this->formatter->getIntroduction($post);

        $this->assertEquals('Hello, world...', $formatted);
    }

    public function testLongIntroductionIsTruncated(): void
    {
        $post = new Post(new User('Author'), 'Test title', "Hello, world...\n\nSecond paragraph.");

        $formatted = $this->formatter->getIntroduction($post);

        $this->assertEquals('Hello, world...', $formatted);
    }

    public function testDateIsFormatted(): void
    {
        $post = new Post(new User('Author'), 'Test title', 'Test text');
        $post->status = Post::STATUS_PUBLISHED;
        $post->publishedUnixTime = 1538912658;

        $formatted = $this->formatter->getDate($post);

        $this->assertEquals('2018-10-07T11:44:18+00:00', $formatted);
    }

    public function testUnpublishedPostHasADatePlaceholder(): void
    {
        $post = new Post(new User('Author'), 'Test title', 'Test text');
        $post->status = Post::STATUS_DRAFT;
        $post->publishedUnixTime = null;

        $formatted = $this->formatter->getDate($post);

        $this->assertNotEmpty($formatted);
    }
}
