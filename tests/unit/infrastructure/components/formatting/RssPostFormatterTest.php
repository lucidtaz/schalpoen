<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\infrastructure\components\formatting;

use schalpoen\domain\models\User;
use schalpoen\infrastructure\components\formatting\RssPostFormatter;
use schalpoen\domain\models\Post;
use schalpoen\application\parser\NullParser;
use PHPUnit\Framework\TestCase;

class RssPostFormatterTest extends TestCase
{
    private RssPostFormatter $formatter;

    protected function setUp(): void
    {
        $this->formatter = new RssPostFormatter(new NullParser());
    }

    public function testShortIntroductionIsFormatted(): void
    {
        $post = new Post(new User('Author'), 'Unit Test', 'Hello, world...');

        $formatted = $this->formatter->getIntroduction($post);

        $this->assertEquals('Hello, world...', $formatted);
    }

    public function testLongIntroductionIsTruncated(): void
    {
        $post = new Post(new User('Author'), 'Unit Test', "Hello, world...\n\nSecond paragraph.");

        $formatted = $this->formatter->getIntroduction($post);

        $this->assertEquals('Hello, world... Lees het hele artikel op de website.', $formatted);
    }

    public function testDateIsFormatted(): void
    {
        $post = new Post(new User('Author'), 'Unit Test', 'Hello, world...');
        $post->publishedUnixTime = 1538912658;

        $formatted = $this->formatter->getDate($post);

        $this->assertEquals('Sun, 07 Oct 2018 11:44:18 +0000', $formatted);
    }
}
