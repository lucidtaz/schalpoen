<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\infrastructure\models;

use DateTimeImmutable;
use schalpoen\infrastructure\models\Token;
use PHPUnit\Framework\TestCase;

class TokenTest extends TestCase
{
    public function testNewTokenIsValid(): void
    {
        $token = new Token(1, 'test-value');
        $this->assertTrue($token->isValid());
    }

    public function testOldTokenIsStillValid(): void
    {
        $token = new Token(1, 'test-value');
        $token->created = new DateTimeImmutable('119 days ago');
        $this->assertTrue($token->isValid());
    }

    public function testExpiredTokenIsNotValid(): void
    {
        $token = new Token(1, 'test-value');
        $token->created = new DateTimeImmutable('121 days ago');
        $this->assertFalse($token->isValid());
    }
}
