<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\mocks\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Comment;
use schalpoen\domain\models\Post;
use schalpoen\application\persistence\CommentRepository;

class MemoryCommentRepository implements CommentRepository
{
    /**
     * @var Comment[]
     */
    private array $comments = [];

    public function insert(Comment $comment): void
    {
        $comment->id = \count($this->comments);
        $this->comments[] = $comment;
    }

    public function retrieve(int $id): Comment
    {
        if (!array_key_exists($id, $this->comments)) {
            throw new NotFoundException('Comment not found.');
        }
        return $this->comments[$id];
    }

    /**
     * @return Comment[]
     */
    public function retrieveRootsForPost(Post $post): array
    {
        return array_filter($this->filterByPost($post), fn(Comment $comment) => $comment->isRoot());
    }

    /**
     * @return Comment[]
     */
    private function filterByPost(Post $post): array
    {
        return array_filter($this->comments, fn(Comment $comment) => $comment->post === $post);
    }

    public function countByPost(Post $post): int
    {
        return \count($this->filterByPost($post));
    }
}