<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\mocks\persistence;

use InvalidArgumentException;
use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;
use schalpoen\domain\models\User;
use schalpoen\application\persistence\PostRepository;

class MemoryPostRepository implements PostRepository
{
    /**
     * @var Post[]
     */
    private array $posts = [];

    public function insert(Post $post): void
    {
        $post->id = \count($this->posts);
        $this->posts[] = $post;
    }

    public function retrieve(int $id): Post
    {
        if (!array_key_exists($id, $this->posts)) {
            throw new NotFoundException('Post not found.');
        }
        return $this->posts[$id];
    }

    /**
     * @return Post[]
     */
    public function retrieveAll(): array
    {
        return $this->posts;
    }

    /**
     * @return Post[]
     */
    public function retrieveLastPublishedPosts(int $limit = 0): array
    {
        $published = array_filter($this->posts, fn(Post $post) => $post->isPublished());

        $sortedByDate = $published;
        usort($sortedByDate, fn(Post $a, Post $b) => $a->getPublishedAt() <=> $b->getPublishedAt());

        if ($limit === 0) {
            return $sortedByDate;
        }
        return \array_slice($sortedByDate, 0, $limit);
    }

    public function countPublishable(): int
    {
        $publishable = array_filter($this->posts, fn(Post $post) => $post->isPublishable());
        return \count($publishable);
    }

    /**
     * @return Post[]
     */
    public function retrieveEditableBy(User $author): array
    {
        return array_filter(
            $this->posts,
            fn(Post $post) => $post->getAuthor() === $author && $post->isEditable()
        );
    }

    public function countEditable(User $author): int
    {
        return \count($this->retrieveEditableBy($author));
    }

    /**
     * @return Post[]
     */
    public function retrieveFinalizedBy(User $author): array
    {
        return array_filter(
            $this->posts,
            fn(Post $post) => $post->getAuthor() === $author && $post->isFinalized()
        );
    }

    /**
     * @return Post[]
     */
    public function retrieveByTag(Tag $tag): array
    {
        return array_filter(
            $this->posts,
            fn(Post $post) => in_array($tag->getId(), $post->getTagIds(), true)
        );
    }

    public function update(Post $post): void
    {
        if ($post->id === null) {
            throw new InvalidArgumentException('id property needed');
        }
        $this->posts[$post->id] = $post;
    }
}
