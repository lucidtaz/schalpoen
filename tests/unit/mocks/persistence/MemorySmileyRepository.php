<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\mocks\persistence;

use schalpoen\application\persistence\SmileyRepository;
use schalpoen\domain\models\Smiley;

class MemorySmileyRepository implements SmileyRepository
{
    /**
     * @var Smiley[]
     */
    private array $contents = [];

    public function add(Smiley $smiley): void
    {
        $this->contents[] = $smiley;
    }

    /**
     * @return Smiley[]
     */
    public function retrieveAll(): array
    {
        return $this->contents;
    }
}
