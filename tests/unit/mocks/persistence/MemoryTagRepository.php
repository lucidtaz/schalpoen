<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\mocks\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\application\persistence\TagRepository;
use schalpoen\domain\models\Post;
use schalpoen\domain\models\Tag;

class MemoryTagRepository implements TagRepository
{
    /**
     * @var Tag[]
     */
    private array $tags = [];

    public function insert(Tag $tag): void
    {
        $tag->id = count($this->tags);
        $this->tags[] = $tag;
    }

    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): Tag
    {
        if (!array_key_exists($id, $this->tags)) {
            throw new NotFoundException('Tag not found.');
        }
        return $this->tags[$id];
    }

    /**
     * @return Tag[]
     */
    public function retrieveByPost(Post $post): array
    {
        return array_filter(
            $this->tags,
            fn(Tag $tag) => in_array($tag->id, $post->getTagIds(), true)
        );
    }

    /**
     * @return Tag[]
     */
    public function retrieveAll(): array
    {
        return $this->tags;
    }

    /**
     * @return Tag[]
     */
    public function retrievePopular(int $n): array
    {
        // We don't have post association info right now.
        throw new \LogicException('Not implemented');
    }
}
