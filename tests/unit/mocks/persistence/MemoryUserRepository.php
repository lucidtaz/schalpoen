<?php

declare(strict_types=1);

namespace schalpoen\tests\unit\mocks\persistence;

use schalpoen\application\exceptions\NotFoundException;
use schalpoen\domain\models\User;
use schalpoen\application\persistence\UserRepository;

class MemoryUserRepository implements UserRepository
{
    /**
     * @var User[]
     */
    private array $users = [];

    public function insert(User $user): void
    {
        $user->id = \count($this->users);
        $this->users[] = $user;
    }

    /**
     * @throws NotFoundException
     */
    public function retrieve(int $id): User
    {
        if (!array_key_exists($id, $this->users)) {
            throw new NotFoundException('User not found.');
        }
        return $this->users[$id];
    }

    /**
     * @return User[]
     */
    private function filterByDisplayname(string $displayname): array
    {
        return array_filter($this->users, fn(User $user) => $user->getName() === $displayname);
    }

    public function isDisplaynameFree(string $displayName): bool
    {
        return \count($this->filterByDisplayname($displayName)) === 0;
    }

    public function update(User $user): void
    {
        if ($user->id === null) {
            throw new \InvalidArgumentException('id attribute must be set');
        }
        $this->users[$user->id] = $user;
    }
}